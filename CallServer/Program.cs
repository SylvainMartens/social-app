﻿using System;
using System.Net;
using Hazel;
using Hazel.Udp;
using SocialAPP.Shared;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CallServer
{
    class Program
    {
        static List<Call> Calls = new List<Call>();
        static ConnectionListener listener;
        static int Port = 4298;

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += delegate (object sender, EventArgs e)
            {
                listener?.Close();
            };
            listener = new UdpConnectionListener(new NetworkEndPoint(IPAddress.Any, Port));
            listener.NewConnection += NewConnectionHandler;
            OnLog(string.Format("Server mounted, listening on port {0}", Port));
            listener.Start();
            while (true)
            {
                Console.ReadLine();
                Thread.Sleep(50);
            }
        }

        static void NewConnectionHandler(object sender, NewConnectionEventArgs args)
        {
            OnLog("Connected: " + args.Connection.EndPoint.ToString());

            args.Connection.DataReceived += DataReceivedHandler;
            args.Connection.Disconnected += Connection_Disconnected;

            args.Recycle();
        }

        private static void Connection_Disconnected(object sender, DisconnectedEventArgs args)
        {
            Connection connection = (Connection)sender;
            if (connection == null)
            {
                OnLog("Connection is null.");
            }
            else
                OnLog("Disconnected: " + connection.EndPoint.ToString());

            connection.DataReceived -= DataReceivedHandler;
            connection.Disconnected -= Connection_Disconnected;
            listener.NewConnection -= NewConnectionHandler;
            listener.NewConnection += NewConnectionHandler;

            var call = Calls.FirstOrDefault(x => x.Connection1.EndPoint.ToString() == connection.EndPoint.ToString() || x.Connection2.EndPoint.ToString() == connection.EndPoint.ToString());
            if (call != null)
            {
                try
                {
                    call.Connection1?.Close();
                }
                catch (Exception ex)
                {
                    OnError(ex.StackTrace);
                }
                try
                {
                    call.Connection2?.Close();
                }
                catch (Exception ex)
                {
                    OnError(ex.StackTrace);
                }
                Calls.Remove(call);
                OnLog("Calls removed.");
            }
            args.Recycle();
        }

        private static void DataReceivedHandler(object sender, DataReceivedEventArgs args)
        {
            Connection connection = (Connection)sender;
            if (args.Bytes == null) return;
            var callMessage = ProtoSerializer.Deserialize<CallMessage>(args.Bytes);
            if (callMessage == null) return;

            switch(callMessage.Tag)
            {
                case CallTags.SET_CALL:
                {
                    OnLog("Received [SET_CALL] from " + connection.EndPoint.ToString() + ".");
                    var callId = callMessage.ReadString();
                    lock (Calls)
                    {
                        var call = Calls.FirstOrDefault(x => x.Id == callId);
                        if (call == null)
                        {
                            call = new Call()
                            {
                                Id = callId,
                                Connection1 = connection
                            };
                            Calls.Add(call);
                        }
                        else
                        {
                            call.Connection2 = connection;
                        }
                    }
                }
                break;
                case CallTags.SYNC_AUDIO:
                {
                    OnLog("Received [SYNC_AUDIO] from " + connection.EndPoint.ToString() + ".");
                    var call = Calls.FirstOrDefault(x => x.Connection1 == connection || x.Connection2 == connection);
                    if (call == null) return;
                    if(call.Connection1 == connection)
                    {
                        call.Connection2?.SendBytes(args.Bytes, args.SendOption);
                    }else if(call.Connection2 == connection)
                    {
                        call.Connection1?.SendBytes(args.Bytes, args.SendOption);
                    }
                }
                break;
                case CallTags.SYNC_VIDEO:
                {
                    OnLog("Received [SYNC_VIDEO] from " + connection.EndPoint.ToString() + ".");
                    var call = Calls.FirstOrDefault(x => x.Connection1 == connection || x.Connection2 == connection);
                    if (call == null) return;
                    if (call.Connection1 == connection)
                    {
                        call.Connection2?.SendBytes(args.Bytes, args.SendOption);
                    }
                    else if (call.Connection2 == connection)
                    {
                        call.Connection1?.SendBytes(args.Bytes, args.SendOption);
                    }
                }
                break;
                case CallTags.DISCONNECTED:
                {
                    OnLog("Received [DISCONNECTED] from " + connection.EndPoint.ToString() + ".");
                    var call = Calls.FirstOrDefault(x => x.Connection1 == connection || x.Connection2 == connection);
                    if (call != null) Calls.Remove(call);
                    connection.Close();
                }
                break;
            }
            args.Recycle();
        }

        private static void OnLog(string message)
        {
            Console.WriteLine(message);
        }

        private static void OnWarning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("[Warning] " + message);
            Console.ResetColor();
        }

        private static void OnError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("[Error] " + message);
            Console.ResetColor();
        }

        private static void OnFatal(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.WriteLine("[Fatal] " + message);
            Console.ResetColor();
        }
    }
}