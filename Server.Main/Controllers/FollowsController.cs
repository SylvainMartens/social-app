﻿using Network;
using Network.Storage;
using Server.Shared.Controllers;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Server.Main.Controllers
{
    public static class FollowsController
    {
        private static void CacheFollowData(FollowData followData)
        {
            if (followData == null) return;
            CacheController.Set("follow_" + followData.Id, followData, 3600);
            CacheController.Set("follow_" + followData.ProfileId + "_" + followData.UserId, followData, 3600);
        }

        private static void DeleteCacheFollowData(string account1Id, string account2Id)
        {
            if (string.IsNullOrEmpty(account1Id) || string.IsNullOrEmpty(account2Id)) return;

            //account 1, unfollow account 2
            CacheController.Remove("follow_" + account2Id + "_" + account1Id);
            //Delete from lists
            try
            {
                var followingsArray = CacheController.Get<FollowData[]>("followings_" + account1Id);
                if (followingsArray != null)
                {
                    var list = followingsArray.ToList();
                    var follow = list.FirstOrDefault(x => x.ProfileId == account2Id && x.UserId == account1Id);
                    if (follow != null)
                    {
                        list.Remove(follow);
                        CacheController.Set("followings_" + account2Id, list.ToArray(), 3600);
                    }
                }

                var followersArray = CacheController.Get<FollowData[]>("followers_" + account2Id);
                if (followersArray != null)
                {
                    var List = followersArray.ToList();
                    var follow = List.FirstOrDefault(x => x.ProfileId == account2Id && x.UserId == account1Id);
                    if (follow != null)
                    {
                        List.Remove(follow);
                        CacheController.Set("followers_" + account1Id, followersArray, 3600);//Refresh cache!
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        public static void Follow(string myAccountId, string otherAccountId)
        {
            if (IsFollowing(myAccountId, otherAccountId)) return;
            try
            {
                var id = "";
                lock (NetworkServer.database)
                {
                    id = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO follows (profileId, userId, dateAt) VALUES (@otherAccountId, @myAccountId, NOW()); SELECT LAST_INSERT_ID();", new QueryParameter("myAccountId", myAccountId), new QueryParameter("otherAccountId", otherAccountId))).ToString();
                }
                if (!string.IsNullOrEmpty(id))
                {
                    var profile = AccountsController.GetAccountById(otherAccountId);
                    if (profile != null)
                    {
                        var user = AccountsController.GetAccountById(myAccountId);
                        if (user != null)
                        {
                            var follow = new FollowData()
                            {
                                Id = id,
                                ProfileId = otherAccountId,
                                ProfileName = profile.Username,
                                ProfileAvatar = profile.Avatar,
                                ProfileGender = profile.Gender,
                                UserId = myAccountId,
                                UserName = user.Username,
                                UserAvatar = user.Avatar,
                                UserGender = user.Gender
                            };
                            CacheFollowData(follow);
                            //Add to followers cache!
                            var followers = CacheController.Get<FollowData[]>("followers_" + otherAccountId);
                            if (followers != null)
                            {
                                var followersList = followers.ToList();
                                followersList.Add(follow);
                                CacheController.Set("followers_" + otherAccountId, followersList.ToArray(), 3600);
                            }
                            //Add to followings cache!
                            var followings = CacheController.Get<FollowData[]>("followings_" + myAccountId);
                            if (followings != null)
                            {
                                var followingsList = followings.ToList();
                                followingsList.Add(follow);
                                CacheController.Set("followings_" + myAccountId, followingsList.ToArray(), 3600);
                            }
                        }
                    }
                }
                NotificationsController.TriggerNotification(myAccountId, otherAccountId, NotificationTypes.Follow);
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        public static void UnFollow(string myAccountId, string otherAccountId, bool triggerNotification = true)
        {
            if (!IsFollowing(myAccountId, otherAccountId)) return;
            try
            {
                lock (NetworkServer.database)
                {
                    NetworkServer.database.ExecuteNonQuery("DELETE FROM follows WHERE profileId = @otherAccountId AND userId = @myAccountId", new QueryParameter("myAccountId", myAccountId), new QueryParameter("otherAccountId", otherAccountId));
                }
                DeleteCacheFollowData(myAccountId, otherAccountId);
                if(triggerNotification)
                    NotificationsController.TriggerNotification(myAccountId, otherAccountId, NotificationTypes.Unfollow);
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        public static bool IsFollowing(string myAccountId, string otherAccountId)
        {
            try
            {
                var follow = CacheController.Get<FollowData>("follow_" + otherAccountId + "_" + myAccountId);
                if (follow == null)
                {
                    //Try to select from MySQL
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM follows WHERE profileId = @profileId AND userId = @userId LIMIT 1", new QueryParameter("profileId", otherAccountId), new QueryParameter("userId", myAccountId));
                    if (rows.Length == 1)
                    {
                        follow = DatabaseRowToFollowData(rows[0]);
                        if (follow != null)
                        {
                            CacheFollowData(follow);
                            return true;
                        }
                    }
                }
                else
                {
                    CacheFollowData(follow);//Refresh Cache
                    return true;
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return false;
        }

        public static bool IsFollowingMe(string otherAccountId, string myAccountId)
        {
            return IsFollowing(otherAccountId, myAccountId);
        }

        public static List<FollowData> GetFollowings(string accountId)
        {
            try
            {
                var followingsArray = CacheController.Get<FollowData[]>("followings_" + accountId);
                if (followingsArray != null)
                {
                    CacheController.Set("followings_" + accountId, followingsArray, 3600);//Refresh cache!
                    return followingsArray.ToList();
                }
                else
                {
                    //Get followings from MySQL
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM follows WHERE userId = @profileId ORDER BY id DESC", new QueryParameter("profileId", accountId));
                    var returnList = new List<FollowData>();
                    foreach (var row in rows)
                    {
                        var follow = DatabaseRowToFollowData(row);
                        if (follow != null) returnList.Add(follow);
                    }
                    if (rows.Length > 0)
                    {
                        CacheController.Set("followings_" + accountId, returnList.ToArray(), 3600);
                    }
                    return returnList;
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return default(List<FollowData>);
        }

        public static List<FollowData> GetFollowers(string accountId)
        {
            try
            {
                var followersArray = CacheController.Get<FollowData[]>("followers_" + accountId);
                if (followersArray != null)
                {
                    CacheController.Set("followers_" + accountId, followersArray, 3600);//Refresh cache!
                    return followersArray.ToList();
                }
                else
                {
                    //Get followers from MySQL
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM follows WHERE profileId = @profileId ORDER BY id DESC", new QueryParameter("profileId", accountId));
                    var returnList = new List<FollowData>();
                    foreach (var row in rows)
                    {
                        var follow = DatabaseRowToFollowData(row);
                        if (follow != null) returnList.Add(follow);
                    }
                    if (rows.Length > 0)
                    {
                        CacheController.Set("followers_" + accountId, returnList.ToArray(), 3600);
                    }
                    return returnList;
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return default(List<FollowData>);
        }

        public static FollowData DatabaseRowToFollowData(DatabaseRow row)
        {
            if (row == null) return null;
            try
            {
                if (row.Count == 4)
                {
                    var profileId = row["profileId"].ToString();
                    var userId = row["userId"].ToString();

                    var profile = AccountsController.GetAccountById(profileId);
                    if (profile == null) return null;

                    var user = AccountsController.GetAccountById(userId);
                    if (user == null) return null;

                    return new FollowData()
                    {
                        Id = row["id"].ToString(),
                        ProfileId = profileId,
                        ProfileName = profile.Username,
                        ProfileAvatar = profile.Avatar,
                        ProfileGender = profile.Gender,
                        UserId = userId,
                        UserName = user.Username,
                        UserAvatar = user.Avatar,
                        UserGender = user.Gender
                    };
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }
    }
}