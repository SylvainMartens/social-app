﻿using System.Collections.Generic;
using System.Linq;

namespace Server.Main.Controllers
{
    public static class SessionsController
    {
        private static List<SessionData> Sessions = new List<SessionData>();

        public static SessionData GetSessionByAccountId(string accountId)
        {
            return Sessions.FirstOrDefault(x => x.AccountId.Equals(accountId));
        }

        public static SessionData GetSessionByConnectionId(ushort connectionId)
        {
            return Sessions.FirstOrDefault(x => x.ConnectionId == connectionId);
        }

        public static void Create(string accountId, string ip, ushort connectionId)
        {
            Sessions.Add(new SessionData()
            {
                AccountId = accountId,
                IP = ip,
                ConnectionId = connectionId
            });
        }

        public static void DeleteByConnectionId(ushort connectionId)
        {
            var session = GetSessionByConnectionId(connectionId);
            if (session != null)
            {
                Sessions.Remove(session);
            }
        }

        public static void DeleteByAccountId(string accountId)
        {
            var session = GetSessionByAccountId(accountId);
            if (session != null)
            {
                Sessions.Remove(session);
            }
        }
    }

    public class SessionData
    {
        public string AccountId { get; set; }
        public string IP { get; set; }
        public ushort ConnectionId { get; set; }
    }
}