﻿using Network;
using Network.Storage;
using Server.Shared.Controllers;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;

namespace Server.Main.Controllers
{
    public static class MappingController
    {
        public static void GetMapPosition(AccountData myAccount, AccountData otherAccount)
        {
            var request = GetRequest(myAccount.Id, otherAccount.Id);
            if (request == null)
            {
                SendMapRequest(myAccount, otherAccount);
            }
            else
            {
                if (request.Accepted)
                {
                    //emit position!
                    TriggerPosition(request, otherAccount);
                    //Delete Request...
                    DeleteMapRequest(request);
                }
            }
        }

        public static void SendMapRequest(AccountData myAccount, AccountData otherAccount)
        {
            var insertedId = "";
            lock (NetworkServer.database)
            {
                insertedId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO requests (userId, profileId, dateAt, requestType) VALUES (@myAccountId, @otherAccountId, NOW(), 1); SELECT LAST_INSERT_ID();", new QueryParameter("myAccountId", myAccount.Id), new QueryParameter("otherAccountId", otherAccount.Id))).ToString();
            }
            if (!string.IsNullOrEmpty(insertedId))
            {
                var callRequest = new RequestData()
                {
                    Id = insertedId,
                    UserId = myAccount.Id,
                    UserName = myAccount.Username,
                    UserAvatar = myAccount.Avatar,
                    UserGender = myAccount.Gender,
                    ProfileId = otherAccount.Id,
                    ProfileName = otherAccount.Username,
                    ProfileAvatar = otherAccount.Avatar,
                    ProfileGender = otherAccount.Gender,
                    DateAt = DateTime.Now,
                    Accepted = false,
                    RequestType = 1
                };
                CacheRequest(callRequest);
                TriggerRequest(callRequest);
            }
        }

        public static RequestData GetRequestById(string requestId)
        {
            var request = CacheController.Get<RequestData>("mapRequest_" + requestId);
            if (request != null)
            {
                CacheController.Set("mapRequest_" + requestId, request, 3600);
            }
            else
            {
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM requests WHERE id = @requestId LIMIT 1", new QueryParameter("requestId", requestId));
                if (rows.Length == 1)
                {
                    request = RowToMapRequest(rows[0]);
                    if (request != null)
                    {
                        CacheRequest(request);
                    }
                }
            }
            return request;
        }
        public static RequestData GetRequest(string myAccountId, string otherAccountId)
        {
            try
            {
                var callRequest = CacheController.Get<RequestData>("mapRequest_" + myAccountId + "_" + otherAccountId);
                if (callRequest != null)
                {
                    CacheRequest(callRequest);
                    return callRequest;
                }
                else
                {
                    //From MySQL
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM requests WHERE userId = @userId AND profileId = @profileId AND requestType=1 LIMIT 1", new QueryParameter("userId", myAccountId), new QueryParameter("profileId", otherAccountId));
                    if (rows.Length == 1)
                    {
                        callRequest = RowToMapRequest(rows[0]);
                        if (callRequest != null)
                        {
                            CacheRequest(callRequest);
                            return callRequest;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }
        public static List<RequestData> GetRequests(string myAccountId)
        {
            var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM requests WHERE (userId = @accountId OR profileId = @accountId) AND accepted=0 AND requestType=1 ORDER BY id DESC", new QueryParameter("accountId", myAccountId));
            List<RequestData> returnList = new List<RequestData>();
            foreach (var row in rows)
            {
                var request = RowToMapRequest(row);
                if (request != null)
                {
                    returnList.Add(request);
                }
            }
            return returnList;
        }
        public static void SetRequestResult(string myAccountId, string otherAccountId, bool accepted)
        {
            var request = GetRequest(otherAccountId, myAccountId);
            if (request == null) return;

            if (myAccountId != request.ProfileId) return;//Only the person who received the request can accept or decline!

            if (accepted)
            {
                //Accepted Call Request
                NetworkServer.database.ExecuteNonQuery("UPDATE requests SET accepted = 1 WHERE id = @mapRequestId LIMIT 1", new QueryParameter("mapRequestId", request.Id));
                request.Accepted = true;
                CacheRequest(request);
                //Trigger Notification, call request accepted.
                NotificationsController.TriggerNotification(request.ProfileId, request.UserId, NotificationTypes.MapRequestAccepted);
            }
            else
            {
                //Declined Call Request
                DeleteMapRequest(request);
                //Trigger Notification, call request declined
                NotificationsController.TriggerNotification(request.ProfileId, request.UserId, NotificationTypes.MapRequestDeclined);
            }
        }

        public static RequestData RowToMapRequest(DatabaseRow row)
        {
            if (row == null || row.Count != 6) return null;
            try
            {
                var user = AccountsController.GetAccountById(row["userId"].ToString());
                var otherAccount = AccountsController.GetAccountById(row["profileId"].ToString());
                if (user == null || otherAccount == null) return null;
                DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());

                return new RequestData()
                {
                    Id = row["id"].ToString(),
                    UserId = user.Id,
                    UserName = user.Username,
                    UserAvatar = user.Avatar,
                    UserGender = user.Gender,
                    ProfileId = otherAccount.Id,
                    ProfileName = otherAccount.Username,
                    ProfileGender = otherAccount.Gender,
                    DateAt = dateAt,
                    Accepted = Convert.ToInt32(row["accepted"].ToString()) == 1,
                    RequestType = Convert.ToInt32(row["requestType"].ToString())
                };
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static void DeleteMapRequest(RequestData request)
        {
            try
            {
                NetworkServer.database.ExecuteNonQuery("DELETE FROM requests WHERE id = @mapRequestId", new QueryParameter("mapRequestId", request.Id));
                DeleteCacheRequest(request);
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        public static void CacheRequest(RequestData request)
        {
            if (request == null) return;
            CacheController.Set("mapRequest_" + request.Id, request, 3600);
            CacheController.Set("mapRequest_" + request.UserId + "_" + request.ProfileId, request, 3600);
            RequestsController.CacheRequest(request);
        }
        public static void DeleteCacheRequest(RequestData request)
        {
            if (request == null) return;
            CacheController.Remove("mapRequest_" + request.Id);
            CacheController.Remove("mapRequest_" + request.UserId + "_" + request.ProfileId);
            RequestsController.DeleteCacheRequest(request);
        }

        public static void TriggerRequest(RequestData request)
        {
            bool triggered1 = false;
            bool triggered2 = false;

            var user1Session = SessionsController.GetSessionByAccountId(request.UserId);
            if (user1Session != null)
            {
                using (NetworkWriter writer = new NetworkWriter())
                {
                    writer.Write(ProtoSerializer.Serialize(request));
                    NetworkServer.GetConnectionServiceByID(user1Session.ConnectionId)?.SendReply(ClientTags.REQUEST, 0, writer);
                    triggered1 = true;
                }
            }
            var user2Session = SessionsController.GetSessionByAccountId(request.ProfileId);
            if (user2Session != null)
            {
                using (NetworkWriter writer = new NetworkWriter())
                {
                    writer.Write(ProtoSerializer.Serialize(request));
                    NetworkServer.GetConnectionServiceByID(user2Session.ConnectionId)?.SendReply(ClientTags.REQUEST, 0, writer);
                    triggered2 = true;
                }
            }

            if (!triggered1 || !triggered2)
            {
                //Trigger On Master Server.
                MainServer.NotifyMapRequest(request);
            }
        }

        public static void TriggerPosition(RequestData request, AccountData otherAccount)
        {
            var triggered = false;
            var session = SessionsController.GetSessionByAccountId(request.UserId);
            if (session != null)
            {
                var connection = NetworkServer.GetConnectionServiceByID(session.ConnectionId);
                if (connection != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(otherAccount.Latitude);
                        writer.Write(otherAccount.Longitude);
                        connection.SendReply(ClientTags.SEND_REQUEST_POS, 0, writer);
                        triggered = true;
                    }
                }
            }
            if (triggered)
            {
                MainServer.NotifyPosition(request, otherAccount);
            }
        }
    }
}