﻿using Network;
using Network.Storage;
using Server.Shared.Controllers;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Server.Main.Controllers
{
    public static class CallsController
    {
        public static void SetCallResult(string callId, string userId, bool answered)
        {
            var callData = CacheController.Get<CallData>("call_" + callId);
            if (callData == null) return;

            if (userId != callData.ProfileId) return;//Only the person being called can accept or decline the call!
            if (answered)
            {
                callData.Answered = true;
                CacheCall(callData);
                var callerUser = SessionsController.GetSessionByAccountId(callData.UserId);
                if (callerUser != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(ProtoSerializer.Serialize(callData));
                        writer.Write("149.56.25.176");
                        writer.Write(4298);
                        NetworkServer.GetConnectionServiceByID(callerUser.ConnectionId).SendReply(ClientTags.START_CALL, 0, writer);
                    }
                }
                else
                {
                    //TODO Send to Master Server!
                }

                var otherUser = SessionsController.GetSessionByAccountId(callData.ProfileId);
                if (otherUser != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(ProtoSerializer.Serialize(callData));
                        writer.Write("149.56.25.176");
                        writer.Write(4298);
                        NetworkServer.GetConnectionServiceByID(otherUser.ConnectionId).SendReply(ClientTags.START_CALL, 0, writer);
                    }
                }
                else
                {
                    //TODO Send to Master Server!
                }
            }
            else
            {
                EndCall(callId, userId);
            }
        }

        public static CallData GetCallById(string callId)
        {
            var callData = CacheController.Get<CallData>("call_" + callId);
            return callData;
        }

        public static void EndCall(string callId, string userId, bool missed = false)
        {
            var callData = CacheController.Get<CallData>("call_" + callId);
            if (callData == null) return;

            if (userId != callData.UserId && userId != callData.ProfileId) return;//You have no right to end this call!

            try
            {
                var callerUser = SessionsController.GetSessionByAccountId(callData.UserId);
                if (callerUser != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(ProtoSerializer.Serialize(callData));
                        NetworkServer.GetConnectionServiceByID(callerUser.ConnectionId)?.SendReply(ClientTags.END_CALL, 0, writer);
                    }
                }
                else
                {
                    //TODO Send to Master Server!
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }

            try
            {
                var otherUser = SessionsController.GetSessionByAccountId(callData.ProfileId);
                if (otherUser != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(ProtoSerializer.Serialize(callData));
                        NetworkServer.GetConnectionServiceByID(otherUser.ConnectionId)?.SendReply(ClientTags.END_CALL, 0, writer);
                    }
                }
                else
                {
                    //TODO Send to Master Server!
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }

            if (missed)
            {
                NotificationsController.TriggerNotification(callData.UserId, callData.ProfileId, NotificationTypes.CallMissed);
            }

            DeleteCacheCall(callData);
        }

        private static async Task RingThread(CallData callData)
        {
            if (callData == null) return;
            int count = 0;
            await Task.Delay(1000);
            while (true)
            {
                var cachedCallData = CacheController.Get<CallData>("call_" + callData.Id);
                if (cachedCallData == null)
                {
                    Interface.LogError("Cannot find any call with id: " + callData.Id);
                    break;//User probably cancelled the call!
                }
                if (cachedCallData.Answered)
                {
                    break;
                }
                if (count != 7)
                {
                    try
                    {
                        var callerUser = SessionsController.GetSessionByAccountId(callData.UserId);
                        if (callerUser != null)
                        {
                            using (NetworkWriter writer = new NetworkWriter())
                            {
                                NetworkServer.GetConnectionServiceByID(callerUser.ConnectionId)?.SendReply(ClientTags.DO_CALL_RING, 0, writer);
                            }
                        }
                        else
                        {
                            //TODO Send to Master Server!
                        }
                    }
                    catch (Exception ex)
                    {
                        Interface.LogError(ex.StackTrace);
                    }

                    try
                    {
                        var otherUser = SessionsController.GetSessionByAccountId(callData.ProfileId);
                        if (otherUser != null)
                        {
                            using (NetworkWriter writer = new NetworkWriter())
                            {
                                NetworkServer.GetConnectionServiceByID(otherUser.ConnectionId)?.SendReply(ClientTags.DO_CALL_RING, 0, writer);
                            }
                        }
                        else
                        {
                            //TODO Send to Master Server!
                        }
                    }
                    catch (Exception ex)
                    {
                        Interface.LogError(ex.StackTrace);
                    }
                }
                if (count == 7)
                {
                    EndCall(callData.Id, callData.UserId, true);
                    break;
                }
                await Task.Delay(4000);
                count++;
            }
        }

        public static void SetCallRequestResult(string accountId, string otherAccountId, bool result)
        {
            var callRequest = GetCallRequest(otherAccountId, accountId);
            if (callRequest == null) return;

            if (result)
            {
                //Accepted Call Request
                NetworkServer.database.ExecuteNonQuery("UPDATE requests SET accepted = 1 WHERE id = @callRequestId LIMIT 1", new QueryParameter("callRequestId", callRequest.Id));
                callRequest.Accepted = true;
                CacheCallRequest(callRequest);
                //Trigger Notification, call request accepted.
                NotificationsController.TriggerNotification(accountId, otherAccountId, NotificationTypes.CallRequestAccepted);
            }
            else
            {
                //Declined Call Request
                DeleteCallRequest(callRequest);
                //Trigger Notification, call request declined
                NotificationsController.TriggerNotification(accountId, otherAccountId, NotificationTypes.CallRequestDeclined);
            }
        }

        public static RequestData GetCallRequest(string myAccountId, string otherAccountId)
        {
            try
            {
                var callRequest = CacheController.Get<RequestData>("callRequest_" + myAccountId + "_" + otherAccountId);
                if (callRequest != null)
                {
                    CacheCallRequest(callRequest);
                    return callRequest;
                }
                else
                {
                    //From MySQL
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM requests WHERE userId = @userId AND profileId = @profileId AND requestType=0 LIMIT 1", new QueryParameter("userId", myAccountId), new QueryParameter("profileId", otherAccountId));
                    if (rows.Length == 1)
                    {
                        callRequest = RowToCallRequest(rows[0]);
                        if (callRequest != null)
                        {
                            CacheCallRequest(callRequest);
                            return callRequest;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static List<RequestData> GetCallRequests(string myAccountId)
        {
            var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM requests WHERE (userId = @accountId OR profileId = @accountId) AND accepted=0 AND requestType=0 ORDER BY id DESC", new QueryParameter("accountId", myAccountId));
            List<RequestData> returnList = new List<RequestData>();
            foreach (var row in rows)
            {
                var callRequestData = RowToCallRequest(row);
                if (callRequestData != null)
                {
                    returnList.Add(callRequestData);
                }
            }
            return returnList;
        }

        public static bool SendCall(AccountData myAccount, AccountData otherAccount)
        {
            var callRequest = GetCallRequest(myAccount.Id, otherAccount.Id);
            if (callRequest == null)
            {
                if (myAccount.IsPremium)
                {
                    SendCallRequest(myAccount, otherAccount);
                }
                else
                {
                    return true;
                }
            }
            else if (callRequest.Accepted)
            {
                //TODO, Check if the user is online!?

                //Delete the Call Request.
                DeleteCallRequest(callRequest);

                var callData = new CallData()
                {
                    Id = Encryption.SHA1Of(myAccount.Id + "_" + otherAccount.Id),
                    UserId = myAccount.Id,
                    UserName = myAccount.Username,
                    UserAvatar = myAccount.Avatar,
                    UserGender = myAccount.Gender,
                    ProfileId = otherAccount.Id,
                    ProfileName = otherAccount.Username,
                    ProfileAvatar = otherAccount.Avatar,
                    ProfileGender = otherAccount.Gender
                };
                CacheCall(callData);

                //Call the user
                var callerUser = SessionsController.GetSessionByAccountId(myAccount.Id);
                if (callerUser != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(ProtoSerializer.Serialize(callData));
                        NetworkServer.GetConnectionServiceByID(callerUser.ConnectionId)?.SendReply(ClientTags.DO_CALL, 0, writer);
                    }
                }

                var otherUser = SessionsController.GetSessionByAccountId(otherAccount.Id);
                if (otherUser != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(ProtoSerializer.Serialize(callData));
                        NetworkServer.GetConnectionServiceByID(otherUser.ConnectionId)?.SendReply(ClientTags.RECEIVE_CALL, 0, writer);
                    }
                }
                else
                {
                    //TODO Send to Master Server!
                }
                Thread thread = new Thread(() => RingThread(callData));
                thread.Start();
            }
            return false;
        }

        private static void SendCallRequest(AccountData myAccount, AccountData otherAccount)
        {
            var insertedId = "";
            lock (NetworkServer.database)
            {
                insertedId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO requests (userId, profileId, dateAt, requestType) VALUES (@myAccountId, @otherAccountId, NOW(), 0); SELECT LAST_INSERT_ID();", new QueryParameter("myAccountId", myAccount.Id), new QueryParameter("otherAccountId", otherAccount.Id))).ToString();
            }
            if (!string.IsNullOrEmpty(insertedId))
            {
                var callRequest = new RequestData()
                {
                    Id = insertedId,
                    UserId = myAccount.Id,
                    UserName = myAccount.Username,
                    UserAvatar = myAccount.Avatar,
                    UserGender = myAccount.Gender,
                    ProfileId = otherAccount.Id,
                    ProfileName = otherAccount.Username,
                    ProfileAvatar = otherAccount.Avatar,
                    ProfileGender = otherAccount.Gender,
                    DateAt = DateTime.Now,
                    Accepted = false,
                    RequestType = 0
                };
                CacheCallRequest(callRequest);
                TriggerCallRequest(callRequest);
            }
        }

        public static RequestData RowToCallRequest(DatabaseRow row)
        {
            if (row == null || row.Count != 6) return null;
            try
            {
                var user = AccountsController.GetAccountById(row["userId"].ToString());
                var otherAccount = AccountsController.GetAccountById(row["profileId"].ToString());
                if (user == null || otherAccount == null) return null;
                DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());

                return new RequestData()
                {
                    Id = row["id"].ToString(),
                    UserId = user.Id,
                    UserName = user.Username,
                    UserAvatar = user.Avatar,
                    UserGender = user.Gender,
                    ProfileId = otherAccount.Id,
                    ProfileName = otherAccount.Username,
                    ProfileGender = otherAccount.Gender,
                    DateAt = dateAt,
                    Accepted = Convert.ToInt32(row["accepted"].ToString()) == 1,
                    RequestType = Convert.ToInt32(row["requestType"].ToString())
                };
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static void DeleteCallRequest(RequestData callRequestData)
        {
            try
            {
                NetworkServer.database.ExecuteNonQuery("DELETE FROM requests WHERE id = @callRequestId", new QueryParameter("callRequestId", callRequestData.Id));
                DeleteCacheCallRequest(callRequestData);
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        public static void CacheCallRequest(RequestData callRequestData)
        {
            if (callRequestData == null) return;
            CacheController.Set("callRequest_" + callRequestData.Id, callRequestData, 3600);
            CacheController.Set("callRequest_" + callRequestData.UserId + "_" + callRequestData.ProfileId, callRequestData, 3600);
            RequestsController.CacheRequest(callRequestData);
        }

        public static void DeleteCacheCallRequest(RequestData callRequestData)
        {
            if (callRequestData == null) return;
            CacheController.Remove("callRequest_" + callRequestData.Id);
            CacheController.Remove("callRequest_" + callRequestData.UserId + "_" + callRequestData.ProfileId);
            RequestsController.DeleteCacheRequest(callRequestData);
        }

        public static void TriggerCallRequest(RequestData callRequestData)
        {
            bool triggered1 = false;
            bool triggered2 = false;

            var user1Session = SessionsController.GetSessionByAccountId(callRequestData.UserId);
            if (user1Session != null)
            {
                using (NetworkWriter writer = new NetworkWriter())
                {
                    writer.Write(ProtoSerializer.Serialize(callRequestData));
                    NetworkServer.GetConnectionServiceByID(user1Session.ConnectionId)?.SendReply(ClientTags.REQUEST, 0, writer);
                    triggered1 = true;
                }
            }
            var user2Session = SessionsController.GetSessionByAccountId(callRequestData.ProfileId);
            if (user2Session != null)
            {
                using (NetworkWriter writer = new NetworkWriter())
                {
                    writer.Write(ProtoSerializer.Serialize(callRequestData));
                    NetworkServer.GetConnectionServiceByID(user2Session.ConnectionId)?.SendReply(ClientTags.REQUEST, 0, writer);
                    triggered2 = true;
                }
            }

            if (!triggered1 || !triggered2)
            {
                //Trigger On Master Server.
                MainServer.NotifyMapRequest(callRequestData);
            }
        }

        public static void CacheCall(CallData callData)
        {
            if (callData == null) return;
            CacheController.Set("call_" + callData.Id, callData, 3600);
        }
        public static void DeleteCacheCall(CallData callData)
        {
            if (callData == null) return;
            CacheController.Remove("call_" + callData.Id);
        }


        //Temp until we fix UDP server.
        private static List<Call> Calls = new List<Call>();
        public static void SetCallId(string id, ushort connection)
        {
            lock (Calls)
            {
                var call = Calls.FirstOrDefault(x => x.Id == id);
                if (call == null)
                {
                    call = new Call()
                    {
                        Id = id,
                        Connection1 = connection
                    };
                    Calls.Add(call);
                }
                else
                {
                    call.Connection2 = connection;
                }
            }
        }
        public static void SyncAudio(byte[] bytes, ushort connection)
        {
            var call = Calls.FirstOrDefault(x => x.Connection1 == connection || x.Connection2 == connection);
            if (call == null) return;
            if (call.Connection1 == connection)
            {
                var userConnection = NetworkServer.GetConnectionServiceByID(call.Connection2);
                if (userConnection != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        //writer.Write(call.Id);
                        writer.Write(bytes);
                        userConnection.SendReply(ClientTags.SYNC_AUDIO, 0, writer);
                    }
                }
                else
                {
                    //Trigger on main server!
                }
            }
            else if (call.Connection2 == connection)
            {
                var userConnection = NetworkServer.GetConnectionServiceByID(call.Connection1);
                if (userConnection != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        //writer.Write(call.Id);
                        writer.Write(bytes);
                        userConnection.SendReply(ClientTags.SYNC_AUDIO, 0, writer);
                    }
                }
                else
                {
                    //Trigger on main server!
                }
            }
        }
        public static void SyncVideo(byte[] bytes, ushort connection)
        {
            var call = Calls.FirstOrDefault(x => x.Connection1 == connection || x.Connection2 == connection);
            if (call == null) return;
            if (call.Connection1 == connection)
            {
                using (NetworkWriter writer = new NetworkWriter())
                {
                    writer.Write(call.Id);
                    writer.Write(bytes);
                    NetworkServer.GetConnectionServiceByID(call.Connection2).SendReply(ClientTags.SYNC_VIDEO, 0, writer);
                }
            }
            else if (call.Connection2 == connection)
            {
                using (NetworkWriter writer = new NetworkWriter())
                {
                    writer.Write(call.Id);
                    writer.Write(bytes);
                    NetworkServer.GetConnectionServiceByID(call.Connection1).SendReply(ClientTags.SYNC_VIDEO, 0, writer);
                }
            }
        }
    }
    public class Call
    {
        public string Id { get; set; }
        public ushort Connection1 { get; set; }
        public ushort Connection2 { get; set; }
    }
}