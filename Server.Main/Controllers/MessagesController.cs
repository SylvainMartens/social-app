﻿using Network;
using Network.Storage;
using Server.Shared.Controllers;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;

namespace Server.Main.Controllers
{
    public static class MessagesController
    {
        //Requests
        public static void GetOrCreateRequest(AccountData myAccount, AccountData otherAccount)
        {
            var request = GetRequest(myAccount.Id, otherAccount.Id);
            if (request != null)
            {
                if (request.Accepted)
                {
                    //Start the conversation and show it.
                    var conversationId = GetConversationId(myAccount.Id, otherAccount.Id);
                    if (!string.IsNullOrEmpty(conversationId))
                    {
                        var conversation = GetConversationById(conversationId);
                        if (conversation != null)
                        {
                            var user1 = AccountsController.GetAccountById(conversation.User1Id);
                            if (user1 != null)
                            {
                                conversation.User1Name = user1.Username;
                                conversation.User1Avatar = user1.Avatar;
                                conversation.User1Gender = user1.Gender;
                            }
                            var user2 = AccountsController.GetAccountById(conversation.User2Id);
                            if (user2 != null)
                            {
                                conversation.User2Name = user2.Username;
                                conversation.User2Avatar = user2.Avatar;
                                conversation.User2Gender = user2.Gender;
                            }
                            CacheConversation(conversation);
                            var session = SessionsController.GetSessionByAccountId(myAccount.Id);
                            if (session != null)
                            {
                                using (NetworkWriter writer = new NetworkWriter())
                                {
                                    writer.Write(1);
                                    writer.Write(ProtoSerializer.Serialize(conversation));
                                    var connection = NetworkServer.GetConnectionServiceByID(session.ConnectionId)?.SendReply(ClientTags.START_CONVERSATION, 0, writer);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                //Create the request...
                SendRequest(myAccount, otherAccount);
            }
        }

        public static void SendRequest(AccountData myAccount, AccountData otherAccount)
        {
            var insertedId = "";
            lock (NetworkServer.database)
            {
                insertedId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO requests (userId, profileId, dateAt, requestType) VALUES (@myAccountId, @otherAccountId, NOW(), 2); SELECT LAST_INSERT_ID();", new QueryParameter("myAccountId", myAccount.Id), new QueryParameter("otherAccountId", otherAccount.Id))).ToString();
            }
            if (!string.IsNullOrEmpty(insertedId))
            {
                var request = new RequestData()
                {
                    Id = insertedId,
                    UserId = myAccount.Id,
                    UserName = myAccount.Username,
                    UserAvatar = myAccount.Avatar,
                    UserGender = myAccount.Gender,
                    ProfileId = otherAccount.Id,
                    ProfileName = otherAccount.Username,
                    ProfileAvatar = otherAccount.Avatar,
                    ProfileGender = otherAccount.Gender,
                    DateAt = DateTime.Now,
                    Accepted = false,
                    RequestType = 2
                };
                CacheRequest(request);
                TriggerRequest(request);
            }
        }

        public static RequestData GetRequest(string myAccountId, string otherAccountId)
        {
            try
            {
                var callRequest = CacheController.Get<RequestData>("msgRequest_" + myAccountId + "_" + otherAccountId);
                if (callRequest != null)
                {
                    return callRequest;
                }
                else
                {
                    //From MySQL
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM requests WHERE ((userId = @userId AND profileId = @profileId) OR (userId = @profileId AND profileId = @userId)) AND requestType=2 LIMIT 1", new QueryParameter("userId", myAccountId), new QueryParameter("profileId", otherAccountId));
                    if (rows.Length == 1)
                    {
                        callRequest = RowToRequest(rows[0]);
                        if (callRequest != null)
                        {
                            return callRequest;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static RequestData RowToRequest(DatabaseRow row)
        {
            if (row == null || row.Count != 6) return null;
            try
            {
                var user = AccountsController.GetAccountById(row["userId"].ToString());
                var otherAccount = AccountsController.GetAccountById(row["profileId"].ToString());
                if (user == null || otherAccount == null) return null;
                DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());

                return new RequestData()
                {
                    Id = row["id"].ToString(),
                    UserId = user.Id,
                    UserName = user.Username,
                    UserAvatar = user.Avatar,
                    UserGender = user.Gender,
                    ProfileId = otherAccount.Id,
                    ProfileName = otherAccount.Username,
                    ProfileGender = otherAccount.Gender,
                    DateAt = dateAt,
                    Accepted = Convert.ToInt32(row["accepted"].ToString()) == 1,
                    RequestType = Convert.ToInt32(row["requestType"].ToString())
                };
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static void SetRequestResult(string myAccountId, string otherAccountId, bool accepted)
        {
            var request = GetRequest(otherAccountId, myAccountId);
            if (request == null) return;

            if (myAccountId != request.ProfileId) return;//Only the person who received the request can accept or decline!

            if (accepted)
            {
                //Accepted Call Request
                DeleteMsgRequest(request);
                request.Accepted = true;
                CacheRequest(request);
                //Trigger Notification, call request accepted.
                NotificationsController.TriggerNotification(request.ProfileId, request.UserId, NotificationTypes.MsgRequestAccepted);
            }
            else
            {
                //Declined Call Request
                DeleteMsgRequest(request);
                //Trigger Notification, call request declined
                NotificationsController.TriggerNotification(request.ProfileId, request.UserId, NotificationTypes.MsgRequestDeclined);
            }
        }

        public static void DeleteMsgRequest(RequestData request)
        {
            try
            {
                NetworkServer.database.ExecuteNonQuery("DELETE FROM requests WHERE id = @msgRequestId", new QueryParameter("msgRequestId", request.Id));
                DeleteCacheRequest(request);
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        public static void TriggerRequest(RequestData request)
        {
            bool triggered1 = false;
            bool triggered2 = false;

            var user1Session = SessionsController.GetSessionByAccountId(request.UserId);
            if (user1Session != null)
            {
                using (NetworkWriter writer = new NetworkWriter())
                {
                    writer.Write(ProtoSerializer.Serialize(request));
                    NetworkServer.GetConnectionServiceByID(user1Session.ConnectionId)?.SendReply(ClientTags.REQUEST, 0, writer);
                    triggered1 = true;
                }
            }
            var user2Session = SessionsController.GetSessionByAccountId(request.ProfileId);
            if (user2Session != null)
            {
                using (NetworkWriter writer = new NetworkWriter())
                {
                    writer.Write(ProtoSerializer.Serialize(request));
                    NetworkServer.GetConnectionServiceByID(user2Session.ConnectionId)?.SendReply(ClientTags.REQUEST, 0, writer);
                    triggered2 = true;
                }
            }

            if (!triggered1 || !triggered2)
            {
                //Trigger On Master Server.
                MainServer.NotifyMapRequest(request);
            }
        }

        public static void CacheRequest(RequestData request)
        {
            if (request == null) return;
            CacheController.Set("msgRequest_" + request.Id, request, 60 * 15);
            CacheController.Set("msgRequest_" + request.UserId + "_" + request.ProfileId, request, 60 * 15);
            CacheController.Set("msgRequest_" + request.ProfileId + "_" + request.UserId, request, 60 * 15);
            RequestsController.CacheRequest(request);
        }

        public static void DeleteCacheRequest(RequestData request)
        {
            if (request == null) return;
            CacheController.Remove("msgRequest_" + request.Id);
            CacheController.Remove("msgRequest_" + request.UserId + "_" + request.ProfileId);
            CacheController.Remove("msgRequest_" + request.ProfileId + "_" + request.UserId);
            RequestsController.DeleteCacheRequest(request);
        }

        //
        public static void CacheConversation(ConversationData conversation)
        {
            if (conversation == null) return;
            CacheController.Set("conversation_" + conversation.Id, conversation, 1800);
            CacheController.Set("conversation_" + conversation.User1Id + "_" + conversation.User2Id, conversation, 1800);
            CacheController.Set("conversation_" + conversation.User2Id + "_" + conversation.User1Id, conversation, 1800);
        }

        private static void CacheMessage(MessageData message)
        {
            if (message == null) return;
            CacheController.Set("message_" + message.Id, message, 1800);
        }

        public static string GetConversationId(string myAccountId, string otherAccountId)
        {
            try
            {
                var conversation = CacheController.Get<ConversationData>("conversation_" + myAccountId + "_" + otherAccountId);
                if (conversation != null)
                {
                    CacheConversation(conversation);//Refresh cache expire timer!
                    return conversation.Id;
                }
                else
                {
                    //Select it, if it doesnt exist, create it!
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM conversations WHERE (user1 = @accountId OR user2 = @accountId) AND (user1 = @otherId OR user2 = @otherId) ORDER BY id DESC LIMIT 1", new QueryParameter("accountId", myAccountId), new QueryParameter("otherId", otherAccountId));
                    if (rows.Length == 1)
                    {
                        conversation = ConversationRowToData(rows[0]);
                        if (conversation != null)
                        {
                            CacheConversation(conversation);
                            return conversation.Id;
                        }
                    }
                    //Conversation doesnt exist lets, Create it!
                    var returnId = "";
                    lock (NetworkServer.database)
                    {
                        returnId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO conversations (user1, user2, startedAt) VALUES (@user1, @user2, NOW()); SELECT LAST_INSERT_ID();", new QueryParameter("user1", myAccountId), new QueryParameter("user2", otherAccountId))).ToString();
                    }
                    conversation = new ConversationData()
                    {
                        Id = returnId,
                        LastMessage = "",
                        LastMessageFromMe = false,
                        LastMessageSeen = false,
                        User1Avatar = "",
                        User1Id = "",
                        User1Name = "",
                        User2Avatar = "",
                        User2Id = "",
                        User2Name = ""
                    };
                    CacheConversation(conversation);
                    return returnId;
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static ConversationData GetConversationById(string conversationId)
        {
            try
            {
                var conversation = CacheController.Get<ConversationData>("conversation_" + conversationId);
                if (conversation != null)
                {
                    CacheConversation(conversation);//Refresh Cache
                    return conversation;
                }
                else
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM conversations WHERE id = @conversationId LIMIT 1", new QueryParameter("conversationId", conversationId));
                    if (rows.Length == 1)
                    {
                        conversation = ConversationRowToData(rows[0]);
                        if (conversation != null)
                        {
                            CacheConversation(conversation);
                            return conversation;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static MessageData SendMessage(string myAccountId, string otherAccountId, string message, bool isPremium, string attachmentId = null)
        {
            if (!isPremium)
            {
                var request = GetRequest(myAccountId, otherAccountId);
                if (request != null)
                {
                    CacheRequest(request);
                }
                else
                {
                    var myAccount = AccountsController.GetAccountById(myAccountId);
                    var otherAccount = AccountsController.GetAccountById(otherAccountId);
                    if (myAccount != null && otherAccount != null)
                    {
                        SendRequest(myAccount, otherAccount);
                    }
                    return null;
                }
            }
            else
            {
                var request = GetRequest(myAccountId, otherAccountId);
                if (request == null)
                {
                    var myAccount = AccountsController.GetAccountById(myAccountId);
                    var otherAccount = AccountsController.GetAccountById(otherAccountId);
                    request = new RequestData()
                    {
                        Id = "0",
                        Accepted = true,
                        DateAt = DateTime.Now,
                        RequestType = 2,
                        UserId = myAccount.Id,
                        UserName = myAccount.Username,
                        UserAvatar = myAccount.Avatar,
                        UserGender = myAccount.Gender,
                        ProfileId = otherAccount.Id,
                        ProfileName = otherAccount.Username,
                        ProfileAvatar = otherAccount.Avatar,
                        ProfileGender = otherAccount.Gender
                    };
                    CacheRequest(request);
                }
                else
                {
                    CacheRequest(request);
                }
            }
            var conversationId = GetConversationId(myAccountId, otherAccountId);
            if (!string.IsNullOrEmpty(conversationId))
            {
                var messageId = "";
                lock (NetworkServer.database)
                {
                    messageId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO messages (conversationId, fromId, toId, message, sentAt, attachmentId) VALUES(@conversationId, @fromId, @toId, @message, NOW(), @attachmentId); SELECT LAST_INSERT_ID();",
                        new QueryParameter("conversationId", conversationId),
                        new QueryParameter("fromId", myAccountId),
                        new QueryParameter("toId", otherAccountId),
                        new QueryParameter("message", message),
                        new QueryParameter("attachmentId", attachmentId))).ToString();
                    //messageId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("SELECT LAST_INSERT_ID();")).ToString();
                }
                var messageData = new MessageData()
                {
                    Id = messageId,
                    ConversationId = conversationId,
                    FromId = myAccountId,
                    ToId = otherAccountId,
                    Message = message,
                    When = DateTime.Now,
                    SeenAt = null,
                    AttachmentId = attachmentId
                };

                var conversation = GetConversationById(conversationId);
                conversation.LastMessage = message;
                conversation.LastMessageFromMe = true;
                conversation.LastMessageSeen = false;
                CacheConversation(conversation);

                CacheMessage(messageData);
                MainServer.NotifiyMessage(messageData);
                return messageData;
            }
            else
            {
                //Unexpected error occured.
            }
            return null;
        }

        public static MessageData GetMessageById(string messageId)
        {
            var messageData = CacheController.Get<MessageData>("message_" + messageId);
            if (messageData == null)
            {
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM messages WHERE id = @messageId LIMIT 1", new QueryParameter("messageId", messageId));
                if (rows.Length == 1)
                {
                    messageData = MessageRowToData(rows[0]);
                    CacheMessage(messageData);
                }
            }
            else CacheMessage(messageData);//Refresh cache expire timer!
            return messageData;
        }

        public static void SetMessageHasSeen(string accountId, string messageId)
        {
            try
            {
                var message = GetMessageById(messageId);
                if (message == null)
                    return;
                if (message.ToId == accountId)
                {
                    NetworkServer.database.ExecuteNonQuery("UPDATE messages SET seenAt = NOW() WHERE id = @messageId AND toId = @accountId", new QueryParameter("messageId", messageId), new QueryParameter("accountId", accountId));
                    MainServer.NotifyMessageSeen(message);
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        public static MessageData GetLastMessageOfConversation(string conversationId)
        {
            var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM messages WHERE conversationId = @conversationId ORDER BY id DESC LIMIT 1", new QueryParameter("conversationId", conversationId));

            if (rows.Length == 1)
            {
                return MessageRowToData(rows[0]);
            }

            return null;
        }

        public static List<ConversationData> GetConversations(string myAccountId, int page = 1)
        {
            var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM conversations WHERE (user1 = @accountId OR user2 = @accountId) ORDER BY id DESC LIMIT 25 OFFSET @page", new QueryParameter("accountId", myAccountId), new QueryParameter("page", page - 1));
            List<ConversationData> returnList = new List<ConversationData>();
            foreach (var row in rows)
            {
                var conversation = ConversationRowToData(row);
                if (conversation != null)
                {
                    var user1 = AccountsController.GetAccountById(conversation.User1Id);
                    if (user1 != null)
                    {
                        conversation.User1Name = user1.Username;
                        conversation.User1Avatar = user1.Avatar;
                        conversation.User1Gender = user1.Gender;
                    }
                    var user2 = AccountsController.GetAccountById(conversation.User2Id);
                    if (user2 != null)
                    {
                        conversation.User2Name = user2.Username;
                        conversation.User2Avatar = user2.Avatar;
                        conversation.User2Gender = user2.Gender;
                    }
                    var lastMessage = GetLastMessageOfConversation(conversation.Id);
                    if (lastMessage != null)
                    {
                        conversation.LastMessage = lastMessage.Message;
                        conversation.LastMessageFromMe = lastMessage.FromId == myAccountId;
                        conversation.LastMessageSeen = lastMessage.SeenAt != null;
                        //
                        returnList.Add(conversation);
                    }
                }
            }
            return returnList;
        }

        private static MessageData MessageRowToData(DatabaseRow row)
        {
            if (row.Count == 8)
            {
                try
                {
                    DateTime? seenAt = (string.IsNullOrEmpty(row["seenAt"].ToString()) || row["seenAt"].ToString() == "0000-00-00 00:00:00") ? null : (DateTime?)DateTime.Parse(row["seenAt"].ToString());

                    DateTime sentAt = DateTime.Parse(row["sentAt"].ToString());
                    return new MessageData()
                    {
                        Id = row["id"].ToString(),
                        ConversationId = row["conversationId"].ToString(),
                        FromId = row["fromId"].ToString(),
                        ToId = row["toId"].ToString(),
                        Message = row["message"].ToString(),
                        SeenAt = seenAt,
                        When = sentAt,
                        AttachmentId = row["attachmentId"].ToString()
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        private static ConversationData ConversationRowToData(DatabaseRow row)
        {
            if (row.Count == 4)
            {
                try
                {
                    return new ConversationData()
                    {
                        Id = row["id"].ToString(),
                        User1Id = row["user1"].ToString(),
                        User2Id = row["user2"].ToString()
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        public static List<MessageData> GetMessages(string conversationId, string accountId, int page = 1)
        {
            var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM ( SELECT * FROM messages WHERE conversationId = @conversationId AND (fromId = @accountId OR toId = @accountId) ORDER BY id DESC LIMIT 25 OFFSET @page ) sub ORDER BY id ASC", new QueryParameter("accountId", accountId), new QueryParameter("conversationId", conversationId), new QueryParameter("page", page - 1));
            List<MessageData> returnList = new List<MessageData>();
            foreach (var row in rows)
            {
                var messageData = MessageRowToData(row);
                if (row != null) returnList.Add(messageData);
            }
            return returnList;
        }
    }
}