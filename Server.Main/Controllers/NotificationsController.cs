﻿using Network;
using Network.Storage;
using Server.Shared.Controllers;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Server.Main.Controllers
{
    public static class NotificationsController
    {
        public static void TriggerNotification(string accountId, string otherAccountId, NotificationTypes type)
        {
            try
            {
                var insertedId = "";
                lock (NetworkServer.database)
                {
                    insertedId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO notifications (userId, fromId, type, dateAt) VALUES (@userId, @fromId, @type, NOW()); SELECT LAST_INSERT_ID();", new QueryParameter("userId", otherAccountId), new QueryParameter("fromId", accountId), new QueryParameter("type", (int)type))).ToString();
                }
                if (!string.IsNullOrEmpty(insertedId))
                {
                    var fromUser = AccountsController.GetAccountById(accountId);
                    var notification = new NotificationData()
                    {
                        Id = insertedId,
                        ProfileId = otherAccountId,
                        Type = type,
                        When = DateTime.Now,
                        FromId = accountId,
                        FromName = fromUser.Username,
                        FromAvatar = fromUser.Avatar,
                        FromGender = fromUser.Gender
                    };
                    CacheNotification(notification);
                    var notificationsArray = CacheController.Get<NotificationData[]>("notifications_" + otherAccountId);
                    if (notificationsArray != null)
                    {
                        var list = notificationsArray.ToList();
                        list.Add(notification);
                        CacheController.Set("notifications_" + otherAccountId, list.ToArray(), 3600);
                    }
                    //Trigger it!
                    MainServer.NotifyNotification(notification);
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        public static void CacheNotification(NotificationData notification)
        {
            if (notification == null) return;
            CacheController.Set("notification_" + notification.Id, notification, 3600);
        }

        public static void DeleteCacheNotification(NotificationData notification)
        {
            if (notification == null) return;
            CacheController.Remove("notification_" + notification.Id);
            //Remove from list
            try
            {
                var notificationsArray = CacheController.Get<NotificationData[]>("notifications_" + notification.ProfileId);
                if (notificationsArray != null)
                {
                    var list = notificationsArray.ToList();
                    var notificationFound = list.FirstOrDefault(x => x.Id == notification.Id);
                    if (notificationFound != null)
                    {
                        list.Remove(notificationFound);
                        CacheController.Set("notifications_" + notification.ProfileId, list.ToArray(), 3600);
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        public static NotificationData GetNotificationById(string notificationId)
        {
            try
            {
                var notification = CacheController.Get<NotificationData>("notification_" + notificationId);
                if (notification != null)
                {
                    return notification;
                }
                else
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM notifications WHERE id = @notificationId LIMIT 1", new QueryParameter("notificationId", notificationId));
                    if (rows.Length == 1)
                    {
                        notification = DatabaseRowToNotificationData(rows[0]);
                        return notification;
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static void DeleteNotification(string accountId, string notificationId)
        {
            var notification = GetNotificationById(notificationId);
            if (notification == null) return;

            if (notification.ProfileId != accountId) return;

            DeleteCacheNotification(notification);
            lock (NetworkServer.database)
            {
                NetworkServer.database.ExecuteNonQuery("DELETE FROM notifications WHERE id = @notificationId", new QueryParameter("notificationId", notificationId));
            }
        }

        public static List<NotificationData> GetNotifications(string accountId)
        {
            try
            {
                var notificationsArray = CacheController.Get<NotificationData[]>("notifications_" + accountId);
                if (notificationsArray != null)
                {
                    CacheController.Set("notifications_" + accountId, notificationsArray, 3600);//Refresh cache!
                    return notificationsArray.ToList();
                }
                else
                {
                    //Get followings from MySQL
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM notifications WHERE userId = @profileId ORDER BY id DESC", new QueryParameter("profileId", accountId));
                    var returnList = new List<NotificationData>();
                    foreach (var row in rows)
                    {
                        var notification = DatabaseRowToNotificationData(row);
                        if (notification != null)
                        {
                            returnList.Add(notification);
                            CacheNotification(notification);
                        }
                    }
                    if (rows.Length > 0)
                    {
                        CacheController.Set("notifications_" + accountId, returnList.ToArray(), 3600);
                    }
                    return returnList;
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return default(List<NotificationData>);
        }

        public static NotificationData DatabaseRowToNotificationData(DatabaseRow row)
        {
            if (row == null) return null;
            try
            {
                if (row.Count == 5)
                {
                    var fromId = row["fromId"].ToString();
                    var userId = row["userId"].ToString();

                    var fromUser = AccountsController.GetAccountById(fromId);
                    if (fromUser == null) return null;

                    return new NotificationData()
                    {
                        Id = row["id"].ToString(),
                        FromAvatar = fromUser.Avatar,
                        FromGender = fromUser.Gender,
                        FromId = fromId,
                        FromName = fromUser.Username,
                        Type = (NotificationTypes)Convert.ToInt32(row["type"].ToString()),
                        When = DateTime.Parse(row["dateAt"].ToString()),
                        ProfileId = userId
                    };
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }
    }
}