﻿using Network;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialAPP
{
    public class NetworkController
    {
        #region Variables

        private static NetworkConnection Connection = new NetworkConnection();
		//#if DEBUG
		//        public static string ServerHostname = "149.56.25.176";
		//        public static string WebServerAddress = "http://149.56.25.176/social/";
		//#else
		public static string ServerHostname = "149.56.25.176";
		public static string WebServerAddress = "http://curssor.com/";
		//#endif
		public static int ServerPort = 4297;
        public const string Version = "1.0.1";
        private static bool shouldBeConnected;
        private static bool isConnecting;
        private static bool changePictureToo = false;
        public static string AccountId = "";
        public static string AccountName = "";
        public static string AccountAvatar = "";
        public static int AccountGender = 0;
        public static bool AccountMembership = false;
        public static bool IsLoggedIn;
        private bool preventSending;
        private static Timer receiveTimer;
        private static volatile int dldnow;
        public static double Latitude;
        public static double Longitude;

        #endregion Variables

        #region Constructors

        public NetworkController()
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    Receive(dldnow);
                }
            });
            //receiveTimer = new Timer(new TimerCallback(Receive), dldnow, 100, 100);
            Connection.onData += OnReceiveData;
            Connection.onPlayerDisconnected += onDisconnected;
        }

        public NetworkController(string serverHostname, int serverPort)
        {
            ServerHostname = serverHostname;
            ServerPort = serverPort;
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    if (IsConnected) Connection.Receive();
                }
            });
            Connection.onData += OnReceiveData;
            Connection.onPlayerDisconnected += onDisconnected;
        }

        #endregion Constructors

        #region Functions

        public void Receive(object obj)
        {
            try
            {
                if (IsConnected)
                {
                    Connection.Receive();
                    //EventsController.Log?.Invoke("Receiving data", null);
                }
                else
                {
                    if (shouldBeConnected)
                    {
                        shouldBeConnected = false;
                        Connect();
                    }
                }
            }
            catch (Exception ex)
            {
                EventsController.Log?.Invoke(ex.StackTrace, null);
                //throw ex;
            }
        }

        public static bool IsConnected { get { return Connection != null && Connection.isConnected; } }

        private void onDisconnected(ushort id)
        {
            IsLoggedIn = false;
            EventsController.OnDisconnected?.Invoke(null, null);
        }

        public bool Connect()
        {
            if (IsConnected)
            {
                return true;
            }
            try
            {
                if (isConnecting) return true;
                isConnecting = true;
                Connection.Connect(ServerHostname, ServerPort);
                if (IsConnected)
                {
                    EventsController.OnConnected?.Invoke(null, null);
                    GetVersion();
                    isConnecting = false;
                    shouldBeConnected = true;
                    return true;
                }
            }
            catch (Exception ex)
            {
				Console.WriteLine(ex.StackTrace);
                //Interface.LogError(ex.StackTrace);
            }
            return false;
        }

        public bool Disconnect()
        {
            try
            {
                Connection.Disconnect();
                EventsController.OnDisconnected?.Invoke(null, null);
                return true;
            }
            catch (Exception ex)
            {
                //Interface.LogError(ex.StackTrace);
            }
            return false;
        }

        private void OnReceiveData(byte tag, ushort subject, object data)
        {
            switch (tag)
            {
                #region Authentication

                case ClientTags.GET_VERSION:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var version = reader.ReadString();
                            EventsController.OnGetVersion?.Invoke(version, null);
                        }
                        catch (Exception ex)
                        {
                            //Interface.LogError(ex.StackTrace);
                        }
                    }
                }
                break;

                case ClientTags.DO_LOGIN:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        int result = reader.ReadInt32();
                        if (result == 0)
                        {
                            IsLoggedIn = true;
                            AccountId = reader.ReadString();
                            AccountName = reader.ReadString();
                            AccountAvatar = reader.ReadString();
                            AccountGender = reader.ReadInt32();
                            AccountMembership = reader.ReadBoolean();

                            var bytes = reader.ReadBytes();
                            var requests = ProtoSerializer.Deserialize<RequestData[]>(bytes);

                            EventsController.OnGetAccountInfos?.Invoke(new object[] { AccountId, AccountName, AccountAvatar, AccountGender, AccountMembership }, null);
                            EventsController.OnGetRequests(requests.ToList(), null);
                            LoadConversations(1);
                            LoadNotifications();
                            GetRecentPosts();
                        }
                        EventsController.OnGetLoginResult?.Invoke(result, null);
                        preventSending = false;
                    }
                }
                break;

                case ClientTags.DO_LOGOUT:
                break;

                case ClientTags.DO_REGISTER:
                using (NetworkReader reader = (NetworkReader)data)
                {
                    int result = reader.ReadInt32();

                    EventsController.OnGetRegisterResult?.Invoke(result, null);
                    preventSending = false;
                }
                break;

                case ClientTags.DO_RECOVER:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        int result = reader.ReadInt32();

                        EventsController.OnGetRecoverResult?.Invoke(result, null);
                        preventSending = false;
                    }
                }
                break;

                case ClientTags.DISCONNECTED_BY_SERVER:
                try
                {
                    Connection.Disconnect();
                }
                catch (Exception) { }

                onDisconnected(0);
                break;

                #endregion Authentication

                #region Edit Account

                case ClientTags.GETEDIT_ACCOUNT:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        string email = reader.ReadString();
                        string username = reader.ReadString();
                        string firstname = reader.ReadString();
                        string lastname = reader.ReadString();
                        string dob_d = reader.ReadString();
                        string dob_m = reader.ReadString();
                        string dob_y = reader.ReadString();

                        EventsController.OnGetEditAccount?.Invoke(new object[] {
                                email,
                                username,
                                firstname,
                                lastname,
                                dob_d,
                                dob_m,
                                dob_y
                            }, null);
                    }
                }
                break;

                case ClientTags.DO_SAVEACCOUNT:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        var result = reader.ReadInt32();
                        EventsController.OnGetEditAccountResult?.Invoke(result, null);
                    }
                }
                break;

                #endregion Edit Account

                case ClientTags.UPDATE_POS:

                break;

                #region View Profile

                case ClientTags.GET_PROFILE_DATA:
                using (NetworkReader reader = (NetworkReader)data)
                {
                    if (reader.ReadInt32() == 0)
                    {
                        EventsController.OnReturnToHome?.Invoke(null, null);
                    }
                    else
                    {
                        string id = reader.ReadString();
                        string username = reader.ReadString();
                        string picture2 = reader.ReadString();
                        string city2 = reader.ReadString();
                        string job2 = reader.ReadString();
                        string activities2 = reader.ReadString();
                        string description2 = reader.ReadString();
                        string lookingFor2 = reader.ReadString();
                        string lookingForDescription2 = reader.ReadString();
                        int gender2 = reader.ReadInt32();
                        int age = reader.ReadInt32();
                        bool followed = reader.ReadBoolean();
                        ProfileData profileData = new ProfileData
                        {
                            Id = id,
                            Username = username,
                            Picture = picture2,
                            City = city2,
                            Job = job2,
                            Activities = activities2,
                            Description = description2.Replace("\n", "\\n"),
                            LookingFor = lookingFor2,
                            LookingForDescription = lookingForDescription2.Replace("\n", "\\n"),
                            Gender = gender2,
                            Age = age,
                            Followed = followed
                        };

                        //Followers
                        var followersBytes = reader.ReadBytes();
                        var followers = ProtoSerializer.Deserialize<List<FollowData>>(followersBytes);
                        EventsController.OnGetProfileFollowers?.Invoke(followers, null);

                        //Followings
                        var followingsBytes = reader.ReadBytes();
                        var followings = ProtoSerializer.Deserialize<List<FollowData>>(followingsBytes);
                        EventsController.OnGetProfileFollowings?.Invoke(followings, null);

                        //Posts
                        var postsBytes = reader.ReadBytes();
                        var posts = ProtoSerializer.Deserialize<List<PostData>>(postsBytes);
                        EventsController.OnGetProfilePosts(posts, null);
                        foreach (var post in posts)
                        {
                            GetPostComments(post.Id);
                        }

                        profileData.PhotosCount = reader.ReadInt32();
                        profileData.IsBlocked = reader.ReadBoolean();

                        //Trigger this at the end!
                        EventsController.OnGetProfileData?.Invoke(profileData, null);
                    }
                }
                break;

                case ClientTags.GETPROFILE_PHOTOS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        var photosBytes = reader.ReadBytes();

                        var photo = ProtoSerializer.Deserialize<PostData[]>(photosBytes);

                        EventsController.OnGetProfilePictures?.Invoke(photo.ToList(), null);
                    }
                }
                break;

                #endregion View Profile

                #region Edit Profile

                case ClientTags.GET_ACCOUNT_DATA:
                using (NetworkReader reader = (NetworkReader)data)
                {
                    string picture = reader.ReadString();
                    string city = reader.ReadString();
                    string job = reader.ReadString();
                    string activities = reader.ReadString();
                    string description = reader.ReadString();
                    string lookingFor = reader.ReadString();
                    string lookingForDescription = reader.ReadString();
                    int gender = reader.ReadInt32();
                    ProfileData profileData = new ProfileData
                    {
                        Picture = picture,
                        City = city,
                        Job = job,
                        Activities = activities,
                        Description = description.Replace("\n", "\\n"),
                        LookingFor = lookingFor,
                        LookingForDescription = lookingForDescription.Replace("\n", "\\n"),
                        Gender = gender
                    };
                    EventsController.OnGetEditProfileData?.Invoke(profileData, null);
                }
                break;

                case ClientTags.EDIT_PROFILE_DATA:
                if (!changePictureToo)
                {
                    EventsController.OnProfileEdited?.Invoke(null, null);
                }
                changePictureToo = false;
                break;

                case ClientTags.EDIT_PROFILE_PICTURE:
                using (NetworkReader reader = (NetworkReader)data)
                {
                    string uploadToken = reader.ReadString();
                    EventsController.OnGetUploadProfilePictureToken?.Invoke(uploadToken, null);
                }
                break;

                #endregion Edit Profile

                #region Search

                case ClientTags.SEARCH_PROFILES:
                using (NetworkReader reader = (NetworkReader)data)
                {
                    try
                    {
                        List<AccountData> key = ProtoSerializer.Deserialize<List<AccountData>>(reader.ReadBytes());
                        int value = reader.ReadInt32();
                        Dictionary<List<AccountData>, int> profiles = new Dictionary<List<AccountData>, int>();
                        profiles.Add(key, value);

                        EventsController.OnGetSearchResults?.Invoke(profiles, null);
                    }
                    catch (Exception ex)
                    {
                        EventsController.Log?.Invoke(ex.StackTrace, null);
                    }
                }
                break;

                #endregion Search

                #region Messages

                case ClientTags.GET_CONVERSATIONS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            List<ConversationData> conversations = ProtoSerializer.Deserialize<List<ConversationData>>(reader.ReadBytes());
                            EventsController.OnLoadConversationsResult?.Invoke(conversations, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.GET_MESSAGES:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            List<MessageData> messages = ProtoSerializer.Deserialize<List<MessageData>>(reader.ReadBytes());
                            EventsController.OnLoadMessagesResult?.Invoke(messages, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.RECEIVED_MESSAGE:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var result = reader.ReadInt32();
                            if (result == 0)
                            {
                                EventsController.OnReturnToHome?.Invoke(null, null);
                            }
                            else
                            {
                                MessageData message = ProtoSerializer.Deserialize<MessageData>(reader.ReadBytes());
                                ConversationData conversation = ProtoSerializer.Deserialize<ConversationData>(reader.ReadBytes());
                                EventsController.OnReceiveMessage?.Invoke(new object[] { message, conversation }, null);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.MESSAGE_SEEN:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            MessageData messages = ProtoSerializer.Deserialize<MessageData>(reader.ReadBytes());
                            EventsController.OnMessageSeen?.Invoke(messages, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.START_CONVERSATION:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var result = reader.ReadInt32();
                            if (result == 0)
                            {
                                EventsController.OnReturnToHome?.Invoke(null, null);
                            }
                            else
                            {
                                ConversationData conversation = ProtoSerializer.Deserialize<ConversationData>(reader.ReadBytes());
                                EventsController.OnConversationStarted?.Invoke(conversation, null);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                #endregion Messages

                #region Notifications

                case ClientTags.GET_NOTIFICATIONS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            List<NotificationData> notifications = ProtoSerializer.Deserialize<List<NotificationData>>(reader.ReadBytes());
                            EventsController.OnGetNotifications?.Invoke(notifications, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.RECEIVED_NOTIFICATION:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            NotificationData notification = ProtoSerializer.Deserialize<NotificationData>(reader.ReadBytes());
                            EventsController.OnGetNotification?.Invoke(notification, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                #endregion Notifications

                #region Posts

                case ClientTags.CREATE_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var postBytes = reader.ReadBytes();
                            var post = ProtoSerializer.Deserialize<PostData>(postBytes);

                            if (post != null)
                            {
                                //Post created successfully
                                EventsController.OnGetPost?.Invoke(post, null);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.DELETE_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var postId = reader.ReadString();
                            var result = reader.ReadBoolean();
                            if (!string.IsNullOrEmpty(postId) && result)
                            {
                                //Post deleted successfully
                                EventsController.OnGetPostDeleted?.Invoke(postId, null);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.GETUSER_POSTS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var bytes = reader.ReadBytes();
                            var posts = ProtoSerializer.Deserialize<PostData[]>(bytes);
                            EventsController.OnGetProfilePosts?.Invoke(posts.ToList(), null);
                            foreach (var post in posts)
                            {
                                GetPostComments(post.Id);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.GETRECENT_POSTS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var bytes = reader.ReadBytes();
                            var posts = ProtoSerializer.Deserialize<PostData[]>(bytes).ToList();
                            if (posts != null)
                            {
                                EventsController.OnGetPosts?.Invoke(posts.ToList(), null);
                                foreach (var post in posts)
                                {
                                    GetPostComments(post.Id);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.COMMENT_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var trigger = reader.ReadBoolean();
                            if (trigger)
                            {
                                var bytes = reader.ReadBytes();
                                var comment = ProtoSerializer.Deserialize<CommentData>(bytes);

                                EventsController.OnGetComment?.Invoke(comment, null);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.UNCOMMENT_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var commentId = reader.ReadString();
                            var result = reader.ReadBoolean();
                            if (result)
                                EventsController.OnGetCommentDeleted?.Invoke(commentId, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.GETCOMMENTS_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var bytes = reader.ReadBytes();
                            var comments = ProtoSerializer.Deserialize<CommentData[]>(bytes);
                            EventsController.OnGetComments?.Invoke(comments.ToList(), null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                #endregion Posts

                #region Payments

                case ClientTags.GET_TRANSACTIONS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var bytes = reader.ReadBytes();
                            var transactions = ProtoSerializer.Deserialize<TransactionData[]>(bytes);
                            EventsController.OnGetTransactions?.Invoke(transactions.ToList(), null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.VERIFY_MEMBERSHIP:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var result = reader.ReadBoolean();
                            AccountMembership = result;
                            EventsController.OnGetVerifyMembership?.Invoke(result, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.SEND_TRANSACTION:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var result = reader.ReadBoolean();
                            AccountMembership = result;
                            EventsController.OnGetPaymentResult?.Invoke(result, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;

                case ClientTags.PREMIUM_REQUIRED:
                {
                    EventsController.OnPremiumRequired(null, null);
                }
                break;

                #endregion Payments

                #region Blocks
                case ClientTags.GET_BLOCKEDUSERS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            List<BlockData> blockedUsers = ProtoSerializer.Deserialize<List<BlockData>>(reader.ReadBytes());
                            EventsController.OnGetBlockedUsers?.Invoke(blockedUsers, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;
                #endregion

                #region Calls
                case ClientTags.REQUEST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            RequestData request = ProtoSerializer.Deserialize<RequestData>(reader.ReadBytes());
                            EventsController.OnGetRequest?.Invoke(request, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;
                case ClientTags.DO_CALL:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            CallData call = ProtoSerializer.Deserialize<CallData>(reader.ReadBytes());
                            EventsController.OnStartCalling?.Invoke(call, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;
                case ClientTags.DO_CALL_RING:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            EventsController.OnCallRingTone?.Invoke(null, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;
                case ClientTags.RECEIVE_CALL:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            CallData call = ProtoSerializer.Deserialize<CallData>(reader.ReadBytes());
                            EventsController.OnReceiveCall?.Invoke(call, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;
                case ClientTags.START_CALL:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            CallData call = ProtoSerializer.Deserialize<CallData>(reader.ReadBytes());
                            var serverHostname = reader.ReadString();
                            var serverPort = reader.ReadInt32();
                            
                            EventsController.OnStartCall?.Invoke(new object[] {
                                call,
                                serverHostname,
                                serverPort
                            }, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;
                case ClientTags.END_CALL:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            CallData call = ProtoSerializer.Deserialize<CallData>(reader.ReadBytes());
                            EventsController.OnCallEnded?.Invoke(call, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;
                case ClientTags.VIDEO_ENABLED:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var enabled = reader.ReadBoolean();
                            EventsController.OnCallVideoChanged?.Invoke(enabled, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;
                case ClientTags.SYNC_AUDIO:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var bytes = reader.ReadBytes();
                            EventsController.OnSyncAudio?.Invoke(bytes, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;
                case ClientTags.SYNC_VIDEO:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var bytes = reader.ReadBytes();
                            EventsController.OnSyncVideo?.Invoke(bytes, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;
                #endregion

                #region Mapping
                case ClientTags.SEND_REQUEST_POS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var latitude = reader.ReadDouble();
                            var longitude = reader.ReadDouble();
                            EventsController.OnGetPosition?.Invoke(new object[] { latitude , longitude }, null);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace, null);
                        }
                    }
                }
                break;
                #endregion
            }
        }

        #endregion Functions

        #region Triggers

        //Send
        public void GetVersion()
        {
            Connection.SendMessageToServer(ClientTags.GET_VERSION, 0, null);
        }

        #region Account Authentication

        public void SendLogin(string username, string password)
        {
            if (preventSending) return;

            preventSending = true;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(Version);
                writer.Write(username);
                writer.Write(password);
                Connection.SendMessageToServer(ClientTags.DO_LOGIN, 0, writer);
            }
        }

        public void Logout()
        {
            Connection.SendMessageToServer(ClientTags.DO_LOGOUT, 0, null);
            IsLoggedIn = false;
            shouldBeConnected = false;
            Connection.Disconnect();
        }

        public void Register(string username, string password, string email, string firstname, string lastname, int dob_d, int dob_m, int dob_y, int gender)
        {
            if (preventSending) return;

            preventSending = true;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(username.ToLower());
                writer.Write(Encryption.SHA1Of(password));
                writer.Write(email);
                writer.Write(firstname);
                writer.Write(lastname);
                writer.Write(dob_d);
                writer.Write(dob_m);
                writer.Write(dob_y);
                writer.Write(gender);
                Connection.SendMessageToServer(ClientTags.DO_REGISTER, 0, writer);
            }
        }

        public void Recover(string userText)
        {
            if (!Connection.isConnected && shouldBeConnected) Connect();
            if (preventSending) return;
            preventSending = true;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userText);
                Connection.SendMessageToServer(ClientTags.DO_RECOVER, 0, writer);
            }
        }

        #endregion Account Authentication

        //Geolocation
        public void UpdateGeolocation(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(latitude);
                writer.Write(longitude);
                Connection.SendMessageToServer(ClientTags.UPDATE_POS, 0, writer);
            }
        }

        //Search
        public void SearchProfiles(string username, string gender, string minAge, string maxAge, string distance, int page = 1)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(username);
                writer.Write(gender);
                writer.Write(minAge);
                writer.Write(maxAge);
                writer.Write(distance);
                writer.Write(page);
                Connection.SendMessageToServer(ClientTags.SEARCH_PROFILES, 0, writer);
            }
        }

        #region Edit Profile

        public void LoadEditAccountInfo()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(0);
                Connection.SendMessageToServer(ClientTags.GETEDIT_ACCOUNT, 0, writer);
            }
        }

        public void DoSaveAccount(string email, string username, string password, string firstname, string lastname, string dob_d, string dob_m, string dob_y)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(email);
                writer.Write(username);
                writer.Write(Encryption.SHA1Of(password));
                writer.Write(firstname);
                writer.Write(lastname);
                writer.Write(Convert.ToInt32(dob_d));
                writer.Write(Convert.ToInt32(dob_m));
                writer.Write(Convert.ToInt32(dob_y));
                Connection.SendMessageToServer(ClientTags.DO_SAVEACCOUNT, 0, writer);
            }
        }

        public void EditProfilePicture()
        {
            changePictureToo = true;
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.EDIT_PROFILE_PICTURE, 0, writer);
            }
        }

        public void EditProfileInfo(string city, string job, string activities, string description, string lookingFor, string lookingForDescription)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(city);
                writer.Write(job);
                writer.Write(activities);
                writer.Write(description);
                writer.Write(lookingFor);
                writer.Write(lookingForDescription);
                Connection.SendMessageToServer(ClientTags.EDIT_PROFILE_DATA, 0, writer);
            }
        }

        public void UpdateProfilePicture()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.UPDATE_PROFILE_PICTURE, 0, null);
            }
        }

        public void LoadEditProfileInfo()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(0);
                Connection.SendMessageToServer(ClientTags.GET_ACCOUNT_DATA, 0, writer);
            }
        }

        #endregion Edit Profile

        #region View Profile

        public void LoadProfileData(string profileId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(profileId);
                Connection.SendMessageToServer(ClientTags.GET_PROFILE_DATA, 0, writer);
            }
        }

        public void GetProfilePhotos(string profileId, int page = 1)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(profileId);
                writer.Write(page);
                Connection.SendMessageToServer(ClientTags.GETPROFILE_PHOTOS, 0, writer);
            }
        }

        #endregion View Profile

        #region Messages

        public void LoadConversations(int page = 1)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(page);
                Connection.SendMessageToServer(ClientTags.GET_CONVERSATIONS, 0, writer);
            }
        }

        public void LoadMessages(string conversationId, int page = 1)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(conversationId);
                writer.Write(page);
                Connection.SendMessageToServer(ClientTags.GET_MESSAGES, 0, writer);
            }
        }

        public void SendMessage(string toAccountId, string message, string attachmentId = "")
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(toAccountId);
                writer.Write(message);
                writer.Write(attachmentId);

                Connection.SendMessageToServer(ClientTags.SEND_MESSAGE, 0, writer);
            }
        }

        public void SeenMessage(string messageId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(messageId);
                Connection.SendMessageToServer(ClientTags.MESSAGE_SEEN, 0, writer);
            }
        }

        public void StartConversation(string profileId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(profileId);
                Connection.SendMessageToServer(ClientTags.START_CONVERSATION, 0, writer);
            }
        }

        public void SetMsgRequestResult(string userId, bool result)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                writer.Write(result);
                Connection.SendMessageToServer(ClientTags.SET_MSG_REQUEST_RESULT, 0, writer);
            }
        }
        #endregion Messages

        #region Follows

        public void Follow(string accountId)
        {
            if (string.IsNullOrEmpty(accountId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(accountId);
                Connection.SendMessageToServer(ClientTags.FOLLOW, 0, writer);
            }
        }

        public void Unfollow(string accountId)
        {
            if (string.IsNullOrEmpty(accountId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(accountId);
                Connection.SendMessageToServer(ClientTags.UNFOLLOW, 0, writer);
            }
        }

        #endregion Follows

        #region Notifications

        public void LoadNotifications()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(AccountId);
                Connection.SendMessageToServer(ClientTags.GET_NOTIFICATIONS, 0, writer);
            }
        }

        public void Poke(string accountId)
        {
            if (string.IsNullOrEmpty(accountId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(accountId);
                Connection.SendMessageToServer(ClientTags.POKE, 0, writer);
            }
        }

        public void DeleteNotification(string notificationId)
        {
            if (string.IsNullOrEmpty(notificationId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(notificationId);
                Connection.SendMessageToServer(ClientTags.DELETE_NOTIFICATION, 0, writer);
            }
        }

        #endregion Notifications

        #region Posts

        public void GetRecentPosts()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.GETRECENT_POSTS, 0, writer);
            }
        }

        public void CreatePost(string content, string attachmentId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(content);
                writer.Write(attachmentId);
                Connection.SendMessageToServer(ClientTags.CREATE_POST, 0, writer);
            }
        }

        public void DeletePost(string postId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(postId);
                Connection.SendMessageToServer(ClientTags.DELETE_POST, 0, writer);
            }
        }

        public void CommentPost(string postId, string content, string attachmentId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(postId);
                writer.Write(content);
                writer.Write(attachmentId);
                Connection.SendMessageToServer(ClientTags.COMMENT_POST, 0, writer);
            }
        }

        public void DeleteComment(string commentId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(commentId);
                Connection.SendMessageToServer(ClientTags.UNCOMMENT_POST, 0, writer);
            }
        }

        public void GetPostComments(string postId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(postId);
                Connection.SendMessageToServer(ClientTags.GETCOMMENTS_POST, 0, writer);
            }
        }

        #endregion Posts

        #region Payments

        public void GetTransactions(int page = 1)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(page);
                Connection.SendMessageToServer(ClientTags.GET_TRANSACTIONS, 0, writer);
            }
        }

        public void VerifyMembership()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.VERIFY_MEMBERSHIP, 0, writer);
            }
        }

        public void SendPayment(string tokenId, int plan = 0)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(tokenId);
                writer.Write(plan);
                Connection.SendMessageToServer(ClientTags.SEND_TRANSACTION, 0, writer);
            }
        }

        #endregion Payments

        #region Blocks
        public void GetBlockedUsers()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.GET_BLOCKEDUSERS, 0, writer);
            }
        }
        public void BlockUser(string userId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                Connection.SendMessageToServer(ClientTags.BLOCK_USER, 0, writer);
            }
        }
        public void UnBlockUser(string userId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                Connection.SendMessageToServer(ClientTags.UNBLOCK_USER, 0, writer);
            }
        }
        #endregion

        #region Calls
        public void SendCall(string userId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                Connection.SendMessageToServer(ClientTags.REQUEST, 0, writer);
            }
        }
        public void SetCallRequestResult(string userId, bool result)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                writer.Write(result);
                Connection.SendMessageToServer(ClientTags.CALL_REQUEST_RESULT, 0, writer);
            }
        }
        
        public void SetCallResult(string callId, bool result)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(callId);
                writer.Write(result);
                Connection.SendMessageToServer(ClientTags.SET_CALL_RESULT, 0, writer);
            }
        }
        public void EndCall(string callId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(callId);
                Connection.SendMessageToServer(ClientTags.END_CALL, 0, writer);
            }
        }
        public void SetCallVideoEnabled(string callId, bool enabled)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(callId);
                writer.Write(enabled);
                Connection.SendMessageToServer(ClientTags.VIDEO_ENABLED, 0, writer);
            }
        }

        #endregion

        #region Mapping
        public void GetMapPosition(string userId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                Connection.SendMessageToServer(ClientTags.REQUEST_POS, 0, writer);
            }
        }
        public void SetMapRequestResult(string userId, bool result)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                writer.Write(result);
                Connection.SendMessageToServer(ClientTags.SET_REQUEST_POS_RESULT, 0, writer);
            }
        }
        #endregion

        #endregion Triggers
    }
}