﻿using System;
using AudioToolbox;

namespace SocialAPP.iOS
{
	public class AudioCapture2
	{
		InputAudioQueue audioQueue;
		readonly int bufferSize = 8000;

		#region IAudioStream implementation

		public event Action<byte[]> OnAudioReady;

		public readonly int SampleRate = 16000;

		public int ChannelCount
		{
			get
			{
				return 1;
			}
		}

		public int BitsPerSample
		{
			get
			{
				return 16;
			}
		}

		#endregion

		public bool Start()
		{
			return (audioQueue.Start() == AudioQueueStatus.Ok);
		}

		public void Stop()
		{
			audioQueue.Stop(true);
		}

		public bool Active
		{
			get
			{
				return audioQueue.IsRunning;
			}
		}

		public AudioCapture2()
		{
			Init();
		}

		void Init()
		{
			var audioFormat = new AudioStreamBasicDescription
			{
				SampleRate = SampleRate,
				Format = AudioFormatType.LinearPCM,
				FormatFlags = AudioFormatFlags.LinearPCMIsSignedInteger | AudioFormatFlags.LinearPCMIsPacked,
				FramesPerPacket = 1,
				ChannelsPerFrame = 1,
				BitsPerChannel = BitsPerSample,
				BytesPerPacket = 2,
				BytesPerFrame = 2,
				Reserved = 0
			};

			audioQueue = new InputAudioQueue(audioFormat);
			audioQueue.InputCompleted += QueueInputCompleted;

			var bufferByteSize = bufferSize * audioFormat.BytesPerPacket;

			IntPtr bufferPtr;
			for (var index = 0; index < 3; index++)
			{
				audioQueue.AllocateBufferWithPacketDescriptors(bufferByteSize, bufferSize, out bufferPtr);
				audioQueue.EnqueueBuffer(bufferPtr, bufferByteSize, null);
			}
		}

		/// <summary>
		/// Handles iOS audio buffer queue completed message.
		/// </summary>
		/// <param name='sender'>Sender object</param>
		/// <param name='e'> Input completed parameters.</param>
		void QueueInputCompleted(object sender, InputCompletedEventArgs e)
		{
			// return if we aren't actively monitoring audio packets
			if (!Active)
			{
				return;
			}

			var buffer = (AudioQueueBuffer)System.Runtime.InteropServices.Marshal.PtrToStructure(e.IntPtrBuffer, typeof(AudioQueueBuffer));
			if (OnAudioReady != null)
			{
				var send = new byte[buffer.AudioDataByteSize];
				System.Runtime.InteropServices.Marshal.Copy(buffer.AudioData, send, 0, (int)buffer.AudioDataByteSize);

				OnAudioReady(send);
			}

			var status = audioQueue.EnqueueBuffer(e.IntPtrBuffer, this.bufferSize, e.PacketDescriptions);

			if (status != AudioQueueStatus.Ok)
			{
				// todo: 
			}
		}

	}
}