// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SocialAPP.iOS
{
    [Register("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode("iOS Designer", "1.0")]
        UIKit.UIView MyCamView { get; set; }

        [Outlet]
        [GeneratedCode("iOS Designer", "1.0")]
        UIKit.UIWebView MyWebView { get; set; }

        [Outlet]
        [GeneratedCode("iOS Designer", "1.0")]
        UIKit.UIImageView OtherVideoView { get; set; }

        void ReleaseDesignerOutlets()
        {
            if (MyCamView != null)
            {
                MyCamView.Dispose();
                MyCamView = null;
            }

            if (MyWebView != null)
            {
                MyWebView.Dispose();
                MyWebView = null;
            }

            if (OtherVideoView != null)
            {
                OtherVideoView.Dispose();
                OtherVideoView = null;
            }
        }
    }
}