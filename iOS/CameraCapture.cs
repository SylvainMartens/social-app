﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AVFoundation;
using UIKit;
using Foundation;

namespace SocialAPP.iOS
{
    public class CameraCapture
    {
        AVCaptureSession captureSession;
        AVCaptureDeviceInput captureDeviceInput;
        AVCaptureStillImageOutput stillImageOutput;
        AVCaptureVideoPreviewLayer videoPreviewLayer;
        public UIView liveCameraStream;
        public CoreGraphics.CGRect MyFrame;
        public System.Action<byte[]> OnFrameReady;
        public bool IsRecording;


        public CameraCapture(UIView view, CoreGraphics.CGRect frame)
        {
            liveCameraStream = view;
            MyFrame = frame;
        }

        public void ToggleCamera()
        {
            if (IsRecording)
            {
                Stop();
            }
            else
            {
                Start();
            }
        }

        async Task<bool> Start()
        {
            IsRecording = true;
            await AuthorizeCameraUse();
            var ready = SetupLiveCameraStream();
            if (!ready)
            {
                IsRecording = false;
                return false;
            }
            //PhoneCamera?.Lock();
            var thread = new Thread(() => { CaptureFrames(); });
            thread.Start();
            return IsRecording;
        }

        void Stop()
        {
            IsRecording = false;
            Thread.Sleep(100);
        }

        public async Task CaptureFrames()
        {
            while (true)
            {
                if (!IsRecording)
                {
                    break;
                }
                if (liveCameraStream == null)
                {
                    IsRecording = false;
                    break;
                }
                var frame = await CaptureFrame();
                if (frame != null)
                {
                    OnFrameReady?.Invoke(frame);
                }
                /*try
                {
                    var bitmap = videoView.Bitmap;
                    var bitmapScalled = Bitmap.CreateScaledBitmap(bitmap, 100, 100, true);
                    bitmap.Recycle();

                    byte[] bitmapData;
                    using (var stream = new MemoryStream())
                    {
                        var ping = MainActivity.callClient.Ping;
                        var quality = 25;
                        if (quality <= 500)
                            quality = 25;
                        if (ping <= 300)
                            quality = 50;
                        if (ping <= 200)
                            quality = 75;
                        if (ping <= 100)
                            quality = 95;
                        if (ping <= 50)
                            quality = 100;
                        bitmapScalled.Compress(Bitmap.CompressFormat.Png, quality, stream);
                        bitmapData = stream.ToArray();
                    }
                    //var encoded = Base64.EncodeToString(bitmapData, Base64Flags.Default);
                    OnFrameReady?.Invoke(bitmapData);
                }
                catch (Exception e)
                {

                }*/

                Thread.Sleep(5);
            }
            captureSession.StopRunning();
            captureSession.Dispose();
        }



        async Task AuthorizeCameraUse()
        {
            try
            {
                var authorizationStatus = AVCaptureDevice.GetAuthorizationStatus(AVMediaType.Video);

                if (authorizationStatus != AVAuthorizationStatus.Authorized)
                {
                    await AVCaptureDevice.RequestAccessForMediaTypeAsync(AVMediaType.Video);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        public bool SetupLiveCameraStream()
        {
            try
            {
                captureSession = new AVCaptureSession()
                {
                    SessionPreset = AVCaptureSession.PresetMedium
                };

                // create a device input and attach it to the session
                var captureDevice = AVCaptureDevice.DefaultDeviceWithMediaType(AVMediaType.Video);
                if (captureDevice == null)
                {
                    Console.WriteLine("No captureDevice - this won't work on the simulator, try a physical device");
                    return false;
                }

                //var viewLayer = liveCameraStream.Layer;
                videoPreviewLayer = new AVCaptureVideoPreviewLayer(captureSession);
                liveCameraStream.Layer.AddSublayer(videoPreviewLayer);

                //var captureDevice = AVCaptureDevice.DefaultDeviceWithMediaType(AVMediaType.Video);
                ConfigureCameraForDevice(captureDevice);
                captureDeviceInput = AVCaptureDeviceInput.FromDevice(captureDevice);
                captureSession.AddInput(captureDeviceInput);

                var dictionary = new NSMutableDictionary();
                dictionary[AVVideo.CodecKey] = new NSNumber((int)AVVideoCodec.JPEG);
                stillImageOutput = new AVCaptureStillImageOutput();

                captureSession.AddOutput(stillImageOutput);
                captureSession.StartRunning();
                return true;
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.StackTrace);
            }
            return false;
        }

        void ConfigureCameraForDevice(AVCaptureDevice device)
        {
            var error = new NSError();
            if (device.IsFocusModeSupported(AVCaptureFocusMode.ContinuousAutoFocus))
            {
                device.LockForConfiguration(out error);
                device.FocusMode = AVCaptureFocusMode.ContinuousAutoFocus;
                device.UnlockForConfiguration();
            }
            else if (device.IsExposureModeSupported(AVCaptureExposureMode.ContinuousAutoExposure))
            {
                device.LockForConfiguration(out error);
                device.ExposureMode = AVCaptureExposureMode.ContinuousAutoExposure;
                device.UnlockForConfiguration();
            }
            else if (device.IsWhiteBalanceModeSupported(AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance))
            {
                device.LockForConfiguration(out error);
                device.WhiteBalanceMode = AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance;
                device.UnlockForConfiguration();
            }
        }

        async Task<byte[]> CaptureFrame()
        {
            var videoConnection = stillImageOutput.ConnectionFromMediaType(AVMediaType.Video);
            var sampleBuffer = await stillImageOutput.CaptureStillImageTaskAsync(videoConnection);

            var jpegImageAsNsData = AVCaptureStillImageOutput.JpegStillToNSData(sampleBuffer);
            var jpegAsByteArray = jpegImageAsNsData.ToArray();
            return jpegAsByteArray;
        }

        public void ToggleSwitchCamera()
        {
            var devicePosition = captureDeviceInput.Device.Position;
            if (devicePosition == AVCaptureDevicePosition.Front)
            {
                devicePosition = AVCaptureDevicePosition.Back;
            }
            else
            {
                devicePosition = AVCaptureDevicePosition.Front;
            }

            var device = GetCameraForOrientation(devicePosition);
            ConfigureCameraForDevice(device);

            captureSession.BeginConfiguration();
            captureSession.RemoveInput(captureDeviceInput);
            captureDeviceInput = AVCaptureDeviceInput.FromDevice(device);
            captureSession.AddInput(captureDeviceInput);
            captureSession.CommitConfiguration();
        }

        public AVCaptureDevice GetCameraForOrientation(AVCaptureDevicePosition orientation)
        {
            var devices = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video);
            foreach (var device in devices)
            {
                if (device.Position == orientation)
                {
                    return device;
                }
            }
            return null;
        }

    }
}