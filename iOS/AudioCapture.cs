﻿using System;
using AVFoundation;
using Foundation;
using System.IO;
using SocialAPP.Shared;
using System.Threading;

namespace SocialAPP.iOS
{
	public class AudioCapture
	{
		AVAudioRecorder _recorder;
		NSError _error;
		NSUrl _url;
		NSDictionary _settings;
		bool _isRecord = false;
		string _path;
		bool isRecording = false;
        bool micMuted = false;


		public Action<byte[]> OnAudioReady;

		public void Start()
		{
			if (isRecording) return;
			isRecording = true;
			Thread thread = new Thread(Capture);
			thread.Start();
		}

		public void Stop()
		{
			isRecording = false;
			Thread.Sleep(100);
		}

		public void ToggleMuteMic()
		{
			micMuted = !micMuted;
		}

		void Capture()
		{
			while (true)
			{
				if (!isRecording)
				{
					break;
				}
				var bytes = StopRecord();
				if (bytes != default(byte[]))
				{
					bytes = ProtoSerializer.Serialize(new AudioPacket()
					{
						Bitrate = 16000,
						Encoding = 1,
						Data = bytes
					});

					OnAudioReady?.Invoke(bytes);
				}
				Record("");
				Thread.Sleep(5);
			}
			_recorder.Stop();
			_recorder.Dispose();
		}


		public bool InitializeRecordService()
		{
			return InitializeRecordService("");
		}

		public bool InitializeRecordService(string path = "")
		{
			var audioSession = AVAudioSession.SharedInstance();
			var err = audioSession.SetCategory(AVAudioSessionCategory.PlayAndRecord);
			if (err != null)
			{
				Console.WriteLine("audioSession: {0}", err);
				return false;
			}
			err = audioSession.SetActive(true);
			if (err != null)
			{
				Console.WriteLine("audioSession: {0}", err);
				return false;
			}

			//Declare string for application temp path and tack on the file extension
			//if (path.Length == 0)
			{
				path = Path.GetTempPath();

			}

			string fileName = string.Format("audio{0}.wav", DateTime.Now.ToString("yyyyMMddHHmmss"));
			string audioFilePath = Path.Combine(path, fileName);

			Console.WriteLine("Audio File Path: " + audioFilePath);
			_path = audioFilePath;

			_url = NSUrl.FromFilename(audioFilePath);
			//set up the NSObject Array of values that will be combined with the keys to make the NSDictionary
			NSObject[] values = new NSObject[]
			{
				NSNumber.FromFloat (16000f), //Sample Rate
				NSNumber.FromInt32 ((int)AudioToolbox.AudioFormatType.LinearPCM), //AVFormat
                NSNumber.FromInt32 (1), //Channels, 1 = mono, 2 = stereo
                NSNumber.FromInt32 (16), //PCMBitDepth
                NSNumber.FromBoolean (false), //IsBigEndianKey
                NSNumber.FromBoolean (false) //IsFloatKey
            };

			//Set up the NSObject Array of keys that will be combined with the values to make the NSDictionary
			NSObject[] keys = new NSObject[]
			{
				AVAudioSettings.AVSampleRateKey,
				AVAudioSettings.AVFormatIDKey,
				AVAudioSettings.AVNumberOfChannelsKey,
				AVAudioSettings.AVLinearPCMBitDepthKey,
				AVAudioSettings.AVLinearPCMIsBigEndianKey,
				AVAudioSettings.AVLinearPCMIsFloatKey
			};

			//Set Settings with the Values and Keys to create the NSDictionary
			_settings = NSDictionary.FromObjectsAndKeys(values, keys);

			//Set recorder parameters
			_recorder = AVAudioRecorder.Create(_url, new AudioSettings(_settings), out _error);

			//Set Recorder to Prepare To Record

			return _recorder.PrepareToRecord();
		}

		public bool Record(string path)
		{

			//if (_recorder == null || _isRecord)
			//{
			//	return false;
			//}

			if (!InitializeRecordService(path))
			{
				return false;
			}

			_recorder.Record();
			_isRecord = true;
			return true;
		}

		public bool IsRecording()
		{
			return _isRecord;
		}

		public byte[] StopRecord()
		{
			if (_recorder == null || !_isRecord)
			{
				return default(byte[]);
			}
			_recorder.Stop();
			_isRecord = false;

			var bytes = default(byte[]);
			using (var streamReader = new StreamReader(_path))
			{
				using (var memstream = new MemoryStream())
				{
					streamReader.BaseStream.CopyTo(memstream);
					bytes = memstream.ToArray();
				}
			}

			File.Delete(_path);

			return bytes;

		}
	}
}