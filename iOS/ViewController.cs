﻿using cdeutsch;
using Foundation;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using UIKit;

namespace SocialAPP.iOS
{
    public partial class ViewController : UIViewController
    {
        #region Variables
        public static NetworkController Network = new NetworkController();

        public static UIImagePickerController ImagePicker;

        public static IGeolocator Locator;

        public static string Username = "";

        public static string Password = "";

        public static bool AutoLogin;

        public static string PictureToUpload = "";

        private static bool _workingLoaderDisplayed;

        private static double _prevLat;

        private static double _prevLon;

        public static string CurrentPage = "home";
        public static int notificationId;
        static bool isCalling;
        static int selectPictureFor;
        public static CallClient callClient;
        public static AudioCapture2 MicCapture;

        public static bool PostFromHome;
        //ImageView imageView;
        //TextureView videoView;

        public static CameraCapture CamCapture;


        #endregion
        public ViewController(IntPtr handle) : base(handle)
        {

        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            #region Events

            #region Authentication

            EventsController.OnGetVersion += OnGetVersionFromServer;
            EventsController.OnGetLoginResult += OnGetLoginResult;
            EventsController.OnGetRegisterResult += OnGetRegisterResult;
            EventsController.OnGetRecoverResult += OnGetRecoverResult;
            EventsController.OnGetAccountInfos += OnGetAccountInfos;
            EventsController.OnGetEditAccount += OnGetEditAccount;
            EventsController.OnGetEditAccountResult += OnGetEditAccountResult;

            #endregion Authentication

            EventsController.OnGetSearchResults += OnGetSearchResults;

            #region Edit Profile

            EventsController.OnProfileEdited += OnProfileEdited;
            EventsController.OnGetUploadProfilePictureToken += OnGetUploadProfilePictureToken;
            EventsController.OnGetEditProfileData += OnGetEditProfileData;

            #endregion Edit Profile

            EventsController.OnGetProfileData += OnGetProfileData;
            EventsController.OnGetProfilePictures += OnGetProfilePictures;

            EventsController.OnReturnToHome += OnReturnToHome;

            #region Messages Events

            EventsController.OnLoadConversationsResult += OnGetConversations;
            EventsController.OnLoadMessagesResult += OnGetMessages;
            EventsController.OnReceiveMessage += OnMessageReceived;
            EventsController.OnMessageSeen += OnMessageSeen;
            EventsController.OnConversationStarted += OnConversationStarted;

            #endregion Messages Events

            #region Follows Events

            EventsController.OnGetProfileFollowers += OnGetProfileFollowers;
            EventsController.OnGetProfileFollowings += OnGetProfileFollowings;

            #endregion Follows Events

            #region Notifications Events

            EventsController.OnGetNotifications += OnGetNotifications;
            EventsController.OnGetNotification += OnGetNotification;

            #endregion Notifications Events

            #region Posts Events

            EventsController.OnGetPost += OnGetPost;
            EventsController.OnGetPostDeleted += OnGetPostDeleted;
            EventsController.OnGetPosts += OnGetPosts;
            EventsController.OnGetProfilePosts += OnGetProfilePosts;
            EventsController.OnGetComments += OnGetComments;
            EventsController.OnGetComment += OnGetComment;
            EventsController.OnGetCommentDeleted += OnGetCommentDeleted;

            #endregion Posts Events

            #region Payments Events

            EventsController.OnGetTransactions += OnGetTransactions;
            EventsController.OnGetPaymentResult += OnGetPaymentResult;
            EventsController.OnGetVerifyMembership += OnGetVerifyMembership;
            EventsController.OnPremiumRequired += OnPremiumRequired;

            #endregion Payments Events

            #region Blocks
            EventsController.OnGetBlockedUsers += OnGetBlockedUsers;
            #endregion

            #region Calls
            EventsController.OnGetRequest += OnGetRequest;
            EventsController.OnGetRequests += OnGetRequests;
            EventsController.OnStartCalling += OnStartCalling;
            EventsController.OnReceiveCall += OnReceiveCall;
            EventsController.OnCallRingTone += OnCallRingTone;
            EventsController.OnStartCall += OnStartCall;
            EventsController.OnCallEnded += OnCallEnded;
            EventsController.OnCallVideoChanged += OnCallVideoChanged;
            #endregion

            #region Mapping
            EventsController.OnGetPosition += OnGetPosition;
            #endregion

            #endregion Events

            //View
            //WebView = new UIWebView(View.Bounds);

            JsBridge.EnableJsBridge();
            #region Browser Client Events
            MyWebView.AddEventListener("GetWebServerAddress", GetWebServerAddress);
            MyWebView.AddEventListener("Connect", Connect);
            MyWebView.AddEventListener("PageChanged", PageChanged);

            #region Authentication
            MyWebView.AddEventListener("Login", Login);
            MyWebView.AddEventListener("Logout", Logout);
            MyWebView.AddEventListener("LoadLoginSettings", LoadLoginSettings);
            MyWebView.AddEventListener("Register", Register);
            MyWebView.AddEventListener("Recover", Recover);
            #endregion

            #region Search
            MyWebView.AddEventListener("SearchProfiles", SearchProfiles);
            #endregion

            #region View Profile
            MyWebView.AddEventListener("LoadProfileData", LoadProfileData);
            MyWebView.AddEventListener("GetProfilePhotos", GetProfilePhotos);
            MyWebView.AddEventListener("Follow", Follow);
            MyWebView.AddEventListener("Unfollow", Unfollow);
            MyWebView.AddEventListener("Poke", Poke);
            #endregion

            MyWebView.AddEventListener("LoadEditAccountInfo", LoadEditAccountInfo);
            MyWebView.AddEventListener("DoSaveAccount", DoSaveAccount);

            MyWebView.AddEventListener("LoadEditProfileInfo", LoadEditProfileInfo);
            MyWebView.AddEventListener("DoEditProfilePicture", DoEditProfilePicture);
            MyWebView.AddEventListener("SaveEditProfile", SaveEditProfile);

            #region Posts
            MyWebView.AddEventListener("CreatePost", CreatePost);
            MyWebView.AddEventListener("DeletePost", DeletePost);
            MyWebView.AddEventListener("CommentPost", CommentPost);
            MyWebView.AddEventListener("DeleteComment", DeleteComment);
            MyWebView.AddEventListener("DoSelectPostPicture", DoSelectPostPicture);

            #endregion

            #region Messages
            MyWebView.AddEventListener("DoSelectMessagePicture", DoSelectMessagePicture);
            MyWebView.AddEventListener("SetMessageHasSeen", SetMessageHasSeen);
            MyWebView.AddEventListener("StartConversation", StartConversation);
            MyWebView.AddEventListener("SendMessage", SendMessage);
            #endregion

            #region Notifications
            MyWebView.AddEventListener("DeleteNotification", DeleteNotification);
            #endregion

            #region Payments
            MyWebView.AddEventListener("SendPayment", SendPayment);
            MyWebView.AddEventListener("GetTransactions", GetTransactions);
            #endregion

            #region Blocks
            MyWebView.AddEventListener("GetBlockedUsers", GetBlockedUsers);
            MyWebView.AddEventListener("BlockUser", BlockUser);
            MyWebView.AddEventListener("UnBlockUser", UnBlockUser);
            #endregion

            #region Calls
            MyWebView.AddEventListener("SendCall", SendCall);
            MyWebView.AddEventListener("SetRequestResult", SetRequestResult);
            MyWebView.AddEventListener("EndCall", EndCall);
            MyWebView.AddEventListener("SetCallResult", SetCallResult);
            MyWebView.AddEventListener("ToggleSpeakers", ToggleSpeakers);
            MyWebView.AddEventListener("ToggleMuteMic", ToggleMuteMic);
            MyWebView.AddEventListener("ToggleMuteSound", ToggleMuteSound);
            MyWebView.AddEventListener("ToggleCamera", ToggleCamera);
            MyWebView.AddEventListener("ToggleSwitchCamera", ToggleSwitchCamera);
            #endregion

            #region Mapping
            MyWebView.AddEventListener("GetMapPosition", GetMapPosition);
            #endregion

            #endregion
            string localHtmlUrl = Path.Combine(NSBundle.MainBundle.BundlePath, "Assets/index.html");
            MyWebView.LoadRequest(new NSUrlRequest(new NSUrl(localHtmlUrl, false)));
            MyWebView.ScalesPageToFit = true;
            MyWebView.ScrollView.Bounces = false;

            //View.AddSubview(WebView);

            MyWebView.InjectMtJavascript();

            Locator = CrossGeolocator.Current;
            Locator.AllowsBackgroundUpdates = true;
            Locator.DesiredAccuracy = 50;
            Locator.PositionChanged += Locator_PositionChanged;

            var settings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null);
            UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);


            CamCapture = new CameraCapture(MyCamView, View.Frame);
            CamCapture.OnFrameReady += OnVideoReadyToBeSent;
        }

        public void OnLog(object sender, EventArgs e)
        {
            Console.WriteLine(sender);
        }

        void Locator_PositionChanged(object sender, PositionEventArgs e)
        {
            if (!NetworkController.IsLoggedIn) return;
            if (_prevLat.Equals(e.Position.Latitude) || _prevLon.Equals(e.Position.Longitude)) return;
            _prevLat = e.Position.Latitude;
            _prevLon = e.Position.Longitude;
            Network.UpdateGeolocation(e.Position.Latitude, e.Position.Longitude);
        }

        #region Functions
        public void ShowWorkingLoader()
        {
            if (_workingLoaderDisplayed)
            {
                return;
            }
            _workingLoaderDisplayed = true;
            ExecuteJavascript("ShowWorkingLoader();");
        }

        public void HideWorkingLoader()
        {
            if (!_workingLoaderDisplayed)
            {
                return;
            }
            _workingLoaderDisplayed = false;
            ExecuteJavascript("HideWorkingLoader();");
        }

        public void ExecuteJavascript(string javascript)
        {
            InvokeOnMainThread(() =>
            {
                // manipulate UI controls
                MyWebView.EvaluateJavascript(javascript);
            });
        }

        public int GetAge(DateTime dateOfBirth)
        {
            DateTime today = DateTime.Today;
            int arg_42_0 = (today.Year * 100 + today.Month) * 100 + today.Day;
            int num = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;
            return (arg_42_0 - num) / 10000;
        }

        public void SaveLoginInfo()
        {
            var plist = NSUserDefaults.StandardUserDefaults;
            plist.SetString("Socialapp_Username", Username);
            plist.SetString("Socialapp_Password", Password);
            plist.Synchronize();

            AutoLogin = true;
        }

        public void LoadLoginInfo()
        {
            var plist = NSUserDefaults.StandardUserDefaults;
            Username = plist.StringForKey("Socialapp_Username");
            Password = plist.StringForKey("Socialapp_Password");

            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                AutoLogin = false;
            }
            else AutoLogin = true;
            ExecuteJavascript(string.Format("loginInfos('{0}', '{1}', {2});", Username, Password, AutoLogin ? 1 : 0));
        }

        public void SelectProfilePicture()
        {
            selectPictureFor = 0;
            ShowImagePicker();
        }

        void ShowImagePicker()
        {
            ImagePicker = new UIImagePickerController();
            InvokeOnMainThread(() =>
            {
                try
                {
                    // manipulate UI controls
                    ImagePicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                    ImagePicker.MediaTypes = UIImagePickerController.AvailableMediaTypes(UIImagePickerControllerSourceType.PhotoLibrary);
                    //ImagePicker.ModalPresentationStyle = UIModalPresentationStyle.Popover;

                    ImagePicker.FinishedPickingMedia += ImagePicker_FinishedPickingMedia;
                    ImagePicker.Canceled += ImagePicker_Canceled;
                    PresentViewController(ImagePicker, true, null);
                    //NavigationController.PresentModalViewController(ImagePicker, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            });
        }

        private void ImagePicker_Canceled(object sender, EventArgs e)
        {
            InvokeOnMainThread(() =>
            {
                // manipulate UI controls
                ImagePicker.DismissModalViewController(true);
            });
        }

        private void ImagePicker_FinishedPickingMedia(object sender, UIImagePickerMediaPickedEventArgs e)
        {
            InvokeOnMainThread(() =>
            {
                // manipulate UI controls
                bool isImage = false;
                switch (e.Info[UIImagePickerController.MediaType].ToString())
                {
                    case "public.image":
                    isImage = true;
                    break;

                    case "public.video":
                    isImage = false;
                    break;
                }
                NSUrl referenceURL = e.Info[new NSString("UIImagePickerControllerReferenceUrl")] as NSUrl;
                if (referenceURL != null)
                {
                }

                if (isImage)
                {
                    //For now lets only use it has profile picture
                    UIImage originalImage = e.Info[UIImagePickerController.OriginalImage] as UIImage;
                    if (originalImage != null)
                    {
                        var data = originalImage.Scale(new CoreGraphics.CGSize() { Height = 300, Width = 300 }).AsPNG();

                        var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                        string filename = Path.Combine(documentsDirectory, "socialapp_temp_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png"); // hardcoded filename, overwritten each time

                        data.Save(filename, false);

                        switch (selectPictureFor)
                        {
                            case 0://Avatar
                            ExecuteJavascript(string.Concat(new string[]
                            {
                                    "DisplayEditProfilePicture('",
                                    "file:///"+filename,
                                    "', '",
                                    filename,
                                    "');"
                            }));
                            break;
                            case 1://Post
                            ExecuteJavascript(string.Format("DisplaySelectPostPicture('{0}', '{1}', {2});", "file:///" + filename, filename, PostFromHome ? 1 : 0));
                            break;
                            case 2://Message
                            ExecuteJavascript(string.Format("DisplaySelectMessagePicture('{0}', '{1}');", "file:///" + filename, filename));
                            break;
                        }
                    }
                }
                else
                {
                    //TODO: used for posting videos, or sending videos via messages
                    NSUrl mediaUrl = e.Info[UIImagePickerController.MediaURL] as NSUrl;
                    if (mediaUrl != null)
                    {
                        //Do something with the video?!
                    }
                }
                ImagePicker.DismissModalViewController(true);
            });
        }

        public void ShowMessage(string conversationId)
        {
            ExecuteJavascript("ShowConversation('" + conversationId + "');");
            ExecuteJavascript("changePage('messages');");
        }

        public void ShowPage(string page)
        {
            ExecuteJavascript("changePage('" + page + "');");
        }

        static UIImage getBitmapFromURL(string uri)
        {
            try
            {
                using (var url = new NSUrl(uri))
                using (var data = NSData.FromUrl(url))
                    return UIImage.LoadFromData(data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            return null;
        }

        public void VideoEnabled()
        {
            var enabled = callClient.MyVideoEnabled || callClient.RemoteVideoEnabled;
            ExecuteJavascript($"VideoEnabled({(enabled ? "true" : "false")});");
        }
        //TODO USE THIS!?
        public void ShowNotification(string title, string text, string avatar, string id, string category)
        {
            InvokeOnMainThread(() =>
            {
				// create the notification
				var notification = new UILocalNotification
				{
					AlertTitle = title,
					AlertAction = title,
					AlertLaunchImage = avatar,
					AlertBody = text,
					ApplicationIconBadgeNumber = 1,
					SoundName = UILocalNotification.DefaultSoundName
                };
                NSDate.FromTimeIntervalSinceNow(1);
                // schedule it
                UIApplication.SharedApplication.ScheduleLocalNotification(notification);
            });
        }

        #endregion
        #region  Network Events

        #region Account Authentication

        public void OnGetVersionFromServer(object sender, EventArgs args)
        {
            if ((string)sender == NetworkController.Version)
            {
                ExecuteJavascript("isVersionValid(true);");
                return;
            }
            ExecuteJavascript("isVersionValid(false);");
        }

        public void OnGetLoginResult(object sender, EventArgs args)
        {
            int result = (int)sender;
            ExecuteJavascript(string.Format("loginResult({0});", result));
        }

        public void OnGetAccountInfos(object sender, EventArgs args)
        {
            var objs = (object[])sender;
            var accountId = (string)objs[0];
            var accountName = (string)objs[1];
            var accountAvatar = (string)objs[2];
            var accountGender = (int)objs[3];
            var accountMembership = (bool)objs[4];

            ExecuteJavascript(string.Format("OnGetAccountInfos('{0}', '{1}', '{2}', {3}, {4});", accountId, accountName, accountAvatar, accountGender, accountMembership ? "true" : "false"));
        }

        public void OnGetEditAccount(object sender, EventArgs args)
        {
            var objs = (object[])sender;
            var email = objs[0];
            var username = objs[1];
            var firstname = objs[2];
            var lastname = objs[3];
            var dob_d = objs[4];
            var dob_m = objs[5];
            var dob_y = objs[6];

            ExecuteJavascript(string.Format("OnGetEditAccount('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}');", email, username, firstname, lastname, dob_d, dob_m, dob_y));
            HideWorkingLoader();
        }

        public void OnGetEditAccountResult(object sender, EventArgs args)
        {
            var result = (int)sender;
            ExecuteJavascript(string.Format("OnGetEditAccountResult({0});", result));
            HideWorkingLoader();
        }

        public void OnGetRegisterResult(object sender, EventArgs args)
        {
            int result = (int)sender;
            ExecuteJavascript(string.Format("registerResult({0});", result));
        }

        public void OnGetRecoverResult(object sender, EventArgs args)
        {
            var result = (int)sender;
            ExecuteJavascript(string.Format("OnGetRecoverResult({0});", result));
            HideWorkingLoader();
        }

        #endregion Account Authentication

        #region Search

        public void OnGetSearchResults(object sender, EventArgs args)
        {
            Dictionary<List<AccountData>, int> results = (Dictionary<List<AccountData>, int>)sender;
            List<AccountData> key = results.First().Key;
            int value = results.First().Value;
            ExecuteJavascript("ClearSearchResults();");
            ExecuteJavascript("CreatePagination(" + value + ");");
            if (key.Count == 0)
            {
                ExecuteJavascript("ShowNoSearchResults(true);");
            }
            else
            {
                ExecuteJavascript("ShowNoSearchResults(false);");
                foreach (AccountData current in key)
                {
                    int age = GetAge(DateTime.Parse(string.Concat(new string[]
                    {
                        current.Dob_y,
                        "-",
                        current.Dob_m,
                        "-",
                        current.Dob_d
                    })));
                    ExecuteJavascript(string.Concat(new object[]
                    {
                        "AddProfileToSearchResults('",
                        current.Id,
                        "','",
                        current.Username,
                        "','",
                        current.Avatar,
                        "','",
                        age,
                        "', '",
                        current.Gender,
                        "');"
                    }));
                }
            }
            HideWorkingLoader();
        }

        #endregion Search

        #region Edit Profile

        public void OnProfileEdited(object sender, EventArgs args)
        {
            ExecuteJavascript("changePage('profile');");
        }

        public void OnGetUploadProfilePictureToken(object sender, EventArgs args)
        {
            string text = (string)sender;
            if (!string.IsNullOrEmpty(text))
            {
                try
                {
                    WebClient webClient = new WebClient { Credentials = CredentialCache.DefaultCredentials };
                    webClient.Headers.Add("Content-Type", "binary/octet-stream");
                    byte[] array = webClient.UploadFile(NetworkController.WebServerAddress + "uploadAvatar?token=" + text, "POST", PictureToUpload);
                    string @string = Encoding.UTF8.GetString(array, 0, array.Length);
                    webClient.Dispose();
                    if (@string.Contains("success"))
                    {
                        Network.UpdateProfilePicture();
                        ExecuteJavascript("changePage('profile');");
                        File.Delete(PictureToUpload);
                    }
                    else
                    {
                        ExecuteJavascript("changePage('profile');");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }

        public void OnGetEditProfileData(object sender, EventArgs args)
        {
            ProfileData profileData = (ProfileData)sender;
            ExecuteJavascript(string.Concat(new object[]
            {
                "OnGetEditProfileInfo('",
                profileData.Picture,
                "', '",
                profileData.City.Replace("\n", "<br>").Replace("'", "\\\'"),
                "', '",
                profileData.Job.Replace("\n", "<br>").Replace("'", "\\\'"),
                "', '",
                profileData.Activities.Replace("\n", "<br>").Replace("'", "\\\'"),
                "', '",
                profileData.Description.Replace("\n", "<br>").Replace("'", "\\\'"),
                "','",
                profileData.LookingFor,
                "', '",
                profileData.LookingForDescription.Replace("\n", "<br>").Replace("'", "\\\'"),
                "', ",
                profileData.Gender,
                ");"
            }));
            HideWorkingLoader();
        }

        #endregion Edit Profile

        #region View Profile

        public void OnGetProfileData(object sender, EventArgs args)
        {
            ProfileData profileData = (ProfileData)sender;
            var js = @"OnGetProfileData('" + profileData.Id + "', '" + profileData.Username + "', '" + profileData.Picture + "', " + (profileData.IsBlocked ? "true" : "false") + ", '" + profileData.PhotosCount + "', '" + profileData.City.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + profileData.Job.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + profileData.Activities.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + profileData.Description.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + profileData.LookingFor + "', '" + profileData.LookingForDescription.Replace("\n", "<br>").Replace("'", "\\\'") + "', " + profileData.Gender + ", " + profileData.Age + ", " + (profileData.Followed ? 1 : 0) + ");";
            ExecuteJavascript(js);
            HideWorkingLoader();
        }

        public void OnGetProfilePictures(object sender, EventArgs args)
        {
            var photos = (List<PostData>)sender;
            if (photos != null)
            {
                foreach (var photo in photos)
                {
                    if (!string.IsNullOrEmpty(photo.AttachmentId))
                    {
                        ExecuteJavascript(string.Format("OnGetProfilePhoto('{0}', '{1}', '{2}');", photo.Id, photo.AttachmentId, photo.Content));
                    }
                }
            }
            HideWorkingLoader();
        }

        #endregion View Profile

        public void OnReturnToHome(object sender, EventArgs args)
        {
            ExecuteJavascript("changePage('home');");
            HideWorkingLoader();
        }

        #region Messages

        public void OnGetConversations(object sender, EventArgs args)
        {
            List<ConversationData> conversations = (List<ConversationData>)sender;
            if (conversations == null)
            {
                //Do something ?
            }
            else
            {
                foreach (var conversation in conversations)
                {
                    //Append to view!
                    var otherId = "";
                    var otherAvatar = "";
                    var otherName = "";
                    var myAvatar = "";
                    var otherGender = 0;
                    var myGender = 0;

                    if (conversation.User1Id == NetworkController.AccountId)
                    {
                        otherId = conversation.User2Id;
                        otherAvatar = conversation.User2Avatar;
                        otherName = conversation.User2Name;
                        otherGender = conversation.User2Gender;
                        myAvatar = conversation.User1Avatar;
                        myGender = conversation.User1Gender;
                    }
                    else
                    {
                        otherId = conversation.User1Id;
                        otherAvatar = conversation.User1Avatar;
                        otherName = conversation.User1Name;
                        otherGender = conversation.User1Gender;
                        myAvatar = conversation.User2Avatar;
                        myGender = conversation.User2Gender;
                    }
                    ExecuteJavascript("OnGetConversation('" + conversation.Id + "', '" + otherId + "', '" + otherName + "', '" + otherAvatar + "', " + otherGender + ", '" + myAvatar + "', " + myGender + ", '" + (!string.IsNullOrEmpty(conversation.LastMessage) ? conversation.LastMessage.Replace("\n", "<br>").Replace("'", "\\\'") : "") + "', " + (conversation.LastMessageFromMe ? 1 : 0) + ", " + (conversation.LastMessageSeen ? 1 : 0) + ");");
                    Network.LoadMessages(conversation.Id, 1);
                }
            }
        }

        public void OnGetMessages(object sender, EventArgs args)
        {
            List<MessageData> messages = (List<MessageData>)sender;
            if (messages == null)
            {
                //Do something ?
            }
            else
            {
                foreach (var message in messages)
                {
                    //Append to view!
                    ExecuteJavascript("OnGetMessage('" + message.ConversationId + "', '" + message.Id + "', '" + message.FromId + "', '" + message.AttachmentId + "', '" + message.Message.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + message.When.ToString() + "', " + ((message.SeenAt == null || !message.SeenAt.HasValue || message.SeenAt.Value.Year == 1) ? 0 : 1) + ", '" + ((message.SeenAt != null && message.SeenAt.HasValue) ? message.SeenAt.Value.ToString() : "") + "');");
                }
            }
        }

        public void OnMessageReceived(object sender, EventArgs args)
        {
            var objs = (object[])sender;
            var message = (MessageData)objs[0];
            if (message == null) return;
            var conversation = (ConversationData)objs[1];
            //Append to view!
            string otherId;
            string otherAvatar;
            string otherName;
            string myAvatar;
            int otherGender;
            int myGender;

            if (conversation.User1Id == NetworkController.AccountId)
            {
                otherId = conversation.User2Id;
                otherAvatar = conversation.User2Avatar;
                otherName = conversation.User2Name;
                otherGender = conversation.User2Gender;
                myAvatar = conversation.User1Avatar;
                myGender = conversation.User1Gender;
            }
            else
            {
                otherId = conversation.User1Id;
                otherAvatar = conversation.User1Avatar;
                otherName = conversation.User1Name;
                otherGender = conversation.User1Gender;
                myAvatar = conversation.User2Avatar;
                myGender = conversation.User2Gender;
            }

            if (message.FromId != NetworkController.AccountId && (!HasWindowFocus || CurrentPage != "messages"))
            {
                ShowNotification(otherName, message.Message, otherAvatar, conversation.Id, "message");
            }
            ExecuteJavascript("OnGetConversation('" + conversation.Id + "', '" + otherId + "', '" + otherName + "', '" + otherAvatar + "', " + otherGender + ", '" + myAvatar + "', " + myGender + ", '" + (!string.IsNullOrEmpty(conversation.LastMessage) ? conversation.LastMessage.Replace("\n", "<br>").Replace("'", "\\\'") : "") + "', " + (conversation.LastMessageFromMe ? 1 : 0) + ", " + (conversation.LastMessageSeen ? 1 : 0) + ");");
            //Append message to view!
            ExecuteJavascript("OnGetMessage('" + message.ConversationId + "', '" + message.Id + "', '" + message.FromId + "', '" + message.AttachmentId + "', '" + message.Message.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + message.When.ToString() + "', " + ((message.SeenAt == null || !message.SeenAt.HasValue || message.SeenAt.Value.Year == 1) ? 0 : 1) + ", '" + ((message.SeenAt != null && message.SeenAt.HasValue) ? message.SeenAt.Value.ToString() : "") + "');");
        }

        public bool HasWindowFocus = true;//TODO...

        public void OnMessageSeen(object sender, EventArgs args)
        {
            var message = (MessageData)sender;
            if (message?.SeenAt != null)
            {
                //set message to view!
                ExecuteJavascript("OnGetMessageSeen('" + message.FromId + "', '" + message.SeenAt.Value.ToString() + "');");
            }
        }

        public void OnConversationStarted(object sender, EventArgs args)
        {
            var conversation = (ConversationData)sender;
            //Append to view!
            string otherId;
            string otherAvatar;
            string otherName;
            string myAvatar;
            int otherGender;
            int myGender;

            if (conversation.User1Id == NetworkController.AccountId)
            {
                otherId = conversation.User2Id;
                otherAvatar = conversation.User2Avatar;
                otherName = conversation.User2Name;
                otherGender = conversation.User2Gender;
                myAvatar = conversation.User1Avatar;
                myGender = conversation.User1Gender;
            }
            else
            {
                otherId = conversation.User1Id;
                otherAvatar = conversation.User1Avatar;
                otherName = conversation.User1Name;
                otherGender = conversation.User1Gender;
                myAvatar = conversation.User2Avatar;
                myGender = conversation.User2Gender;
            }
            ExecuteJavascript("OnGetConversation('" + conversation.Id + "', '" + otherId + "', '" + otherName + "', '" + otherAvatar + "', " + otherGender + ", '" + myAvatar + "', " + myGender + ", '" + (!string.IsNullOrEmpty(conversation.LastMessage) ? conversation.LastMessage : "") + "', " + (conversation.LastMessageFromMe ? 1 : 0) + ", " + (conversation.LastMessageSeen ? 1 : 0) + ");");
            ExecuteJavascript("ShowConversation('" + conversation.Id + "');");
            ExecuteJavascript("changePage('messages');");
            HideWorkingLoader();
        }

        #endregion Messages

        #region Follows

        public void OnGetProfileFollowers(object sender, EventArgs args)
        {
            var followers = (List<FollowData>)sender;
            ExecuteJavascript("OnGetFollowersCount(" + followers.Count + ");");
            foreach (var follower in followers)
            {
                //if (follower.UserId == NetworkController.AccountId) follower.UserId = "0";
                ExecuteJavascript("OnGetFollower('" + follower.Id + "', '" + follower.UserId + "', '" + follower.UserName + "', '" + follower.UserAvatar + "', " + follower.UserGender + ");");
            }
        }

        public void OnGetProfileFollowings(object sender, EventArgs args)
        {
            var followings = (List<FollowData>)sender;
            ExecuteJavascript("OnGetFollowingsCount(" + followings.Count + ");");
            foreach (var following in followings)
            {
                //if (following.UserId == NetworkController.AccountId) following.UserId = "0";
                ExecuteJavascript("OnGetFollowing('" + following.Id + "', '" + following.ProfileId + "', '" + following.ProfileName + "', '" + following.ProfileAvatar + "', " + following.ProfileGender + ");");
            }
        }

        #endregion Follows

        #region Notifications

        public void OnGetNotifications(object sender, EventArgs args)
        {
            var notifications = (List<NotificationData>)sender;
            if (notifications == null) return;
            foreach (var notification in notifications)
            {
                //Append it to the view!
                ExecuteJavascript("OnGetNotification('" + notification.Id + "', '" + notification.FromId + "', '" + notification.FromName + "', '" + notification.FromAvatar + "', " + notification.FromGender + ", " + ((int)notification.Type) + ",'" + notification.When.ToString() + "');");
            }
        }

        public void OnGetNotification(object sender, EventArgs args)
        {
            var notification = (NotificationData)sender;
            if (notification == null) return;
            //Trigger phone notification here!
            if (CurrentPage != "notifications" && notification.Type != NotificationTypes.View)
            {
                try
                {
                    var message = "";
                    switch (notification.Type)
                    {
                        case NotificationTypes.View:
                        message = "Viewed your profile.";
                        break;

                        case NotificationTypes.Poke:
                        message = "Poked you.";
                        break;

                        case NotificationTypes.Follow:
                        message = "Followed your profile.";
                        break;

                        case NotificationTypes.Unfollow:
                        message = "Unfollowed your profile.";
                        break;
                        case NotificationTypes.CallRequestAccepted:
                        ExecuteJavascript($"OnDeleteMyRequest('{notification.FromId}', 0);");
                        message = "Accepted your call request.";
                        break;
                        case NotificationTypes.CallRequestDeclined:
                        ExecuteJavascript($"OnDeleteMyRequest('{notification.FromId}', 0);");
                        message = "Declined your call request.";
                        break;
                        case NotificationTypes.CallMissed:
                        message = "You missed a call.";
                        break;
                        case NotificationTypes.CallEnded:
                        message = "Call terminated.";
                        break;
                        case NotificationTypes.MapRequestAccepted:
                        {
                            ExecuteJavascript($"OnDeleteMyRequest('{notification.FromId}', 1);");
                            message = "Accepted your position request.";
                        }
                        break;
                        case NotificationTypes.MapRequestDeclined:
                        {
                            ExecuteJavascript($"OnDeleteMyRequest('{notification.FromId}', 1);");
                            message = "Declined your position request.";
                        }
                        break;
                        case NotificationTypes.MsgRequestAccepted:
                        {
                            ExecuteJavascript($"OnDeleteMyRequest('{notification.FromId}', 2);");
                            message = "Accepted your message request.";
                        }
                        break;
                        case NotificationTypes.MsgRequestDeclined:
                        {
                            ExecuteJavascript($"OnDeleteMyRequest('{notification.FromId}', 2);");
                            message = "Declined your message request.";
                        }
                        break;
                    }
                    ShowNotification(notification.FromName, message, notification.FromAvatar, notification.Id, "notification");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
            //Append it to the view!
            ExecuteJavascript("OnGetNotification('" + notification.Id + "', '" + notification.FromId + "', '" + notification.FromName + "', '" + notification.FromAvatar + "', " + notification.FromGender + ", " + ((int)notification.Type) + ",'" + notification.When.ToString() + "');");
        }

        #endregion Notifications

        #region Posts

        public void OnGetPost(object sender, EventArgs args)
        {
            var post = (PostData)sender;
            if (post == null) return;

            //if (post.UserId == NetworkController.AccountId) post.UserId = "0";

            ExecuteJavascript("OnGetPost('" + post.Id + "','" + post.UserId + "','" + post.UserName + "','" + post.UserAvatar + "', " + post.Gender + ",'" + post.Content.Replace("\n", "<br>").Replace("'", "\\\'") + "','" + post.AttachmentId + "','" + post.DateAt.ToString() + "')");
        }

        public void OnGetPostDeleted(object sender, EventArgs args)
        {
            var postId = (string)sender;
            if (string.IsNullOrEmpty(postId)) return;

            ExecuteJavascript("OnGetPostDeleted('" + postId + "');");
        }

        public void OnGetPosts(object sender, EventArgs args)
        {
            var posts = (List<PostData>)sender;
            if (posts == null) return;

            foreach (var post in posts)
            {
                //if (post.UserId == NetworkController.AccountId) post.UserId = "0";

                ExecuteJavascript("OnGetPost('" + post.Id + "','" + post.UserId + "','" + post.UserName + "','" + post.UserAvatar + "', " + post.Gender + ",'" + post.Content.Replace("\n", "<br>").Replace("'", "\\\'") + "','" + post.AttachmentId + "','" + post.DateAt.ToString() + "')");
            }
        }

        public void OnGetProfilePosts(object sender, EventArgs args)
        {
            var posts = (List<PostData>)sender;
            if (posts == null) return;

            foreach (var post in posts)
            {
                //if (post.UserId == NetworkController.AccountId) post.UserId = "0";

                ExecuteJavascript("OnGetProfilePost('" + post.Id + "','" + post.UserId + "','" + post.UserName + "','" + post.UserAvatar + "', " + post.Gender + ",'" + post.Content.Replace("\n", "<br>").Replace("'", "\\\'") + "','" + post.AttachmentId + "','" + post.DateAt.ToString() + "')");
            }
        }

        public void OnGetComments(object sender, EventArgs args)
        {
            var comments = (List<CommentData>)sender;
            if (comments == null) return;

            foreach (var comment in comments)
            {
                ExecuteJavascript("OnGetComment('" + comment.Id + "','" + comment.PostId + "','" + comment.PostUserId + "','" + comment.UserId + "','" + comment.UserName + "','" + comment.UserAvatar + "', " + comment.Gender + ",'" + comment.Content.Replace("\n", "<br>").Replace("'", "\\\'") + "','" + comment.AttachmentId + "','" + comment.DateAt.ToString() + "');");
            }
        }

        public void OnGetComment(object sender, EventArgs args)
        {
            var comment = (CommentData)sender;
            if (comment == null) return;

            ExecuteJavascript("OnGetComment('" + comment.Id + "','" + comment.PostId + "','" + comment.PostUserId + "','" + comment.UserId + "','" + comment.UserName + "','" + comment.UserAvatar + "', " + comment.Gender + ",'" + comment.Content.Replace("\n", "<br>").Replace("'", "\\\'") + "','" + comment.AttachmentId + "','" + comment.DateAt.ToString() + "');");
        }

        public void OnGetCommentDeleted(object sender, EventArgs args)
        {
            var commentId = (string)sender;
            if (string.IsNullOrEmpty(commentId)) return;

            ExecuteJavascript("OnGetCommentDeleted('" + commentId + "');");
        }

        #endregion Posts

        #region Payments

        public void OnGetTransactions(object sender, EventArgs args)
        {
            var transactions = (List<TransactionData>)sender;
            if (transactions == null) return;
            foreach (var transaction in transactions)
            {
                ExecuteJavascript(
                    $"OnGetTransaction('{transaction.Id}', '{transaction.TokenId}', '{transaction.Amount}', '{transaction.Currency}', '{transaction.DateAt.ToString()}', '{transaction.ExpireAt.ToString()}')");
            }
            HideWorkingLoader();
        }

        public void OnGetPaymentResult(object sender, EventArgs args)
        {
            var result = (bool)sender;
            ExecuteJavascript($"OnGetPaymentResult({(result ? "true" : "false")})");
            HideWorkingLoader();
        }

        public void OnGetVerifyMembership(object sender, EventArgs args)
        {
            var result = (bool)sender;
            ExecuteJavascript($"OnGetVerifyMembership({(result ? "true" : "false")})");
            HideWorkingLoader();
        }

        public void OnPremiumRequired(object sender, EventArgs args)
        {
            ExecuteJavascript("OnPremiumRequired();");
            HideWorkingLoader();
        }

        #endregion Payments

        #region Blocks
        public void OnGetBlockedUsers(object sender, EventArgs args)
        {
            var blockedUsers = (List<BlockData>)sender;
            ExecuteJavascript(blockedUsers.Count == 0 ? "$('#no-blocked-users').show();" : "$('#no-blocked-users').hide();");
            ExecuteJavascript("$('.blocked-users').html('');");
            foreach (var blockedUser in blockedUsers)
            {
                ExecuteJavascript(
                    $"OnGetBlockedUser('{blockedUser.ProfileId}', '{blockedUser.ProfileUserName}', '{blockedUser.ProfileAvatar}', '{blockedUser.ProfileGender}', '{blockedUser.DateAt.ToString()}');");
            }
            HideWorkingLoader();
        }
        #endregion

        #region Calls
        public void OnCallVideoChanged(object sender, EventArgs args)
        {
            var enabled = (bool)sender;
            if (enabled)
            {
                callClient.RemoteVideoEnabled = true;
                VideoEnabled();
            }
            else
            {
                callClient.RemoteVideoEnabled = false;
                VideoEnabled();
                //imageView.SetImageDrawable(null);
                //imageView.RefreshDrawableState();
                //imageView.ClearFocus();
                //WebView.ClearFocus();
            }
        }
        public void OnGetRequests(object sender, EventArgs args)
        {
            var requests = (List<RequestData>)sender;
            if (requests == null) return;
            foreach (var request in requests)
            {
                OnGetRequest(request, new EventArgs());
            }
            if (requests.Count == 0)
            {
                ExecuteJavascript("$('.requests-feed').append('<center id=\"no-request\"><b>You have no requests.</b></center>');");
            }
        }
        public void OnGetRequest(object sender, EventArgs args)
        {
            var callRequest = (RequestData)sender;
            if (callRequest == null) return;

            if (callRequest.UserId == NetworkController.AccountId)
            {
                //I Sent the request
                ExecuteJavascript(
                    $"OnGetRequest('{callRequest.ProfileId}', '{callRequest.ProfileName}', '{callRequest.ProfileAvatar}', '{callRequest.ProfileGender}', '{callRequest.DateAt}', true, {callRequest.RequestType}, {(callRequest.Accepted ? "true" : "false")});");
                if (args == null) ShowPage("requests");
            }
            else
            {
                //I received the request
                if (!callRequest.Accepted)
                {
                    try
                    {
                        var message = "";
                        switch (callRequest.RequestType)
                        {
                            case 0://Call Request
                            {
                                message = "Sent you a call request.";
                            }
                            break;
                            case 1://Mapping Request
                            {
                                message = "Sent you a position request.";
                            }
                            break;
                            case 2://Message Request
                            {
                                message = "Sent you a private message request.";
                            }
                            break;
                        }
                        ShowNotification(callRequest.UserName, message, callRequest.UserAvatar, callRequest.Id, "request");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.StackTrace);
                    }
                }
                ExecuteJavascript(
                    $"OnGetRequest('{callRequest.UserId}', '{callRequest.UserName}', '{callRequest.UserAvatar}', '{callRequest.UserGender}', '{callRequest.DateAt}', false, {callRequest.RequestType}, {(callRequest.Accepted ? "true" : "false")});");
            }
            HideWorkingLoader();
        }
        public void OnStartCalling(object sender, EventArgs args)
        {
            var callData = (CallData)sender;
            if (callData == null) return;
            isCalling = true;
            ExecuteJavascript(
                $"OnStartCalling('{callData.Id}', '{callData.ProfileId}', '{callData.ProfileName}', '{callData.ProfileAvatar}', '{callData.ProfileGender}');");
        }
        public void OnReceiveCall(object sender, EventArgs args)
        {
            var callData = (CallData)sender;
            if (callData == null) return;
            isCalling = false;
            ExecuteJavascript(string.Format("OnReceiveCall('{0}', '{1}', '{2}', '{3}', '{4}');", callData.Id, callData.UserId, callData.UserName, callData.UserAvatar, callData.UserGender));
        }
        public void OnCallRingTone(object sender, EventArgs args)
        {
            AudioPlayer.PlayRingTone(!isCalling);
        }
        public void OnStartCall(object sender, EventArgs args)
        {
            var objs = (object[])sender;
            if (objs == null) return;

            var callData = (CallData)objs[0];
            var hostName = (string)objs[1];
            var port = (int)objs[2];

            if (callClient != null)
            {
                callClient.Destroy();
                callClient = null;
            }

            if (MicCapture != null)
            {
                MicCapture.Stop();
                MicCapture = null;
            }
            AudioPlayer.StopRingTone();
            ExecuteJavascript($"OnStartCall('{callData.Id}');");
            bool connected = false;
            try
            {
#if DEBUG
                //hostName = "192.168.1.106";
#endif
                callClient = new CallClient(hostName, port, callData.Id);
                callClient.OnGetAudio += OnGetSyncAudio;
                callClient.OnGetVideo += OnGetSyncVideo;
                callClient.OnDisconnected += OnGetCallDisconnected;
                connected = callClient.Connect();
                ExecuteJavascript("OnCallConnected('" + callData.Id + "');");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            if (connected)
            {
                MicCapture = new AudioCapture2();
                MicCapture.OnAudioReady += OnAudioReadyToBeSent;
                MicCapture.Start();
            }
            else
                Network.EndCall(callData.Id);
        }
        public void OnCallEnded(object sender, EventArgs args)
        {
            var callData = (CallData)sender;
            if (callData == null) return;
            AudioPlayer.StopRingTone();
            /*
            CamCapture?.Stop();*/

            if (callClient != null)
            {
                callClient.Destroy();
                callClient = null;
            }

            if (MicCapture != null)
            {
                MicCapture.Stop();
                MicCapture = null;
            }
            AudioPlayer.Stop();
            ExecuteJavascript(string.Format("OnCallEnded('{0}');", callData.Id));
        }

        public void OnAudioReadyToBeSent(byte[] bytes)
        {
            callClient?.SyncAudio(bytes);
        }

        public void OnVideoReadyToBeSent(byte[] bytes)
        {
            callClient?.SyncVideo(bytes);
        }

        public void OnGetSyncAudio(byte[] bytes)
        {
            AudioPlayer.PlayByteArrayAsync(bytes);
            //AudioPlayer.WriteBytes(bytes);
        }

        public void OnGetSyncVideo(byte[] bytes)
        {
            if (bytes == null) return;

            var data = NSData.FromArray(bytes);
            var uiimage = UIImage.LoadFromData(data);
            InvokeOnMainThread(() =>
            {
                // manipulate UI controls
                OtherVideoView.Image = uiimage;
            });

            //Bitmap bmp = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);

            //using (var h = new Handler(Looper.MainLooper))
            {
                //h.Post(() => { imageView.SetImageBitmap(bmp); });
            }

            //var encoded = Base64.EncodeToString(bytes, Base64Flags.Default);
            //var encoded = "";
            //ExecuteJavascript(string.Format("OnGetCallVideoFrame('{0}')", encoded));
        }

        public void OnGetCallDisconnected()
        {
            Network.EndCall(callClient.CallId);
        }
        #endregion

        #region Mapping
        public void OnGetPosition(object sender, EventArgs args)
        {
            var objs = (object[])sender;
            var latitude = (double)objs[0];
            var longitude = (double)objs[1];

            ExecuteJavascript(string.Format("OnGetPosition({0}, {1}, {2}, {3});", latitude.ToString().Replace(',', '.'), longitude.ToString().Replace(',', '.'), NetworkController.Latitude.ToString().Replace(',', '.'), NetworkController.Longitude.ToString().Replace(',', '.')));
        }
        #endregion
        #endregion
        #region  Client Events

        public void GetWebServerAddress(FireEventData e)
        {
            ExecuteJavascript("getWebServerAddress('" + NetworkController.WebServerAddress + "');");
        }

        public void Connect(FireEventData e)
        {
            var result = 0;
            if (NetworkController.IsConnected) result = 1;
            else result = Network.Connect() ? 1 : 0;

            ExecuteJavascript("connectionResult(" + result + ");");
        }

        #region Account Authentication
        public void LoadLoginSettings(FireEventData e)
        {
            LoadLoginInfo();
        }

        public void Login(FireEventData e)
        {
            var username = e.Data["username"].ToString();
            var password = e.Data["password"].ToString();
            var remember = (bool)e.Data["remember"];

            Console.WriteLine("Sending login request for user {0} password {1}", username, password);
            if (remember)
            {
                Username = username;
                Password = password;
                AutoLogin = true;
            }
            else
            {
                Username = "";
                Password = "";
            }
            SaveLoginInfo();
            password = Encryption.SHA1Of(password);

            Network.SendLogin(username, password);
        }

        public void Logout(FireEventData e)
        {
            Network.Logout();
            Password = "";
            AutoLogin = false;
            SaveLoginInfo();

            InvokeOnMainThread(() =>
            {
                // manipulate UI controls
                string localHtmlUrl = Path.Combine(NSBundle.MainBundle.BundlePath, "Assets/index.html");
                MyWebView.LoadRequest(new NSUrlRequest(new NSUrl(localHtmlUrl, false)));
                MyWebView.ScalesPageToFit = false;
            });
        }

        public void Register(FireEventData e)
        {
            var username = e.Data["username"].ToString();
            var password = e.Data["password"].ToString();
            var email = e.Data["email"].ToString();
            var fname = e.Data["fname"].ToString();
            var lname = e.Data["lname"].ToString();
            var dob_d = Convert.ToInt32(e.Data["dob_d"].ToString());
            var dob_m = Convert.ToInt32(e.Data["dob_m"].ToString());
            var dob_y = Convert.ToInt32(e.Data["dob_y"].ToString());
            var gender = Convert.ToInt32(e.Data["gender"].ToString());

            Network.Register(username, password, email, fname, lname, dob_d, dob_m, dob_y, gender);
        }

        public void Recover(FireEventData e)
        {
            ShowWorkingLoader();

            var recoverText = e.Data["username"].ToString();

            Network.Recover(recoverText);
        }
        #endregion Account Authentication

        #region Search
        public void SearchProfiles(FireEventData e)
        {
            ShowWorkingLoader();

            var username = e.Data["username"].ToString();
            var gender = e.Data["gender"].ToString();
            var minAge = e.Data["minAge"].ToString();
            var maxAge = e.Data["maxAge"].ToString();
            var distance = e.Data["distance"].ToString();
            var page = Convert.ToInt32(e.Data["page"].ToString());

            Network.SearchProfiles(username, gender, minAge, maxAge, distance, page);
        }
        #endregion

        #region Edit Profile
        public void LoadEditAccountInfo(FireEventData e)
        {
            ShowWorkingLoader();
            //TODO...
            //var profileId = e.Data["profileId"].ToString();
            //Network.LoadProfileData(profileId);
        }

        public void DoSaveAccount(FireEventData e)
        {
            ShowWorkingLoader();

            var email = e.Data["email"].ToString();
            var username = e.Data["username"].ToString();
            var password = e.Data["password"].ToString();
            var firstname = e.Data["firstname"].ToString();
            var lastname = e.Data["lastname"].ToString();
            var dob_d = e.Data["dob_d"].ToString();
            var dob_m = e.Data["dob_m"].ToString();
            var dob_y = e.Data["dob_m"].ToString();

            Network.DoSaveAccount(email, username, password, firstname, lastname, dob_d, dob_m, dob_y);
        }

        public void DoEditProfilePicture(FireEventData e)
        {
            SelectProfilePicture();
        }

        public void SaveEditProfile(FireEventData e)
        {
            ShowWorkingLoader();

            var picturePath = e.Data["picturePath"].ToString();
            var city = e.Data["city"].ToString();
            var job = e.Data["job"].ToString();
            var activities = e.Data["activities"].ToString();
            var description = e.Data["description"].ToString();
            var lookingFor = e.Data["lookingFor"].ToString();
            var lookingForDescription = e.Data["lookingForDescription"].ToString();

            if (!string.IsNullOrEmpty(picturePath) && File.Exists(picturePath))
            {
                PictureToUpload = picturePath;
                Network.EditProfilePicture();
            }
            Network.EditProfileInfo(city, job, activities, description, lookingFor, lookingForDescription);
        }


        public void LoadEditProfileInfo(FireEventData e)
        {
            ShowWorkingLoader();
            Network.LoadEditProfileInfo();
        }
        #endregion

        #region View Profile
        public void LoadProfileData(FireEventData e)
        {
            ShowWorkingLoader();
            var profileId = e.Data["profileId"].ToString();
            Network.LoadProfileData(profileId);
        }

        public void StartConversation(FireEventData e)
        {
            ShowWorkingLoader();
            var profileId = e.Data["userId"].ToString();
            Network.StartConversation(profileId);
        }

        public void GetProfilePhotos(FireEventData e)
        {
            ShowWorkingLoader();
            var profileId = e.Data["userId"].ToString();
            var page = Convert.ToInt32(e.Data["page"].ToString());
            Network.GetProfilePhotos(profileId, page);
        }

        #endregion

        #region Messages
        public void LoadConversations(FireEventData e)
        {
            var page = Convert.ToInt32(e.Data["page"].ToString());
            Network.LoadConversations(page);
        }
        public void LoadMessages(FireEventData e)
        {
            var conversationId = e.Data["conversationId"].ToString();
            var page = Convert.ToInt32(e.Data["page"].ToString());
            Network.LoadMessages(conversationId, page);
        }
        public void SendMessage(FireEventData e)
        {
            var userId = e.Data["userId"].ToString();
            var message = e.Data["message"].ToString();
            var attachmentPath = e.Data["attachmentPath"].ToString();

            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(message)) return;

            var attachmentId = "";
            ShowWorkingLoader();
            try
            {
                if (!string.IsNullOrEmpty(attachmentPath) && File.Exists(attachmentPath))
                {
                    WebClient webClient = new WebClient();
                    webClient.Credentials = CredentialCache.DefaultCredentials;
                    webClient.Headers.Add("Content-Type", "binary/octet-stream");
                    byte[] array = webClient.UploadFile(NetworkController.WebServerAddress + "uploadAttachment", "POST", attachmentPath);
                    string @string = Encoding.UTF8.GetString(array, 0, array.Length);
                    webClient.Dispose();
                    if (!string.IsNullOrEmpty(@string) && @string != "0")
                    {
                        attachmentId = @string;
                        File.Delete(attachmentPath);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }

            Network.SendMessage(userId, message, attachmentId);
            HideWorkingLoader();
        }
        public void DoSelectMessagePicture(FireEventData e)
        {
            selectPictureFor = 2;
            ShowImagePicker();
        }
        public void SetMessageHasSeen(FireEventData e)
        {
            var messageId = e.Data["messageId"].ToString();
            if (string.IsNullOrEmpty(messageId)) return;
            Network.SeenMessage(messageId);
        }
        #endregion

        #region Follows
        public void Follow(FireEventData e)
        {
            var userId = e.Data["userId"].ToString();
            if (string.IsNullOrEmpty(userId)) return;
            Network.Follow(userId);
        }
        public void Unfollow(FireEventData e)
        {
            var userId = e.Data["userId"].ToString();
            if (string.IsNullOrEmpty(userId)) return;
            Network.Unfollow(userId);
        }
        #endregion

        #region Notifications
        public void Poke(FireEventData e)
        {
            var userId = e.Data["userId"].ToString();
            if (string.IsNullOrEmpty(userId)) return;
            Network.Poke(userId);
        }
        public void DeleteNotification(FireEventData e)
        {
            var notificationId_ = e.Data["notificationId"].ToString();
            if (string.IsNullOrEmpty(notificationId_)) return;
            Network.DeleteNotification(notificationId_);
        }
        #endregion

        public void PageChanged(FireEventData e)
        {
            var page = e.Data["page"].ToString();
            if (string.IsNullOrEmpty(page)) return;
            CurrentPage = page;
        }

        #region Posts
        public void CreatePost(FireEventData e)
        {
            var message = e.Data["message"].ToString();
            var attachmentPath = e.Data["attachmentPath"].ToString();
            if (string.IsNullOrEmpty(message)) return;

            ShowWorkingLoader();
            var attachmentId = "";
            try
            {
                if (!string.IsNullOrEmpty(attachmentPath) && File.Exists(attachmentPath))
                {
                    WebClient webClient = new WebClient();
                    webClient.Credentials = CredentialCache.DefaultCredentials;
                    webClient.Headers.Add("Content-Type", "binary/octet-stream");
                    byte[] array = webClient.UploadFile(NetworkController.WebServerAddress + "uploadAttachment", "POST", attachmentPath);
                    string @string = Encoding.UTF8.GetString(array, 0, array.Length);
                    webClient.Dispose();
                    if (!string.IsNullOrEmpty(@string) && @string != "0")
                    {
                        attachmentId = @string;
                        File.Delete(attachmentPath);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            Network.CreatePost(message, attachmentId);
            HideWorkingLoader();
        }
        public void DoSelectPostPicture(FireEventData e)
        {
            var fromHome = Convert.ToBoolean(e.Data["fromHome"]);//TODO ... test his...
            PostFromHome = fromHome;
            selectPictureFor = 1;
            ShowImagePicker();
        }
        public void DeletePost(FireEventData e)
        {
            var postId = e.Data["postId"].ToString();
            if (string.IsNullOrEmpty(postId)) return;
            Network.DeletePost(postId);
        }
        public void CommentPost(FireEventData e)
        {
            var postId = e.Data["postId"].ToString();
            var message = e.Data["message"].ToString();
            var attachmentPath = e.Data["attachmentPath"].ToString();
            if (string.IsNullOrEmpty(postId) || string.IsNullOrEmpty(message)) return;
            Network.CommentPost(postId, message, attachmentPath);
        }
        public void DeleteComment(FireEventData e)
        {
            var commentId = e.Data["commentId"].ToString();
            if (string.IsNullOrEmpty(commentId)) return;
            Network.DeleteComment(commentId);
        }
        #endregion

        #region Payments
        public void GetTransactions(FireEventData e)
        {
            var page = Convert.ToInt32(e.Data["page"].ToString());
            Network.GetTransactions(page);
        }
        public void VerifyMembership(FireEventData e)
        {
            Network.VerifyMembership();
        }
        public void SendPayment(FireEventData e)
        {
            var tokenId = e.Data["tokenId"].ToString();
            var plan = Convert.ToInt32(e.Data["plan"].ToString());
            if (string.IsNullOrEmpty(tokenId)) return;
            Network.SendPayment(tokenId, plan);
        }
        #endregion

        #region Blocks
        public void GetBlockedUsers(FireEventData e)
        {
            ShowWorkingLoader();
            Network.GetBlockedUsers();
        }
        public void BlockUser(FireEventData e)
        {
            var userId = e.Data["userId"].ToString();
            if (string.IsNullOrEmpty(userId)) return;
            Network.BlockUser(userId);
        }
        public void UnBlockUser(FireEventData e)
        {
            var userId = e.Data["userId"].ToString();
            if (string.IsNullOrEmpty(userId)) return;
            Network.UnBlockUser(userId);
        }
        #endregion

        #region Calls
        public void SendCall(FireEventData e)
        {
            var userId = e.Data["userId"].ToString();
            if (string.IsNullOrEmpty(userId)) return;
            Network.SendCall(userId);
        }
        public void SetRequestResult(FireEventData e)
        {
            var userId = e.Data["userId"].ToString();
            var result = Convert.ToInt32(e.Data["result"].ToString());
            var type = Convert.ToInt32(e.Data["requestType"].ToString());
            if (string.IsNullOrEmpty(userId)) return;
            switch (type)
            {
                case 0:
                {
                    Network.SetCallRequestResult(userId, result == 1);
                }
                break;
                case 1:
                {
                    Network.SetMapRequestResult(userId, result == 1);
                }
                break;
                case 2:
                {
                    Network.SetMsgRequestResult(userId, result == 1);
                }
                break;
            }
        }
        public void SetCallResult(FireEventData e)
        {
            var callId = e.Data["callId"].ToString();
            var result = Convert.ToInt32(e.Data["result"].ToString());
            if (string.IsNullOrEmpty(callId)) return;
            Network.SetCallResult(callId, result == 1);
        }
        public void EndCall(FireEventData e)
        {
            var callId = e.Data["callId"].ToString();
            if (string.IsNullOrEmpty(callId)) return;
            Network.EndCall(callId);
        }
        public void SetCallVideoEnabled(FireEventData e)
        {
            var callId = e.Data["callId"].ToString();
            var enabled = Convert.ToInt32(e.Data["enabled"].ToString());
            if (string.IsNullOrEmpty(callId)) return;
            Network.SetCallVideoEnabled(callId, enabled == 1);
        }
        public void ToggleSpeakers(FireEventData e)
        {
            //AudioPlayer.ToggleSpeaker();//Todo... implement this...
        }
        public void ToggleMuteMic(FireEventData e)
        {
            //Todo... implement this...
        }
        public void ToggleMuteSound(FireEventData e)
        {
            AudioPlayer.ToggleMuteSound();
        }
        public void ToggleCamera(FireEventData e)
        {
            if (CamCapture == null || callClient == null) return;
            CamCapture.ToggleCamera();
            callClient.MyVideoEnabled = CamCapture.IsRecording;
            VideoEnabled();
            Network.SetCallVideoEnabled(callClient.CallId, CamCapture.IsRecording);
        }
        public void ToggleSwitchCamera(FireEventData e)
        {
            CamCapture?.ToggleSwitchCamera();
        }
        #endregion

        #region Mapping
        public void GetMapPosition(FireEventData e)
        {
            var userId = e.Data["userId"].ToString();
            if (string.IsNullOrEmpty(userId)) return;
            Network.GetMapPosition(userId);
        }
        #endregion

        #endregion//Client Triggers.
        //
        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}