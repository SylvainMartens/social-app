﻿namespace SocialAPP.Shared
{
    public enum NotificationTypes
    {
        View = 0,
        Poke = 1,
        Follow = 2,
        Unfollow = 3,
        CallRequest = 4,
        CallRequestAccepted = 5,
        CallRequestDeclined = 6,
        CallMissed = 7,
        CallEnded = 8,
        MapRequestAccepted = 9,
        MapRequestDeclined = 10,
        MsgRequestAccepted = 11,
        MsgRequestDeclined = 12,
        GroupInvite = 13,
        GroupInviteAccepted = 14,
        GroupInviteDeclined = 15,
        GroupRequestAccepted = 16,
        GroupRequestDeclined = 17
    }
}