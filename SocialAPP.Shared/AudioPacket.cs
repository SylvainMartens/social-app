﻿using ProtoBuf;
using System;

namespace SocialAPP.Shared
{
    [ProtoContract, Serializable]
    public class AudioPacket
    {
        [ProtoMember(1)]
        public byte[] Data { get; set; }
        [ProtoMember(2)]
        public int Bitrate { get; set; }
        [ProtoMember(3)]
        public int Encoding { get; set; }
        //[ProtoMember(4)]
        //public int Channel { get; set; }
    }
}