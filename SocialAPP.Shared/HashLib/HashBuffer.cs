using System;
using System.Diagnostics;

namespace HashLib
{
    internal class HashBuffer
    {
        private byte[] m_data;

        private int m_pos;

        public bool IsEmpty
        {
            get
            {
                return this.m_pos == 0;
            }
        }

        public int Pos
        {
            get
            {
                return this.m_pos;
            }
        }

        public int Length
        {
            get
            {
                return this.m_data.Length;
            }
        }

        public bool IsFull
        {
            get
            {
                return this.m_pos == this.m_data.Length;
            }
        }

        public HashBuffer(int a_length)
        {
            Debug.Assert(a_length > 0);
            this.m_data = new byte[a_length];
            this.Initialize();
        }

        public void Initialize()
        {
            this.m_pos = 0;
        }

        public byte[] GetBytes()
        {
            Debug.Assert(this.IsFull);
            this.m_pos = 0;
            return this.m_data;
        }

        public byte[] GetBytesZeroPadded()
        {
            Array.Clear(this.m_data, this.m_pos, this.m_data.Length - this.m_pos);
            this.m_pos = 0;
            return this.m_data;
        }

        public bool Feed(byte[] a_data, ref int a_start_index, ref int a_length, ref ulong a_processed_bytes)
        {
            Debug.Assert(a_start_index >= 0);
            Debug.Assert(a_length >= 0);
            Debug.Assert(a_start_index + a_length <= a_data.Length);
            Debug.Assert(!this.IsFull);
            bool flag = a_data.Length == 0;
            bool result;
            if (flag)
            {
                result = false;
            }
            else
            {
                bool flag2 = a_length == 0;
                if (flag2)
                {
                    result = false;
                }
                else
                {
                    int num = this.m_data.Length - this.m_pos;
                    bool flag3 = num > a_length;
                    if (flag3)
                    {
                        num = a_length;
                    }
                    Array.Copy(a_data, a_start_index, this.m_data, this.m_pos, num);
                    this.m_pos += num;
                    a_start_index += num;
                    a_length -= num;
                    a_processed_bytes += (ulong)((long)num);
                    result = this.IsFull;
                }
            }
            return result;
        }

        public bool Feed(byte[] a_data, int a_length)
        {
            Debug.Assert(a_length >= 0);
            Debug.Assert(a_length <= a_data.Length);
            Debug.Assert(!this.IsFull);
            bool flag = a_data.Length == 0;
            bool result;
            if (flag)
            {
                result = false;
            }
            else
            {
                bool flag2 = a_length == 0;
                if (flag2)
                {
                    result = false;
                }
                else
                {
                    int num = this.m_data.Length - this.m_pos;
                    bool flag3 = num > a_length;
                    if (flag3)
                    {
                        num = a_length;
                    }
                    Array.Copy(a_data, 0, this.m_data, this.m_pos, num);
                    this.m_pos += num;
                    result = this.IsFull;
                }
            }
            return result;
        }

        public override string ToString()
        {
            return string.Format("HashBuffer, Legth: {0}, Pos: {1}, IsEmpty: {2}", new object[]
            {
                this.Length,
                this.Pos,
                this.IsEmpty
            });
        }
    }
}