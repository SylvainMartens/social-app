using System;
using System.Diagnostics;
using System.Text;

namespace HashLib
{
    internal abstract class Hash : IHash
    {
        private readonly int m_block_size;

        private readonly int m_hash_size;

        public static int BUFFER_SIZE = 65536;

        public virtual string Name
        {
            get
            {
                return base.GetType().Name;
            }
        }

        public virtual int BlockSize
        {
            get
            {
                return this.m_block_size;
            }
        }

        public virtual int HashSize
        {
            get
            {
                return this.m_hash_size;
            }
        }

        public Hash(int a_hash_size, int a_block_size)
        {
            Debug.Assert(a_block_size > 0 || a_block_size == -1);
            Debug.Assert(a_hash_size > 0);
            this.m_block_size = a_block_size;
            this.m_hash_size = a_hash_size;
        }

        public virtual HashResult ComputeObject(object a_data)
        {
            bool flag = a_data is byte;
            HashResult result;
            if (flag)
            {
                result = this.ComputeByte((byte)a_data);
            }
            else
            {
                bool flag2 = a_data is short;
                if (flag2)
                {
                    result = this.ComputeShort((short)a_data);
                }
                else
                {
                    bool flag3 = a_data is ushort;
                    if (flag3)
                    {
                        result = this.ComputeUShort((ushort)a_data);
                    }
                    else
                    {
                        bool flag4 = a_data is char;
                        if (flag4)
                        {
                            result = this.ComputeChar((char)a_data);
                        }
                        else
                        {
                            bool flag5 = a_data is int;
                            if (flag5)
                            {
                                result = this.ComputeInt((int)a_data);
                            }
                            else
                            {
                                bool flag6 = a_data is uint;
                                if (flag6)
                                {
                                    result = this.ComputeUInt((uint)a_data);
                                }
                                else
                                {
                                    bool flag7 = a_data is long;
                                    if (flag7)
                                    {
                                        result = this.ComputeLong((long)a_data);
                                    }
                                    else
                                    {
                                        bool flag8 = a_data is ulong;
                                        if (flag8)
                                        {
                                            result = this.ComputeULong((ulong)a_data);
                                        }
                                        else
                                        {
                                            bool flag9 = a_data is float;
                                            if (flag9)
                                            {
                                                result = this.ComputeFloat((float)a_data);
                                            }
                                            else
                                            {
                                                bool flag10 = a_data is double;
                                                if (flag10)
                                                {
                                                    result = this.ComputeDouble((double)a_data);
                                                }
                                                else
                                                {
                                                    bool flag11 = a_data is string;
                                                    if (flag11)
                                                    {
                                                        result = this.ComputeString((string)a_data);
                                                    }
                                                    else
                                                    {
                                                        bool flag12 = a_data is byte[];
                                                        if (flag12)
                                                        {
                                                            result = this.ComputeBytes((byte[])a_data);
                                                        }
                                                        else
                                                        {
                                                            bool flag13 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(short);
                                                            if (flag13)
                                                            {
                                                                result = this.ComputeShorts((short[])a_data);
                                                            }
                                                            else
                                                            {
                                                                bool flag14 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(ushort);
                                                                if (flag14)
                                                                {
                                                                    result = this.ComputeUShorts((ushort[])a_data);
                                                                }
                                                                else
                                                                {
                                                                    bool flag15 = a_data is char[];
                                                                    if (flag15)
                                                                    {
                                                                        result = this.ComputeChars((char[])a_data);
                                                                    }
                                                                    else
                                                                    {
                                                                        bool flag16 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(int);
                                                                        if (flag16)
                                                                        {
                                                                            result = this.ComputeInts((int[])a_data);
                                                                        }
                                                                        else
                                                                        {
                                                                            bool flag17 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(uint);
                                                                            if (flag17)
                                                                            {
                                                                                result = this.ComputeUInts((uint[])a_data);
                                                                            }
                                                                            else
                                                                            {
                                                                                bool flag18 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(long);
                                                                                if (flag18)
                                                                                {
                                                                                    result = this.ComputeLongs((long[])a_data);
                                                                                }
                                                                                else
                                                                                {
                                                                                    bool flag19 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(ulong);
                                                                                    if (flag19)
                                                                                    {
                                                                                        result = this.ComputeULongs((ulong[])a_data);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        bool flag20 = a_data is float[];
                                                                                        if (flag20)
                                                                                        {
                                                                                            result = this.ComputeFloats((float[])a_data);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            bool flag21 = a_data is double[];
                                                                                            if (!flag21)
                                                                                            {
                                                                                                throw new ArgumentException();
                                                                                            }
                                                                                            result = this.ComputeDoubles((double[])a_data);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public virtual HashResult ComputeByte(byte a_data)
        {
            return this.ComputeBytes(new byte[]
            {
                a_data
            });
        }

        public virtual HashResult ComputeChar(char a_data)
        {
            return this.ComputeBytes(BitConverter.GetBytes(a_data));
        }

        public virtual HashResult ComputeShort(short a_data)
        {
            return this.ComputeBytes(BitConverter.GetBytes(a_data));
        }

        public virtual HashResult ComputeUShort(ushort a_data)
        {
            return this.ComputeBytes(BitConverter.GetBytes(a_data));
        }

        public virtual HashResult ComputeInt(int a_data)
        {
            return this.ComputeBytes(BitConverter.GetBytes(a_data));
        }

        public virtual HashResult ComputeUInt(uint a_data)
        {
            return this.ComputeBytes(BitConverter.GetBytes(a_data));
        }

        public virtual HashResult ComputeLong(long a_data)
        {
            return this.ComputeBytes(BitConverter.GetBytes(a_data));
        }

        public virtual HashResult ComputeULong(ulong a_data)
        {
            return this.ComputeBytes(BitConverter.GetBytes(a_data));
        }

        public virtual HashResult ComputeFloat(float a_data)
        {
            return this.ComputeBytes(BitConverter.GetBytes(a_data));
        }

        public virtual HashResult ComputeDouble(double a_data)
        {
            return this.ComputeBytes(BitConverter.GetBytes(a_data));
        }

        public virtual HashResult ComputeString(string a_data)
        {
            return this.ComputeBytes(Converters.ConvertStringToBytes(a_data));
        }

        public virtual HashResult ComputeString(string a_data, Encoding a_encoding)
        {
            return this.ComputeBytes(Converters.ConvertStringToBytes(a_data, a_encoding));
        }

        public virtual HashResult ComputeChars(char[] a_data)
        {
            return this.ComputeBytes(Converters.ConvertCharsToBytes(a_data));
        }

        public virtual HashResult ComputeShorts(short[] a_data)
        {
            return this.ComputeBytes(Converters.ConvertShortsToBytes(a_data));
        }

        public virtual HashResult ComputeUShorts(ushort[] a_data)
        {
            return this.ComputeBytes(Converters.ConvertUShortsToBytes(a_data));
        }

        public virtual HashResult ComputeInts(int[] a_data)
        {
            return this.ComputeBytes(Converters.ConvertIntsToBytes(a_data));
        }

        public virtual HashResult ComputeUInts(uint[] a_data)
        {
            return this.ComputeBytes(Converters.ConvertUIntsToBytes(a_data, 0, -1));
        }

        public virtual HashResult ComputeLongs(long[] a_data)
        {
            return this.ComputeBytes(Converters.ConvertLongsToBytes(a_data, 0, -1));
        }

        public virtual HashResult ComputeULongs(ulong[] a_data)
        {
            return this.ComputeBytes(Converters.ConvertULongsToBytes(a_data, 0, -1));
        }

        public virtual HashResult ComputeDoubles(double[] a_data)
        {
            return this.ComputeBytes(Converters.ConvertDoublesToBytes(a_data, 0, -1));
        }

        public virtual HashResult ComputeFloats(float[] a_data)
        {
            return this.ComputeBytes(Converters.ConvertFloatsToBytes(a_data, 0, -1));
        }

        public virtual HashResult ComputeBytes(byte[] a_data)
        {
            this.Initialize();
            this.TransformBytes(a_data);
            HashResult result = this.TransformFinal();
            this.Initialize();
            return result;
        }

        public void TransformObject(object a_data)
        {
            bool flag = a_data is byte;
            if (flag)
            {
                this.TransformByte((byte)a_data);
            }
            else
            {
                bool flag2 = a_data is short;
                if (flag2)
                {
                    this.TransformShort((short)a_data);
                }
                else
                {
                    bool flag3 = a_data is ushort;
                    if (flag3)
                    {
                        this.TransformUShort((ushort)a_data);
                    }
                    else
                    {
                        bool flag4 = a_data is char;
                        if (flag4)
                        {
                            this.TransformChar((char)a_data);
                        }
                        else
                        {
                            bool flag5 = a_data is int;
                            if (flag5)
                            {
                                this.TransformInt((int)a_data);
                            }
                            else
                            {
                                bool flag6 = a_data is uint;
                                if (flag6)
                                {
                                    this.TransformUInt((uint)a_data);
                                }
                                else
                                {
                                    bool flag7 = a_data is long;
                                    if (flag7)
                                    {
                                        this.TransformLong((long)a_data);
                                    }
                                    else
                                    {
                                        bool flag8 = a_data is ulong;
                                        if (flag8)
                                        {
                                            this.TransformULong((ulong)a_data);
                                        }
                                        else
                                        {
                                            bool flag9 = a_data is float;
                                            if (flag9)
                                            {
                                                this.TransformFloat((float)a_data);
                                            }
                                            else
                                            {
                                                bool flag10 = a_data is double;
                                                if (flag10)
                                                {
                                                    this.TransformDouble((double)a_data);
                                                }
                                                else
                                                {
                                                    bool flag11 = a_data is string;
                                                    if (flag11)
                                                    {
                                                        this.TransformString((string)a_data);
                                                    }
                                                    else
                                                    {
                                                        bool flag12 = a_data is byte[];
                                                        if (flag12)
                                                        {
                                                            this.TransformBytes((byte[])a_data);
                                                        }
                                                        else
                                                        {
                                                            bool flag13 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(short);
                                                            if (flag13)
                                                            {
                                                                this.TransformShorts((short[])a_data);
                                                            }
                                                            else
                                                            {
                                                                bool flag14 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(ushort);
                                                                if (flag14)
                                                                {
                                                                    this.TransformUShorts((ushort[])a_data);
                                                                }
                                                                else
                                                                {
                                                                    bool flag15 = a_data is char[];
                                                                    if (flag15)
                                                                    {
                                                                        this.TransformChars((char[])a_data);
                                                                    }
                                                                    else
                                                                    {
                                                                        bool flag16 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(int);
                                                                        if (flag16)
                                                                        {
                                                                            this.TransformInts((int[])a_data);
                                                                        }
                                                                        else
                                                                        {
                                                                            bool flag17 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(uint);
                                                                            if (flag17)
                                                                            {
                                                                                this.TransformUInts((uint[])a_data);
                                                                            }
                                                                            else
                                                                            {
                                                                                bool flag18 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(long);
                                                                                if (flag18)
                                                                                {
                                                                                    this.TransformLongs((long[])a_data);
                                                                                }
                                                                                else
                                                                                {
                                                                                    bool flag19 = a_data.GetType().IsArray && a_data.GetType().GetElementType() == typeof(ulong);
                                                                                    if (flag19)
                                                                                    {
                                                                                        this.TransformULongs((ulong[])a_data);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        bool flag20 = a_data is float[];
                                                                                        if (flag20)
                                                                                        {
                                                                                            this.TransformFloats((float[])a_data);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            bool flag21 = a_data is double[];
                                                                                            if (!flag21)
                                                                                            {
                                                                                                throw new ArgumentException();
                                                                                            }
                                                                                            this.TransformDoubles((double[])a_data);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void TransformByte(byte a_data)
        {
            this.TransformBytes(new byte[]
            {
                a_data
            });
        }

        public void TransformChar(char a_data)
        {
            this.TransformBytes(BitConverter.GetBytes(a_data));
        }

        public void TransformShort(short a_data)
        {
            this.TransformBytes(BitConverter.GetBytes(a_data));
        }

        public void TransformUShort(ushort a_data)
        {
            this.TransformBytes(BitConverter.GetBytes(a_data));
        }

        public void TransformInt(int a_data)
        {
            this.TransformBytes(BitConverter.GetBytes(a_data));
        }

        public void TransformUInt(uint a_data)
        {
            this.TransformBytes(BitConverter.GetBytes(a_data));
        }

        public void TransformLong(long a_data)
        {
            this.TransformBytes(BitConverter.GetBytes(a_data));
        }

        public void TransformULong(ulong a_data)
        {
            this.TransformBytes(BitConverter.GetBytes(a_data));
        }

        public void TransformFloat(float a_data)
        {
            this.TransformBytes(BitConverter.GetBytes(a_data));
        }

        public void TransformDouble(double a_data)
        {
            this.TransformBytes(BitConverter.GetBytes(a_data));
        }

        public void TransformChars(char[] a_data)
        {
            this.TransformBytes(Converters.ConvertCharsToBytes(a_data));
        }

        public void TransformString(string a_data)
        {
            this.TransformBytes(Converters.ConvertStringToBytes(a_data));
        }

        public void TransformString(string a_data, Encoding a_encoding)
        {
            this.TransformBytes(Converters.ConvertStringToBytes(a_data, a_encoding));
        }

        public void TransformShorts(short[] a_data)
        {
            this.TransformBytes(Converters.ConvertShortsToBytes(a_data));
        }

        public void TransformUShorts(ushort[] a_data)
        {
            this.TransformBytes(Converters.ConvertUShortsToBytes(a_data));
        }

        public void TransformInts(int[] a_data)
        {
            this.TransformBytes(Converters.ConvertIntsToBytes(a_data));
        }

        public void TransformUInts(uint[] a_data)
        {
            this.TransformBytes(Converters.ConvertUIntsToBytes(a_data, 0, -1));
        }

        public void TransformLongs(long[] a_data)
        {
            this.TransformBytes(Converters.ConvertLongsToBytes(a_data, 0, -1));
        }

        public void TransformULongs(ulong[] a_data)
        {
            this.TransformBytes(Converters.ConvertULongsToBytes(a_data, 0, -1));
        }

        public void TransformDoubles(double[] a_data)
        {
            this.TransformBytes(Converters.ConvertDoublesToBytes(a_data, 0, -1));
        }

        public void TransformFloats(float[] a_data)
        {
            this.TransformBytes(Converters.ConvertFloatsToBytes(a_data, 0, -1));
        }

        public void TransformBytes(byte[] a_data)
        {
            this.TransformBytes(a_data, 0, a_data.Length);
        }

        public void TransformBytes(byte[] a_data, int a_index)
        {
            Debug.Assert(a_index >= 0);
            int num = a_data.Length - a_index;
            Debug.Assert(num >= 0);
            this.TransformBytes(a_data, a_index, num);
        }

        public abstract void Initialize();

        public abstract void TransformBytes(byte[] a_data, int a_index, int a_length);

        public abstract HashResult TransformFinal();
    }
}