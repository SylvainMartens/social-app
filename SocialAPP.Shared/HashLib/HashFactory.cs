using HashLib.Crypto;

namespace HashLib
{
    public static class HashFactory
    {
        public static class Crypto
        {
            public static IHash CreateSHA0()
            {
                return new SHA0();
            }

            public static IHash CreateSHA1()
            {
                return new SHA1();
            }
        }
    }
}