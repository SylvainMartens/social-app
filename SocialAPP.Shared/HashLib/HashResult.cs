using System;
using System.Diagnostics;
using System.Linq;

namespace HashLib
{
    [DebuggerDisplay("HashResult, Size: {m_hash.Length}, Hash: {ToString()}")]
    public class HashResult
    {
        private byte[] m_hash;

        public HashResult(uint a_hash)
        {
            this.m_hash = BitConverter.GetBytes(a_hash);
        }

        internal HashResult(int a_hash)
        {
            this.m_hash = BitConverter.GetBytes(a_hash);
        }

        public HashResult(ulong a_hash)
        {
            this.m_hash = BitConverter.GetBytes(a_hash);
        }

        public HashResult(byte[] a_hash)
        {
            this.m_hash = a_hash;
        }

        public byte[] GetBytes()
        {
            return this.m_hash.ToArray<byte>();
        }

        public uint GetUInt()
        {
            bool flag = this.m_hash.Length != 4;
            if (flag)
            {
                throw new InvalidOperationException();
            }
            return BitConverter.ToUInt32(this.m_hash, 0);
        }

        public int GetInt()
        {
            bool flag = this.m_hash.Length != 4;
            if (flag)
            {
                throw new InvalidOperationException();
            }
            return BitConverter.ToInt32(this.m_hash, 0);
        }

        public ulong GetULong()
        {
            bool flag = this.m_hash.Length != 8;
            if (flag)
            {
                throw new InvalidOperationException();
            }
            return BitConverter.ToUInt64(this.m_hash, 0);
        }

        public override string ToString()
        {
            return Converters.ConvertBytesToHexString(this.m_hash, true);
        }

        public override bool Equals(object a_obj)
        {
            HashResult hashResult = a_obj as HashResult;
            bool flag = hashResult == null;
            return !flag && this.Equals(hashResult);
        }

        public bool Equals(HashResult a_hashResult)
        {
            return HashResult.SameArrays(a_hashResult.GetBytes(), this.m_hash);
        }

        public override int GetHashCode()
        {
            return Convert.ToBase64String(this.m_hash).GetHashCode();
        }

        private static bool SameArrays(byte[] a_ar1, byte[] a_ar2)
        {
            bool flag = a_ar1 == a_ar2;
            bool result;
            if (flag)
            {
                result = true;
            }
            else
            {
                bool flag2 = a_ar1.Length != a_ar2.Length;
                if (flag2)
                {
                    result = false;
                }
                else
                {
                    for (int i = 0; i < a_ar1.Length; i++)
                    {
                        bool flag3 = a_ar1[i] != a_ar2[i];
                        if (flag3)
                        {
                            result = false;
                            return result;
                        }
                    }
                    result = true;
                }
            }
            return result;
        }
    }
}