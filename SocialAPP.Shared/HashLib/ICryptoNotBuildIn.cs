namespace HashLib
{
    public interface ICryptoNotBuildIn : ICrypto, IHash, IBlockHash
    {
    }
}