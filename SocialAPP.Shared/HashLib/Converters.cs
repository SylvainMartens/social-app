using System;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace HashLib
{
    public static class Converters
    {
        public static byte[] ConvertToBytes(object a_in)
        {
            bool flag = a_in is byte;
            byte[] result;
            if (flag)
            {
                result = new byte[]
                {
                    (byte)a_in
                };
            }
            else
            {
                bool flag2 = a_in is short;
                if (flag2)
                {
                    result = BitConverter.GetBytes((short)a_in);
                }
                else
                {
                    bool flag3 = a_in is ushort;
                    if (flag3)
                    {
                        result = BitConverter.GetBytes((ushort)a_in);
                    }
                    else
                    {
                        bool flag4 = a_in is char;
                        if (flag4)
                        {
                            result = BitConverter.GetBytes((char)a_in);
                        }
                        else
                        {
                            bool flag5 = a_in is int;
                            if (flag5)
                            {
                                result = BitConverter.GetBytes((int)a_in);
                            }
                            else
                            {
                                bool flag6 = a_in is uint;
                                if (flag6)
                                {
                                    result = BitConverter.GetBytes((uint)a_in);
                                }
                                else
                                {
                                    bool flag7 = a_in is long;
                                    if (flag7)
                                    {
                                        result = BitConverter.GetBytes((long)a_in);
                                    }
                                    else
                                    {
                                        bool flag8 = a_in is ulong;
                                        if (flag8)
                                        {
                                            result = BitConverter.GetBytes((ulong)a_in);
                                        }
                                        else
                                        {
                                            bool flag9 = a_in is float;
                                            if (flag9)
                                            {
                                                result = BitConverter.GetBytes((float)a_in);
                                            }
                                            else
                                            {
                                                bool flag10 = a_in is double;
                                                if (flag10)
                                                {
                                                    result = BitConverter.GetBytes((double)a_in);
                                                }
                                                else
                                                {
                                                    bool flag11 = a_in is string;
                                                    if (flag11)
                                                    {
                                                        result = Converters.ConvertStringToBytes((string)a_in);
                                                    }
                                                    else
                                                    {
                                                        bool flag12 = a_in is byte[];
                                                        if (flag12)
                                                        {
                                                            result = (byte[])((byte[])a_in).Clone();
                                                        }
                                                        else
                                                        {
                                                            bool flag13 = a_in.GetType().IsArray && a_in.GetType().GetElementType() == typeof(short);
                                                            if (flag13)
                                                            {
                                                                result = Converters.ConvertShortsToBytes((short[])a_in);
                                                            }
                                                            else
                                                            {
                                                                bool flag14 = a_in.GetType().IsArray && a_in.GetType().GetElementType() == typeof(ushort);
                                                                if (flag14)
                                                                {
                                                                    result = Converters.ConvertUShortsToBytes((ushort[])a_in);
                                                                }
                                                                else
                                                                {
                                                                    bool flag15 = a_in is char[];
                                                                    if (flag15)
                                                                    {
                                                                        result = Converters.ConvertCharsToBytes((char[])a_in);
                                                                    }
                                                                    else
                                                                    {
                                                                        bool flag16 = a_in.GetType().IsArray && a_in.GetType().GetElementType() == typeof(int);
                                                                        if (flag16)
                                                                        {
                                                                            result = Converters.ConvertIntsToBytes((int[])a_in);
                                                                        }
                                                                        else
                                                                        {
                                                                            bool flag17 = a_in.GetType().IsArray && a_in.GetType().GetElementType() == typeof(uint);
                                                                            if (flag17)
                                                                            {
                                                                                result = Converters.ConvertUIntsToBytes((uint[])a_in, 0, -1);
                                                                            }
                                                                            else
                                                                            {
                                                                                bool flag18 = a_in.GetType().IsArray && a_in.GetType().GetElementType() == typeof(long);
                                                                                if (flag18)
                                                                                {
                                                                                    result = Converters.ConvertLongsToBytes((long[])a_in, 0, -1);
                                                                                }
                                                                                else
                                                                                {
                                                                                    bool flag19 = a_in.GetType().IsArray && a_in.GetType().GetElementType() == typeof(ulong);
                                                                                    if (flag19)
                                                                                    {
                                                                                        result = Converters.ConvertULongsToBytes((ulong[])a_in, 0, -1);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        bool flag20 = a_in is float[];
                                                                                        if (flag20)
                                                                                        {
                                                                                            result = Converters.ConvertFloatsToBytes((float[])a_in, 0, -1);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            bool flag21 = a_in is double[];
                                                                                            if (!flag21)
                                                                                            {
                                                                                                throw new ArgumentException();
                                                                                            }
                                                                                            result = Converters.ConvertDoublesToBytes((double[])a_in, 0, -1);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static uint[] ConvertBytesToUInts(byte[] a_in, int a_index = 0, int a_length = -1)
        {
            bool flag = a_length == -1;
            if (flag)
            {
                a_length = a_in.Length;
            }
            Converters.Check<byte>(a_in, 1, 4, a_index, a_length);
            uint[] array = new uint[a_length / 4];
            Converters.ConvertBytesToUInts(a_in, a_index, a_length, array);
            return array;
        }

        public static void ConvertBytesToUInts(byte[] a_in, int a_index, int a_length, uint[] a_out)
        {
            Converters.Check<byte>(a_in, 1, 4, a_index, a_length);
            Buffer.BlockCopy(a_in, a_index, a_out, 0, a_length);
        }

        public static ulong[] ConvertBytesToULongs(byte[] a_in, int a_index = 0, int a_length = -1)
        {
            bool flag = a_length == -1;
            if (flag)
            {
                a_length = a_in.Length;
            }
            Converters.Check<byte>(a_in, 1, 8, a_index, a_length);
            ulong[] array = new ulong[a_length / 8];
            Converters.ConvertBytesToULongs(a_in, a_index, a_length, array, 0);
            return array;
        }

        public static void ConvertBytesToULongs(byte[] a_in, int a_index_in, int a_length, ulong[] a_out, int a_index_out)
        {
            Converters.Check<byte, ulong>(a_in, 1, a_out, 8, a_index_in, a_length, a_index_out);
            Buffer.BlockCopy(a_in, a_index_in, a_out, a_index_out * 8, a_length);
        }

        public static uint[] ConvertBytesToUIntsSwapOrder(byte[] a_in, int a_index, int a_length)
        {
            Converters.Check<byte>(a_in, 1, 4, a_index, a_length);
            uint[] array = new uint[a_length / 4];
            Converters.ConvertBytesToUIntsSwapOrder(a_in, a_index, a_length, array, 0);
            return array;
        }

        public static void ConvertBytesToUIntsSwapOrder(byte[] a_in, int a_index, int a_length, uint[] a_result, int a_index_out)
        {
            Converters.Check<byte, uint>(a_in, 1, a_result, 4, a_index, a_length, a_index_out);
            int num = a_index_out;
            while (a_length > 0)
            {
                a_result[num++] = (uint)((int)a_in[a_index++] << 24 | (int)a_in[a_index++] << 16 | (int)a_in[a_index++] << 8 | (int)a_in[a_index++]);
                a_length -= 4;
            }
        }

        public static ulong ConvertBytesToULongSwapOrder(byte[] a_in, int a_index)
        {
            Debug.Assert(a_index >= 0);
            Debug.Assert(a_index + 8 <= a_in.Length);
            return (ulong)a_in[a_index++] << 56 | (ulong)a_in[a_index++] << 48 | (ulong)a_in[a_index++] << 40 | (ulong)a_in[a_index++] << 32 | (ulong)a_in[a_index++] << 24 | (ulong)a_in[a_index++] << 16 | (ulong)a_in[a_index++] << 8 | (ulong)a_in[a_index];
        }

        public static ulong ConvertBytesToULong(byte[] a_in, int a_index)
        {
            Debug.Assert(a_index >= 0);
            Debug.Assert(a_index + 8 <= a_in.Length);
            return BitConverter.ToUInt64(a_in, a_index);
        }

        public static uint ConvertBytesToUIntSwapOrder(byte[] a_in, int a_index)
        {
            Debug.Assert(a_index >= 0);
            Debug.Assert(a_index + 4 <= a_in.Length);
            return (uint)((int)a_in[a_index++] << 24 | (int)a_in[a_index++] << 16 | (int)a_in[a_index++] << 8 | (int)a_in[a_index]);
        }

        public static uint ConvertBytesToUInt(byte[] a_in, int a_index = 0)
        {
            Debug.Assert(a_index >= 0);
            Debug.Assert(a_index + 4 <= a_in.Length);
            return (uint)((int)a_in[a_index++] | (int)a_in[a_index++] << 8 | (int)a_in[a_index++] << 16 | (int)a_in[a_index] << 24);
        }

        public static ulong[] ConvertBytesToULongsSwapOrder(byte[] a_in, int a_index, int a_length)
        {
            Converters.Check<byte>(a_in, 1, 8, a_index, a_length);
            ulong[] array = new ulong[a_length / 8];
            Converters.ConvertBytesToULongsSwapOrder(a_in, a_index, a_length, array);
            return array;
        }

        public static void ConvertBytesToULongsSwapOrder(byte[] a_in, int a_index, int a_length, ulong[] a_out)
        {
            Converters.Check<byte>(a_in, 1, 8, a_index, a_length);
            int num = 0;
            while (a_length > 0)
            {
                a_out[num++] = ((ulong)a_in[a_index++] << 56 | (ulong)a_in[a_index++] << 48 | (ulong)a_in[a_index++] << 40 | (ulong)a_in[a_index++] << 32 | (ulong)a_in[a_index++] << 24 | (ulong)a_in[a_index++] << 16 | (ulong)a_in[a_index++] << 8 | (ulong)a_in[a_index++]);
                a_length -= 8;
            }
        }

        public static byte[] ConvertUIntsToBytesSwapOrder(uint[] a_in, int a_index = 0, int a_length = -1)
        {
            bool flag = a_length == -1;
            if (flag)
            {
                a_length = a_in.Length;
            }
            Converters.Check<uint>(a_in, 4, 1, a_index, a_length);
            byte[] array = new byte[a_length * 4];
            int num = 0;
            while (a_length > 0)
            {
                array[num++] = (byte)(a_in[a_index] >> 24);
                array[num++] = (byte)(a_in[a_index] >> 16);
                array[num++] = (byte)(a_in[a_index] >> 8);
                array[num++] = (byte)a_in[a_index];
                a_length--;
                a_index++;
            }
            return array;
        }

        public static byte[] ConvertUIntsToBytes(uint[] a_in, int a_index = 0, int a_length = -1)
        {
            bool flag = a_length == -1;
            if (flag)
            {
                a_length = a_in.Length;
            }
            Converters.Check<uint>(a_in, 4, 1, a_index, a_length);
            byte[] array = new byte[a_length * 4];
            Buffer.BlockCopy(a_in, a_index * 4, array, 0, a_length * 4);
            return array;
        }

        public static byte[] ConvertULongsToBytes(ulong[] a_in, int a_index = 0, int a_length = -1)
        {
            bool flag = a_length == -1;
            if (flag)
            {
                a_length = a_in.Length;
            }
            Converters.Check<ulong>(a_in, 8, 1, a_index, a_length);
            byte[] array = new byte[a_length * 8];
            Buffer.BlockCopy(a_in, a_index * 8, array, 0, a_length * 8);
            return array;
        }

        public static byte[] ConvertULongsToBytesSwapOrder(ulong[] a_in, int a_index = 0, int a_length = -1)
        {
            bool flag = a_length == -1;
            if (flag)
            {
                a_length = a_in.Length;
            }
            Converters.Check<ulong>(a_in, 8, 1, a_index, a_length);
            byte[] array = new byte[a_length * 8];
            int num = 0;
            while (a_length > 0)
            {
                array[num++] = (byte)(a_in[a_index] >> 56);
                array[num++] = (byte)(a_in[a_index] >> 48);
                array[num++] = (byte)(a_in[a_index] >> 40);
                array[num++] = (byte)(a_in[a_index] >> 32);
                array[num++] = (byte)(a_in[a_index] >> 24);
                array[num++] = (byte)(a_in[a_index] >> 16);
                array[num++] = (byte)(a_in[a_index] >> 8);
                array[num++] = (byte)a_in[a_index];
                a_length--;
                a_index++;
            }
            return array;
        }

        public static byte[] ConvertUIntToBytes(uint a_in)
        {
            byte[] array = new byte[4];
            Converters.ConvertUIntToBytes((ulong)a_in, array, 0);
            return array;
        }

        public static void ConvertUIntToBytes(ulong a_in, byte[] a_out, int a_index)
        {
            Debug.Assert(a_index + 4 <= a_out.Length);
            Array.Copy(BitConverter.GetBytes(a_in), 0, a_out, a_index, 4);
        }

        public static byte[] ConvertULongToBytes(ulong a_in)
        {
            byte[] array = new byte[8];
            Converters.ConvertULongToBytes(a_in, array, 0);
            return array;
        }

        public static void ConvertULongToBytes(ulong a_in, byte[] a_out, int a_index)
        {
            Debug.Assert(a_index + 8 <= a_out.Length);
            Array.Copy(BitConverter.GetBytes(a_in), 0, a_out, a_index, 8);
        }

        public static void ConvertULongToBytesSwapOrder(ulong a_in, byte[] a_out, int a_index)
        {
            Debug.Assert(a_index + 8 <= a_out.Length);
            a_out[a_index++] = (byte)(a_in >> 56);
            a_out[a_index++] = (byte)(a_in >> 48);
            a_out[a_index++] = (byte)(a_in >> 40);
            a_out[a_index++] = (byte)(a_in >> 32);
            a_out[a_index++] = (byte)(a_in >> 24);
            a_out[a_index++] = (byte)(a_in >> 16);
            a_out[a_index++] = (byte)(a_in >> 8);
            a_out[a_index++] = (byte)a_in;
        }

        public static byte[] ConvertStringToBytes(string a_in)
        {
            byte[] array = new byte[a_in.Length * 2];
            Buffer.BlockCopy(a_in.ToCharArray(), 0, array, 0, array.Length);
            return array;
        }

        public static byte[] ConvertStringToBytes(string a_in, Encoding a_encoding)
        {
            return a_encoding.GetBytes(a_in);
        }

        public static byte[] ConvertCharsToBytes(char[] a_in)
        {
            Converters.Check<char>(a_in, 2, 1);
            byte[] array = new byte[a_in.Length * 2];
            Buffer.BlockCopy(a_in, 0, array, 0, array.Length);
            return array;
        }

        public static byte[] ConvertShortsToBytes(short[] a_in)
        {
            Converters.Check<short>(a_in, 2, 1);
            byte[] array = new byte[a_in.Length * 2];
            Buffer.BlockCopy(a_in, 0, array, 0, array.Length);
            return array;
        }

        public static byte[] ConvertUShortsToBytes(ushort[] a_in)
        {
            Converters.Check<ushort>(a_in, 2, 1);
            byte[] array = new byte[a_in.Length * 2];
            Buffer.BlockCopy(a_in, 0, array, 0, array.Length);
            return array;
        }

        public static byte[] ConvertIntsToBytes(int[] a_in)
        {
            Converters.Check<int>(a_in, 4, 1);
            byte[] array = new byte[a_in.Length * 4];
            Buffer.BlockCopy(a_in, 0, array, 0, a_in.Length * 4);
            return array;
        }

        public static byte[] ConvertLongsToBytes(long[] a_in, int a_index = 0, int a_length = -1)
        {
            bool flag = a_length == -1;
            if (flag)
            {
                a_length = a_in.Length;
            }
            Converters.Check<long>(a_in, 8, 1, a_index, a_length);
            byte[] array = new byte[a_length * 8];
            Buffer.BlockCopy(a_in, a_index * 8, array, 0, a_length * 8);
            return array;
        }

        public static byte[] ConvertDoublesToBytes(double[] a_in, int a_index = 0, int a_length = -1)
        {
            bool flag = a_length == -1;
            if (flag)
            {
                a_length = a_in.Length;
            }
            Converters.Check<double>(a_in, 8, 1, a_index, a_length);
            byte[] array = new byte[a_length * 8];
            Buffer.BlockCopy(a_in, a_index * 8, array, 0, a_length * 8);
            return array;
        }

        public static byte[] ConvertFloatsToBytes(float[] a_in, int a_index = 0, int a_length = -1)
        {
            bool flag = a_length == -1;
            if (flag)
            {
                a_length = a_in.Length;
            }
            Converters.Check<float>(a_in, 4, 1, a_index, a_length);
            byte[] array = new byte[a_length * 4];
            Buffer.BlockCopy(a_in, a_index * 4, array, 0, a_length * 4);
            return array;
        }

        public static ulong[] ConvertFloatsToULongs(float[] a_in, int a_index = 0, int a_length = -1)
        {
            bool flag = a_length == -1;
            if (flag)
            {
                a_length = a_in.Length;
            }
            Converters.Check<float>(a_in, 4, 8, a_index, a_length);
            ulong[] array = new ulong[a_length / 2];
            Converters.ConvertFloatsToULongs(a_in, a_index, a_length, array, 0);
            return array;
        }

        public static void ConvertFloatsToULongs(float[] a_in, int a_index_in, int a_length, ulong[] a_out, int a_index_out)
        {
            Converters.Check<float, ulong>(a_in, 4, a_out, 8, a_index_in, a_length, a_index_out);
            Buffer.BlockCopy(a_in, a_index_in, a_out, a_index_out * 8, a_length * 4);
        }

        public static byte[] ConvertHexStringToBytes(string a_in)
        {
            a_in = a_in.Replace("-", "");
            Debug.Assert(a_in.Length % 2 == 0);
            byte[] array = new byte[a_in.Length / 2];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = byte.Parse(a_in.Substring(i * 2, 2), NumberStyles.HexNumber);
            }
            return array;
        }

        public static string ConvertBytesToHexString(byte[] a_in, bool a_group = true)
        {
            string text = BitConverter.ToString(a_in).ToUpper();
            if (a_group)
            {
                Converters.Check<byte>(a_in, 1, 4);
                string[] array = BitConverter.ToString(a_in).ToUpper().Split(new char[]
                {
                    '-'
                });
                text = "";
                for (int i = 0; i < array.Length / 4; i++)
                {
                    bool flag = i != 0;
                    if (flag)
                    {
                        text += "-";
                    }
                    text = string.Concat(new string[]
                    {
                        text,
                        array[i * 4],
                        array[i * 4 + 1],
                        array[i * 4 + 2],
                        array[i * 4 + 3]
                    });
                }
            }
            else
            {
                text = text.Replace("-", "");
            }
            return text;
        }

        [Conditional("DEBUG")]
        private static void Check<I>(I[] a_in, int a_in_size, int a_out_size)
        {
            Debug.Assert(a_in.Length * a_in_size % a_out_size == 0);
        }

        [Conditional("DEBUG")]
        private static void Check<I>(I[] a_in, int a_in_size, int a_out_size, int a_index, int a_length)
        {
            Debug.Assert(a_length * a_in_size % a_out_size == 0);
            bool flag = a_out_size > a_in_size;
            if (flag)
            {
                Debug.Assert(a_length % (a_out_size / a_in_size) == 0);
            }
            else
            {
                Debug.Assert(a_in_size % a_out_size == 0);
            }
            Debug.Assert(a_index >= 0);
            bool flag2 = a_length > 0;
            if (flag2)
            {
                Debug.Assert(a_index < a_in.Length);
            }
            Debug.Assert(a_length >= 0);
            Debug.Assert(a_index + a_length <= a_in.Length);
            Debug.Assert(a_index + a_length <= a_in.Length);
        }

        [Conditional("DEBUG")]
        private static void Check<I, O>(I[] a_in, int a_in_size, O[] a_result, int a_out_size, int a_index_in, int a_length, int a_index_out)
        {
            Debug.Assert(a_length * a_in_size % a_out_size == 0);
            bool flag = a_out_size > a_in_size;
            if (flag)
            {
                Debug.Assert(a_length % (a_out_size / a_in_size) == 0);
            }
            Debug.Assert(a_index_in >= 0);
            bool flag2 = a_length > 0;
            if (flag2)
            {
                Debug.Assert(a_index_in < a_in.Length);
            }
            Debug.Assert(a_length >= 0);
            Debug.Assert(a_index_in + a_length <= a_in.Length);
            Debug.Assert(a_index_in + a_length <= a_in.Length);
            Debug.Assert(a_index_out + a_result.Length >= a_length / a_out_size);
        }
    }
}