namespace HashLib
{
    public interface IHMACBuildIn : IHMAC, IWithKey, IHash, ICrypto, IBlockHash, ICryptoBuildIn
    {
    }
}