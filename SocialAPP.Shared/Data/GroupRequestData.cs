﻿using ProtoBuf;
using System;

namespace SocialAPP.Shared.Data
{
    [ProtoContract, Serializable]
    public class GroupRequestData
    {
        [ProtoMember(1)]
        public string Id
        {
            get;
            set;
        }
        [ProtoMember(2)]
        public string GroupId
        {
            get;
            set;
        }
        [ProtoMember(3)]
        public string UserId
        {
            get;
            set;
        }
        [ProtoMember(4)]
        public DateTime DateAt
        {
            get;
            set;
        }
    }
}