﻿using ProtoBuf;
using System;

namespace SocialAPP.Shared.Data
{
    [ProtoContract, Serializable]
    public class TransactionData
    {
        [ProtoMember(1)]
        public string Id
        {
            get;
            set;
        }

        [ProtoMember(2)]
        public string UserId
        {
            get;
            set;
        }

        [ProtoMember(3)]
        public string TokenId
        {
            get;
            set;
        }

        [ProtoMember(4)]
        public int Amount
        {
            get;
            set;
        }

        [ProtoMember(5)]
        public string Currency
        {
            get;
            set;
        }

        [ProtoMember(6)]
        public DateTime DateAt
        {
            get;
            set;
        }

        [ProtoMember(7)]
        public DateTime ExpireAt
        {
            get;
            set;
        }
    }
}