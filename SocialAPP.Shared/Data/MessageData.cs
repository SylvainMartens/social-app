﻿using ProtoBuf;
using System;

namespace SocialAPP.Shared.Data
{
    [ProtoContract, Serializable]
    public class MessageData
    {
        [ProtoMember(1)]
        public string Id { get; set; }

        [ProtoMember(2)]
        public string ConversationId { get; set; }

        [ProtoMember(3)]
        public string FromId { get; set; }

        [ProtoMember(4)]
        public string ToId { get; set; }

        [ProtoMember(5)]
        public DateTime When { get; set; }

        [ProtoMember(6)]
        public DateTime? SeenAt { get; set; }

        [ProtoMember(7)]
        public string Message { get; set; }

        [ProtoMember(8)]
        public string AttachmentId { get; set; }
    }
}