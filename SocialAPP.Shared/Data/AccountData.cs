using ProtoBuf;
using System;

namespace SocialAPP.Shared.Data
{
    [ProtoContract, Serializable]
    public class AccountData
    {
        [ProtoMember(1)]
        public string Id
        {
            get;
            set;
        }

        [ProtoMember(2)]
        public string Username
        {
            get;
            set;
        }

        [ProtoMember(3)]
        public string Password
        {
            get;
            set;
        }

        [ProtoMember(4)]
        public string Email
        {
            get;
            set;
        }

        [ProtoMember(5)]
        public string Firstname
        {
            get;
            set;
        }

        [ProtoMember(6)]
        public string Lastname
        {
            get;
            set;
        }

        [ProtoMember(7)]
        public string CreationIp
        {
            get;
            set;
        }

        [ProtoMember(8)]
        public string LastIp
        {
            get;
            set;
        }

        [ProtoMember(9)]
        public bool IsSuspended
        {
            get;
            set;
        }

        [ProtoMember(10)]
        public string SuspendedReason
        {
            get;
            set;
        }

        [ProtoMember(11)]
        public bool IsBanned
        {
            get;
            set;
        }

        [ProtoMember(12)]
        public string BannedReason
        {
            get;
            set;
        }

        [ProtoMember(13)]
        public string Avatar
        {
            get;
            set;
        }

        [ProtoMember(14)]
        public int PremiumMode
        {
            get;
            set;
        }

        [ProtoMember(15)]
        public bool IsPremium
        {
            get;
            set;
        }

        [ProtoMember(16)]
        public double Latitude
        {
            get;
            set;
        }

        [ProtoMember(17)]
        public double Longitude
        {
            get;
            set;
        }

        [ProtoMember(18)]
        public string Dob_d
        {
            get;
            set;
        }

        [ProtoMember(19)]
        public string Dob_m
        {
            get;
            set;
        }

        [ProtoMember(20)]
        public string Dob_y
        {
            get;
            set;
        }

        [ProtoMember(21)]
        public int Gender
        {
            get;
            set;
        }

        [ProtoMember(22)]
        public string City
        {
            get;
            set;
        }

        [ProtoMember(23)]
        public string Job
        {
            get;
            set;
        }

        [ProtoMember(24)]
        public string Activities
        {
            get;
            set;
        }

        [ProtoMember(25)]
        public string Description
        {
            get;
            set;
        }

        [ProtoMember(26)]
        public string LookingFor
        {
            get;
            set;
        }

        [ProtoMember(27)]
        public string LookingForDescription
        {
            get;
            set;
        }

        [ProtoMember(28)]
        public bool IsBlocked
        {
            get;
            set;
        }
    }
}