﻿using Hazel;
using Hazel.Udp;
using System;
using System.Net;

namespace SocialAPP.Shared
{
    public static class CallServer
    {
        private static ConnectionListener listener;
        private static Connection RemoteConnection;

        public static Action<AudioPacket> OnReceiveAudioPacket;

        public static string CallId { get; set; }

        public static void Start(string callId)
        {
            CallId = callId;
            AppDomain.CurrentDomain.ProcessExit += delegate (object sender, EventArgs e)
            {
                listener?.Close();
            };
            if(listener == null)
            {
                listener = new UdpConnectionListener(new NetworkEndPoint(IPAddress.Any, 4298));
                listener.NewConnection += Listener_NewConnection;
                //OnLog(string.Format("Server mounted, listening on port {0}", Port));
                listener.Start();
            }
        }

        public static void Stop(string callId, bool forced = false)
        {
            if(!forced && callId != CallId) return;
            listener.Close();
            listener.Dispose();
            listener = null;
            RemoteConnection = null;
        }

        private static void Listener_NewConnection(object sender, NewConnectionEventArgs args)
        {
            args.Connection.DataReceived += Connection_DataReceived;
            args.Connection.Disconnected += Connection_Disconnected;
            args.Recycle();
        }

        private static void Connection_Disconnected(object sender, DisconnectedEventArgs args)
        {
            Connection connection = (Connection)sender;
            if (connection == null || connection.EndPoint == null) return;

            if(connection.EndPoint == RemoteConnection.EndPoint)
            {
                //On End Call Here...
                RemoteConnection = null;
            }
        }

        private static void Connection_DataReceived(object sender, DataReceivedEventArgs args)
        {
            Connection connection = (Connection)sender;
            
            if (args.Bytes == null) return;
            var callMessage = ProtoSerializer.Deserialize<CallMessage>(args.Bytes);
            if (callMessage == null) return;

            switch(callMessage.Tag)
            {
                case CallTags.SET_CALL:
                {
                    var callId = callMessage.ReadString();
                    if(callId == CallId && RemoteConnection == null)
                    {
                        RemoteConnection = connection;
                    }
                }
                break;
                case CallTags.SYNC_AUDIO:
                {
                    if(RemoteConnection != null && RemoteConnection.EndPoint == connection.EndPoint)
                    {
                        var audioPacket = ProtoSerializer.Deserialize<AudioPacket>(callMessage.Data);
                        if (audioPacket != null)
                            OnReceiveAudioPacket?.Invoke(audioPacket);
                    }
                }
                break;
                case CallTags.SYNC_VIDEO:
                {
                    //TODO...
                }
                break;
                case CallTags.DISCONNECTED:
                {
                    connection.Close();
                }
                break;
            }
            args.Recycle();
        }

        public static void SyncAudio(AudioPacket audioPacket)
        {

        }
    }
}