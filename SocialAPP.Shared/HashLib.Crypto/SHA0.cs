namespace HashLib.Crypto
{
    internal class SHA0 : BlockHash, ICryptoNotBuildIn, ICrypto, IHash, IBlockHash
    {
        protected const uint C1 = 1518500249u;

        protected const uint C2 = 1859775393u;

        protected const uint C3 = 2400959708u;

        protected const uint C4 = 3395469782u;

        protected uint[] m_state = new uint[5];

        public SHA0() : base(20, 64, -1)
        {
            this.Initialize();
        }

        public override void Initialize()
        {
            this.m_state[0] = 1732584193u;
            this.m_state[1] = 4023233417u;
            this.m_state[2] = 2562383102u;
            this.m_state[3] = 271733878u;
            this.m_state[4] = 3285377520u;
            base.Initialize();
        }

        protected override void Finish()
        {
            ulong a_in = this.m_processed_bytes * 8uL;
            int num = (this.m_buffer.Pos < 56) ? (56 - this.m_buffer.Pos) : (120 - this.m_buffer.Pos);
            byte[] array = new byte[num + 8];
            array[0] = 128;
            Converters.ConvertULongToBytesSwapOrder(a_in, array, num);
            num += 8;
            this.TransformBytes(array, 0, num);
        }

        protected virtual void Expand(uint[] a_data)
        {
            for (int i = 16; i < 80; i++)
            {
                a_data[i] = (a_data[i - 3] ^ a_data[i - 8] ^ a_data[i - 14] ^ a_data[i - 16]);
            }
        }

        protected override void TransformBlock(byte[] a_data, int a_index)
        {
            uint[] array = new uint[80];
            Converters.ConvertBytesToUIntsSwapOrder(a_data, a_index, this.BlockSize, array, 0);
            this.Expand(array);
            uint num = this.m_state[0];
            uint num2 = this.m_state[1];
            uint num3 = this.m_state[2];
            uint num4 = this.m_state[3];
            uint num5 = this.m_state[4];
            int num6 = 0;
            uint num7 = array[num6++] + 1518500249u + (num << 5 | num >> 27) + ((num2 & num3) | (~num2 & num4)) + num5;
            uint num8 = num2 << 30 | num2 >> 2;
            uint num9 = array[num6++] + 1518500249u + (num7 << 5 | num7 >> 27) + ((num & num8) | (~num & num3)) + num4;
            uint num10 = num << 30 | num >> 2;
            uint num11 = array[num6++] + 1518500249u + (num9 << 5 | num9 >> 27) + ((num7 & num10) | (~num7 & num8)) + num3;
            uint num12 = num7 << 30 | num7 >> 2;
            uint num13 = array[num6++] + 1518500249u + (num11 << 5 | num11 >> 27) + ((num9 & num12) | (~num9 & num10)) + num8;
            uint num14 = num9 << 30 | num9 >> 2;
            uint num15 = array[num6++] + 1518500249u + (num13 << 5 | num13 >> 27) + ((num11 & num14) | (~num11 & num12)) + num10;
            uint num16 = num11 << 30 | num11 >> 2;
            uint num17 = array[num6++] + 1518500249u + (num15 << 5 | num15 >> 27) + ((num13 & num16) | (~num13 & num14)) + num12;
            uint num18 = num13 << 30 | num13 >> 2;
            uint num19 = array[num6++] + 1518500249u + (num17 << 5 | num17 >> 27) + ((num15 & num18) | (~num15 & num16)) + num14;
            uint num20 = num15 << 30 | num15 >> 2;
            uint num21 = array[num6++] + 1518500249u + (num19 << 5 | num19 >> 27) + ((num17 & num20) | (~num17 & num18)) + num16;
            uint num22 = num17 << 30 | num17 >> 2;
            num7 = array[num6++] + 1518500249u + (num21 << 5 | num21 >> 27) + ((num19 & num22) | (~num19 & num20)) + num18;
            num8 = (num19 << 30 | num19 >> 2);
            num9 = array[num6++] + 1518500249u + (num7 << 5 | num7 >> 27) + ((num21 & num8) | (~num21 & num22)) + num20;
            num10 = (num21 << 30 | num21 >> 2);
            num11 = array[num6++] + 1518500249u + (num9 << 5 | num9 >> 27) + ((num7 & num10) | (~num7 & num8)) + num22;
            num12 = (num7 << 30 | num7 >> 2);
            num13 = array[num6++] + 1518500249u + (num11 << 5 | num11 >> 27) + ((num9 & num12) | (~num9 & num10)) + num8;
            num14 = (num9 << 30 | num9 >> 2);
            num15 = array[num6++] + 1518500249u + (num13 << 5 | num13 >> 27) + ((num11 & num14) | (~num11 & num12)) + num10;
            num16 = (num11 << 30 | num11 >> 2);
            num17 = array[num6++] + 1518500249u + (num15 << 5 | num15 >> 27) + ((num13 & num16) | (~num13 & num14)) + num12;
            num18 = (num13 << 30 | num13 >> 2);
            num19 = array[num6++] + 1518500249u + (num17 << 5 | num17 >> 27) + ((num15 & num18) | (~num15 & num16)) + num14;
            num20 = (num15 << 30 | num15 >> 2);
            num21 = array[num6++] + 1518500249u + (num19 << 5 | num19 >> 27) + ((num17 & num20) | (~num17 & num18)) + num16;
            num22 = (num17 << 30 | num17 >> 2);
            num7 = array[num6++] + 1518500249u + (num21 << 5 | num21 >> 27) + ((num19 & num22) | (~num19 & num20)) + num18;
            num8 = (num19 << 30 | num19 >> 2);
            num9 = array[num6++] + 1518500249u + (num7 << 5 | num7 >> 27) + ((num21 & num8) | (~num21 & num22)) + num20;
            num10 = (num21 << 30 | num21 >> 2);
            num11 = array[num6++] + 1518500249u + (num9 << 5 | num9 >> 27) + ((num7 & num10) | (~num7 & num8)) + num22;
            num12 = (num7 << 30 | num7 >> 2);
            num13 = array[num6++] + 1518500249u + (num11 << 5 | num11 >> 27) + ((num9 & num12) | (~num9 & num10)) + num8;
            num14 = (num9 << 30 | num9 >> 2);
            num15 = array[num6++] + 1859775393u + (num13 << 5 | num13 >> 27) + (num11 ^ num14 ^ num12) + num10;
            num16 = (num11 << 30 | num11 >> 2);
            num17 = array[num6++] + 1859775393u + (num15 << 5 | num15 >> 27) + (num13 ^ num16 ^ num14) + num12;
            num18 = (num13 << 30 | num13 >> 2);
            num19 = array[num6++] + 1859775393u + (num17 << 5 | num17 >> 27) + (num15 ^ num18 ^ num16) + num14;
            num20 = (num15 << 30 | num15 >> 2);
            num21 = array[num6++] + 1859775393u + (num19 << 5 | num19 >> 27) + (num17 ^ num20 ^ num18) + num16;
            num22 = (num17 << 30 | num17 >> 2);
            num7 = array[num6++] + 1859775393u + (num21 << 5 | num21 >> 27) + (num19 ^ num22 ^ num20) + num18;
            num8 = (num19 << 30 | num19 >> 2);
            num9 = array[num6++] + 1859775393u + (num7 << 5 | num7 >> 27) + (num21 ^ num8 ^ num22) + num20;
            num10 = (num21 << 30 | num21 >> 2);
            num11 = array[num6++] + 1859775393u + (num9 << 5 | num9 >> 27) + (num7 ^ num10 ^ num8) + num22;
            num12 = (num7 << 30 | num7 >> 2);
            num13 = array[num6++] + 1859775393u + (num11 << 5 | num11 >> 27) + (num9 ^ num12 ^ num10) + num8;
            num14 = (num9 << 30 | num9 >> 2);
            num15 = array[num6++] + 1859775393u + (num13 << 5 | num13 >> 27) + (num11 ^ num14 ^ num12) + num10;
            num16 = (num11 << 30 | num11 >> 2);
            num17 = array[num6++] + 1859775393u + (num15 << 5 | num15 >> 27) + (num13 ^ num16 ^ num14) + num12;
            num18 = (num13 << 30 | num13 >> 2);
            num19 = array[num6++] + 1859775393u + (num17 << 5 | num17 >> 27) + (num15 ^ num18 ^ num16) + num14;
            num20 = (num15 << 30 | num15 >> 2);
            num21 = array[num6++] + 1859775393u + (num19 << 5 | num19 >> 27) + (num17 ^ num20 ^ num18) + num16;
            num22 = (num17 << 30 | num17 >> 2);
            num7 = array[num6++] + 1859775393u + (num21 << 5 | num21 >> 27) + (num19 ^ num22 ^ num20) + num18;
            num8 = (num19 << 30 | num19 >> 2);
            num9 = array[num6++] + 1859775393u + (num7 << 5 | num7 >> 27) + (num21 ^ num8 ^ num22) + num20;
            num10 = (num21 << 30 | num21 >> 2);
            num11 = array[num6++] + 1859775393u + (num9 << 5 | num9 >> 27) + (num7 ^ num10 ^ num8) + num22;
            num12 = (num7 << 30 | num7 >> 2);
            num13 = array[num6++] + 1859775393u + (num11 << 5 | num11 >> 27) + (num9 ^ num12 ^ num10) + num8;
            num14 = (num9 << 30 | num9 >> 2);
            num15 = array[num6++] + 1859775393u + (num13 << 5 | num13 >> 27) + (num11 ^ num14 ^ num12) + num10;
            num16 = (num11 << 30 | num11 >> 2);
            num17 = array[num6++] + 1859775393u + (num15 << 5 | num15 >> 27) + (num13 ^ num16 ^ num14) + num12;
            num18 = (num13 << 30 | num13 >> 2);
            num19 = array[num6++] + 1859775393u + (num17 << 5 | num17 >> 27) + (num15 ^ num18 ^ num16) + num14;
            num20 = (num15 << 30 | num15 >> 2);
            num21 = array[num6++] + 1859775393u + (num19 << 5 | num19 >> 27) + (num17 ^ num20 ^ num18) + num16;
            num22 = (num17 << 30 | num17 >> 2);
            num7 = array[num6++] + 2400959708u + (num21 << 5 | num21 >> 27) + ((num19 & num22) | (num19 & num20) | (num22 & num20)) + num18;
            num8 = (num19 << 30 | num19 >> 2);
            num9 = array[num6++] + 2400959708u + (num7 << 5 | num7 >> 27) + ((num21 & num8) | (num21 & num22) | (num8 & num22)) + num20;
            num10 = (num21 << 30 | num21 >> 2);
            num11 = array[num6++] + 2400959708u + (num9 << 5 | num9 >> 27) + ((num7 & num10) | (num7 & num8) | (num10 & num8)) + num22;
            num12 = (num7 << 30 | num7 >> 2);
            num13 = array[num6++] + 2400959708u + (num11 << 5 | num11 >> 27) + ((num9 & num12) | (num9 & num10) | (num12 & num10)) + num8;
            num14 = (num9 << 30 | num9 >> 2);
            num15 = array[num6++] + 2400959708u + (num13 << 5 | num13 >> 27) + ((num11 & num14) | (num11 & num12) | (num14 & num12)) + num10;
            num16 = (num11 << 30 | num11 >> 2);
            num17 = array[num6++] + 2400959708u + (num15 << 5 | num15 >> 27) + ((num13 & num16) | (num13 & num14) | (num16 & num14)) + num12;
            num18 = (num13 << 30 | num13 >> 2);
            num19 = array[num6++] + 2400959708u + (num17 << 5 | num17 >> 27) + ((num15 & num18) | (num15 & num16) | (num18 & num16)) + num14;
            num20 = (num15 << 30 | num15 >> 2);
            num21 = array[num6++] + 2400959708u + (num19 << 5 | num19 >> 27) + ((num17 & num20) | (num17 & num18) | (num20 & num18)) + num16;
            num22 = (num17 << 30 | num17 >> 2);
            num7 = array[num6++] + 2400959708u + (num21 << 5 | num21 >> 27) + ((num19 & num22) | (num19 & num20) | (num22 & num20)) + num18;
            num8 = (num19 << 30 | num19 >> 2);
            num9 = array[num6++] + 2400959708u + (num7 << 5 | num7 >> 27) + ((num21 & num8) | (num21 & num22) | (num8 & num22)) + num20;
            num10 = (num21 << 30 | num21 >> 2);
            num11 = array[num6++] + 2400959708u + (num9 << 5 | num9 >> 27) + ((num7 & num10) | (num7 & num8) | (num10 & num8)) + num22;
            num12 = (num7 << 30 | num7 >> 2);
            num13 = array[num6++] + 2400959708u + (num11 << 5 | num11 >> 27) + ((num9 & num12) | (num9 & num10) | (num12 & num10)) + num8;
            num14 = (num9 << 30 | num9 >> 2);
            num15 = array[num6++] + 2400959708u + (num13 << 5 | num13 >> 27) + ((num11 & num14) | (num11 & num12) | (num14 & num12)) + num10;
            num16 = (num11 << 30 | num11 >> 2);
            num17 = array[num6++] + 2400959708u + (num15 << 5 | num15 >> 27) + ((num13 & num16) | (num13 & num14) | (num16 & num14)) + num12;
            num18 = (num13 << 30 | num13 >> 2);
            num19 = array[num6++] + 2400959708u + (num17 << 5 | num17 >> 27) + ((num15 & num18) | (num15 & num16) | (num18 & num16)) + num14;
            num20 = (num15 << 30 | num15 >> 2);
            num21 = array[num6++] + 2400959708u + (num19 << 5 | num19 >> 27) + ((num17 & num20) | (num17 & num18) | (num20 & num18)) + num16;
            num22 = (num17 << 30 | num17 >> 2);
            num7 = array[num6++] + 2400959708u + (num21 << 5 | num21 >> 27) + ((num19 & num22) | (num19 & num20) | (num22 & num20)) + num18;
            num8 = (num19 << 30 | num19 >> 2);
            num9 = array[num6++] + 2400959708u + (num7 << 5 | num7 >> 27) + ((num21 & num8) | (num21 & num22) | (num8 & num22)) + num20;
            num10 = (num21 << 30 | num21 >> 2);
            num11 = array[num6++] + 2400959708u + (num9 << 5 | num9 >> 27) + ((num7 & num10) | (num7 & num8) | (num10 & num8)) + num22;
            num12 = (num7 << 30 | num7 >> 2);
            num13 = array[num6++] + 2400959708u + (num11 << 5 | num11 >> 27) + ((num9 & num12) | (num9 & num10) | (num12 & num10)) + num8;
            num14 = (num9 << 30 | num9 >> 2);
            num15 = array[num6++] + 3395469782u + (num13 << 5 | num13 >> 27) + (num11 ^ num14 ^ num12) + num10;
            num16 = (num11 << 30 | num11 >> 2);
            num17 = array[num6++] + 3395469782u + (num15 << 5 | num15 >> 27) + (num13 ^ num16 ^ num14) + num12;
            num18 = (num13 << 30 | num13 >> 2);
            num19 = array[num6++] + 3395469782u + (num17 << 5 | num17 >> 27) + (num15 ^ num18 ^ num16) + num14;
            num20 = (num15 << 30 | num15 >> 2);
            num21 = array[num6++] + 3395469782u + (num19 << 5 | num19 >> 27) + (num17 ^ num20 ^ num18) + num16;
            num22 = (num17 << 30 | num17 >> 2);
            num7 = array[num6++] + 3395469782u + (num21 << 5 | num21 >> 27) + (num19 ^ num22 ^ num20) + num18;
            num8 = (num19 << 30 | num19 >> 2);
            num9 = array[num6++] + 3395469782u + (num7 << 5 | num7 >> 27) + (num21 ^ num8 ^ num22) + num20;
            num10 = (num21 << 30 | num21 >> 2);
            num11 = array[num6++] + 3395469782u + (num9 << 5 | num9 >> 27) + (num7 ^ num10 ^ num8) + num22;
            num12 = (num7 << 30 | num7 >> 2);
            num13 = array[num6++] + 3395469782u + (num11 << 5 | num11 >> 27) + (num9 ^ num12 ^ num10) + num8;
            num14 = (num9 << 30 | num9 >> 2);
            num15 = array[num6++] + 3395469782u + (num13 << 5 | num13 >> 27) + (num11 ^ num14 ^ num12) + num10;
            num16 = (num11 << 30 | num11 >> 2);
            num17 = array[num6++] + 3395469782u + (num15 << 5 | num15 >> 27) + (num13 ^ num16 ^ num14) + num12;
            num18 = (num13 << 30 | num13 >> 2);
            num19 = array[num6++] + 3395469782u + (num17 << 5 | num17 >> 27) + (num15 ^ num18 ^ num16) + num14;
            num20 = (num15 << 30 | num15 >> 2);
            num21 = array[num6++] + 3395469782u + (num19 << 5 | num19 >> 27) + (num17 ^ num20 ^ num18) + num16;
            num22 = (num17 << 30 | num17 >> 2);
            num7 = array[num6++] + 3395469782u + (num21 << 5 | num21 >> 27) + (num19 ^ num22 ^ num20) + num18;
            num8 = (num19 << 30 | num19 >> 2);
            num9 = array[num6++] + 3395469782u + (num7 << 5 | num7 >> 27) + (num21 ^ num8 ^ num22) + num20;
            num10 = (num21 << 30 | num21 >> 2);
            num11 = array[num6++] + 3395469782u + (num9 << 5 | num9 >> 27) + (num7 ^ num10 ^ num8) + num22;
            num12 = (num7 << 30 | num7 >> 2);
            num13 = array[num6++] + 3395469782u + (num11 << 5 | num11 >> 27) + (num9 ^ num12 ^ num10) + num8;
            num14 = (num9 << 30 | num9 >> 2);
            num15 = array[num6++] + 3395469782u + (num13 << 5 | num13 >> 27) + (num11 ^ num14 ^ num12) + num10;
            num16 = (num11 << 30 | num11 >> 2);
            num17 = array[num6++] + 3395469782u + (num15 << 5 | num15 >> 27) + (num13 ^ num16 ^ num14) + num12;
            num18 = (num13 << 30 | num13 >> 2);
            num19 = array[num6++] + 3395469782u + (num17 << 5 | num17 >> 27) + (num15 ^ num18 ^ num16) + num14;
            num20 = (num15 << 30 | num15 >> 2);
            this.m_state[0] += array[num6++] + 3395469782u + (num19 << 5 | num19 >> 27) + (num17 ^ num20 ^ num18) + num16;
            this.m_state[1] += num19;
            this.m_state[2] += (num17 << 30 | num17 >> 2);
            this.m_state[3] += num20;
            this.m_state[4] += num18;
        }

        protected override byte[] GetResult()
        {
            return Converters.ConvertUIntsToBytesSwapOrder(this.m_state, 0, -1);
        }
    }
}