﻿namespace SocialAPP.Shared
{
    public static class CallTags
    {
        public const byte SET_CALL = 0;
        public const byte DISCONNECTED = 1;
        public const byte SYNC_AUDIO = 2;
        public const byte SYNC_VIDEO = 3;
    }
}