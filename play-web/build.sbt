name := "socialweb"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "mysql" % "mysql-connector-java" % "5.1.18",
  "commons-io" % "commons-io" % "2.4",
  "commons-codec" % "commons-codec" % "1.9"
)     

play.Project.playJavaSettings
