package models;

import javax.persistence.*;
import play.db.ebean.*;
import play.data.validation.*;

@SuppressWarnings("serial")
@Entity
@Table(name = "uploadTokens")
public class UploadToken extends Model {
	@Id
	@Column(name = "id")
    public String id;

	@Column(name = "accountId")
    @Constraints.Required
    public String AccountId;

	@Column(name = "ip")
    @Constraints.Required
    public String Ip;
    
    public static Model.Finder<Long, UploadToken> find = new Model.Finder<>(Long.class, UploadToken.class);
}
