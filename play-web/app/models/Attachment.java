package models;

import javax.persistence.*;
import play.db.ebean.*;
import play.data.validation.*;

@SuppressWarnings("serial")
@Entity
@Table(name = "attachments")
public class Attachment extends Model {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    private long id;
	
	public String GetId()
	{
		return id + "";
	}

	@Column(name = "hash")
    @Constraints.Required
    public String Hash;

	@Column(name = "path")
    @Constraints.Required
    public String Path;
	
    public static Model.Finder<Long, Attachment> find = new Model.Finder<>(Long.class, Attachment.class);
}
