package controllers;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Date;
import java.util.Iterator;
import java.util.UUID;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

import com.avaje.ebean.Ebean;

import play.mvc.Http.MultipartFormData.FilePart;
import models.Attachment;
import models.UploadToken;
import play.api.Play;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.Constraints;
import play.mvc.*;
import play.mvc.Http.MultipartFormData;

public class Application extends Controller
{

    @SuppressWarnings("deprecation")
	public static Result index()
    {
    	DynamicForm dynamicForm = Form.form().bindFromRequest();
        String attachmentId = dynamicForm.get("attachment");
        String avatar = dynamicForm.get("avatar");
        
        if (Constraints.required().isValid(attachmentId))
		{
        	Attachment attachment = Attachments.getAttachmentById(attachmentId);
        	if(attachment != null)
        	{
        		ByteArrayInputStream input = null;
        		try {
                    byte[] byteArray;
                    //System.out.println("assets/attachments/"+attachment.Path);
                    File file = Play.getFile("public/attachments/"+attachment.Path, Play.current());
                    byteArray = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
                    //byteArray = IOUtils.toByteArray(new FileInputStream(file));
                    input = new ByteArrayInputStream(byteArray);
                    
                    Date d = new Date(file.lastModified());
                    response().setHeader(CACHE_CONTROL, "public, max-age=31536000");
                    response().setHeader(LAST_MODIFIED, d.toGMTString());
                    
                    return ok(input).as("image/jpeg");
                } catch (Exception e) {
                }
        	}
		}else if (Constraints.required().isValid(avatar))
		{
			ByteArrayInputStream input = null;
    		try {
                byte[] byteArray;
                //System.out.println(avatar);
                String[] stringData = avatar.split("&");
                File file = Play.getFile(stringData[0].replace("assets", "public"), Play.current());
                byteArray = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
                //byteArray = IOUtils.toByteArray(new FileInputStream(file));
                input = new ByteArrayInputStream(byteArray);
                
                /*
                Date d = new Date(file.lastModified());
                response().setHeader(CACHE_CONTROL, "public, max-age=31536000");
                response().setHeader(LAST_MODIFIED, d.toGMTString());
                */
                return ok(input).as("image/jpeg");
            } catch (Exception e) {
            }
		}
    	return notFound();
    }
    
    public static Result uploadAvatar()
    {
    	DynamicForm dynamicForm = Form.form().bindFromRequest();
    	MultipartFormData body = request().body().asMultipartFormData();
    	
        String tokenId = dynamicForm.get("token");
        
        if (Constraints.required().isValid(tokenId))
		{
        	FilePart file = body.getFile("file");
        	if (file != null) {
    			String contentType = file.getContentType();
    			//TODO: Make sure content type is an image!
    			UploadToken uploadToken = UploadTokens.getTokenFromId(tokenId);
    			if(uploadToken != null)
    			{
    				File temp = new File("public/avatars/" + uploadToken.AccountId + ".png");
    			    if (temp.exists()) {
    			    	temp.delete();     
    			    }
    			    
    			    file.getFile().renameTo(new File("public/avatars/" + uploadToken.AccountId + ".png"));
    			    
    				//Once everything is done.
    				Ebean.delete(uploadToken);
    				
    				try {
						resize("public/avatars/" + uploadToken.AccountId + ".png", 500, 500);
						//compress("public/avatars/" + uploadToken.AccountId + ".png", 0.5f);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    				
    				return ok("success");
    			}
        	}
		}
    	return ok("");
    }
    
    public static String createSalt() {
        String ts = String.valueOf(System.currentTimeMillis());
        String rand = UUID.randomUUID().toString();
        return DigestUtils.shaHex(ts + rand);
    }
    
    public static Result uploadAttachment()
    {
    	MultipartFormData body = request().body().asMultipartFormData();
    	FilePart file = body.getFile("file");
    	
    	if (file != null) {
    		try {
    			String contentType = file.getContentType();
    			
    			//TODO: Make sure content type is an image!
    			File theFile = file.getFile();
    			String tempFilename = createSalt()+".png";
    			theFile.renameTo(new File("public/attachments/" + tempFilename));
    			
				String hash = DigestUtils.md5Hex(new FileInputStream("public/attachments/" + tempFilename));
    			//String hash = theFile.hashCode() + "";
				Attachment attachment = Attachments.getAttachmentByHash(hash);
				if(attachment != null)
				{
					new File("public/attachments/" + tempFilename).delete();
					return ok(attachment.GetId());
				}else{
					attachment = new Attachment();
					attachment.Hash = hash;
					attachment.Path = tempFilename;
					
					attachment.save();
					
					attachment = Attachments.getAttachmentByHash(hash);
					
					return ok(attachment.GetId());
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	return ok("");
    }

    public static void resize(String imagePath, int scaledWidth, int scaledHeight) throws IOException {
        // reads input image
        File inputFile = new File(imagePath);
        
        BufferedImage inputImage = ImageIO.read(inputFile);
        
        inputFile.delete();
        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth, scaledHeight, inputImage.getType());
 
        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();
        
        inputImage.flush();
        
        // extracts extension of output file
        String formatName = imagePath.substring(imagePath .lastIndexOf(".") + 1);
 
        // writes to output file
        Boolean worked = ImageIO.write(outputImage, formatName, new File(imagePath));
        if(!worked)
        {
        	System.out.println("Cannot resize picture.");
        }
    }
    
    public static void compress(String imagePath, float quality) throws IOException
    {
    	File input = new File(imagePath);
    	BufferedImage image = ImageIO.read(input);
    	input.delete();
    	
    	File compressedImageFile = new File(imagePath);  
    	OutputStream os = new FileOutputStream(compressedImageFile);
		
    	Iterator<ImageWriter>writers = ImageIO.getImageWritersByFormatName("png");
    	ImageWriter writer = (ImageWriter) writers.next();
		
    	ImageOutputStream ios = ImageIO.createImageOutputStream(os);
    	writer.setOutput(ios);
		
    	ImageWriteParam param = writer.getDefaultWriteParam();
    	// Check if canWriteCompressed is true
    	if(param.canWriteCompressed()) {
    		param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
	     	param.setCompressionQuality(quality);
    	}
    	// End of check
    	writer.write(null, new IIOImage(image, null, null), param);
    	image.flush();
    }
}
