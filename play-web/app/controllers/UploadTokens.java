package controllers;

import models.UploadToken;

public class UploadTokens {
	public static UploadToken getTokenFromId(String id)
	{
		return UploadToken.find.where().eq("id", id).findUnique();
	}
}