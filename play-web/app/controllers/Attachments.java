package controllers;

import models.Attachment;
import play.cache.Cache;

public class Attachments {
	
	public static Attachment getAttachmentById(String id)
	{
		Attachment attachment = (Attachment) Cache.get("attachment_id_" + id);
		if(attachment == null)
		{
			attachment = Attachment.find.where().eq("id", id).findUnique();
			if(attachment != null) CacheAttachment(attachment);
		}
		return attachment;
	}
	
	public static Attachment getAttachmentByHash(String hash)
	{
		Attachment attachment = (Attachment) Cache.get("attachment_h_" + hash);
		if(attachment == null)
		{
			attachment = Attachment.find.where().eq("Hash", hash).findUnique();
			if(attachment != null) CacheAttachment(attachment);
		}
		return attachment;
	}
	
	public static void CacheAttachment(Attachment attachment)
	{
		Cache.set("attachment_id_" + attachment.GetId(), attachment, 3600);
		Cache.set("attachment_h_" + attachment.Hash, attachment, 3600);
	}
}