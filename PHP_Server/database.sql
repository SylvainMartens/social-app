-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2016 at 09:12 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `social`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `creationIp` varchar(255) NOT NULL,
  `lastIp` varchar(255) NOT NULL,
  `isSuspended` int(255) NOT NULL DEFAULT '0',
  `suspendedReason` varchar(255) NOT NULL DEFAULT '',
  `isBanned` int(255) NOT NULL DEFAULT '0',
  `bannedReason` varchar(255) NOT NULL DEFAULT '',
  `picture` varchar(255) NOT NULL DEFAULT '',
  `premiumMode` int(255) NOT NULL DEFAULT '0',
  `isPremium` int(255) NOT NULL DEFAULT '0',
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `dob` datetime NOT NULL,
  `gender` int(255) NOT NULL,
  `city` varchar(255) NOT NULL DEFAULT '',
  `job` varchar(255) NOT NULL DEFAULT '',
  `activities` longtext NOT NULL,
  `description` longtext NOT NULL,
  `lookingFor` int(255) NOT NULL DEFAULT '0',
  `lookingForDescription` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `username`, `password`, `email`, `firstname`, `lastname`, `creationIp`, `lastIp`, `isSuspended`, `suspendedReason`, `isBanned`, `bannedReason`, `picture`, `premiumMode`, `isPremium`, `latitude`, `longitude`, `dob`, `gender`, `city`, `job`, `activities`, `description`, `lookingFor`, `lookingForDescription`) VALUES
(1, 'hesa2020', 'e7dfe3ef155eb1ecd8ca5d189cc6befbe7739735', 'thehesa@gmail.com', 'Sylvain', 'Martens', '192.168.1.102', '192.168.1.102', 0, '', 0, '', 'assets/avatars/1.png', 0, 0, 45.82296833333333, -73.76548833333332, '1994-06-21 00:00:00', 0, 'Montréal', 'Developer', 'Test,Programmation,Dev', 'Im just à developer\n', 1, 'Is this even working!?'),
(2, 'test', 'e7dfe3ef155eb1ecd8ca5d189cc6befbe7739735', 'hesawisa@hotmail.com', 'Test', 'Test', '127.0.0.1', '', 0, '', 0, '', '', 0, 0, 47.640068, -122.129858, '1990-08-09 00:00:00', 1, '', '', '', '', 0, ''),
(3, 'wisa2020', 'e7dfe3ef155eb1ecd8ca5d189cc6befbe7739735', 'sylvainmartens@gmail.com', 'Sylvain', 'Martens', '192.168.241.1', '192.168.241.1', 0, '', 0, '', 'assets/avatars/3.png', 0, 1, 45.82030596, -73.75682894, '1994-06-21 00:00:00', 0, 'Testtes', 'T2dsad', 'Dsadsaddsa,Sadasdmmm,dsadada,dsadasd,adsadsa', 'Dsadasddasd', 3, 'Dasdasdas4214'),
(4, 'max', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test', 'Max', 'Taxe', '127.0.0.1', '', 0, '', 0, '', '', 0, 1, 45.55000097, -73.66976176, '1990-01-11 00:00:00', 0, 'Lavallois', 'Trucker', 'Glissade', 'Je suis Jimmy', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE `attachments` (
  `id` bigint(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attachments`
--

INSERT INTO `attachments` (`id`, `hash`, `path`) VALUES
(1, '9aa4dc483431fd7e1b783523ae7cefb5', 'uploads/attachment/9aa4dc483431fd7e1b783523ae7cefb5.png'),
(6, '8c892234b8b6d0ce1504e165a57724c9', 'uploads/attachment/8c892234b8b6d0ce1504e165a57724c9.png'),
(7, 'b304cd3c799a10df1d4aac03171a1b98', 'uploads/attachment/b304cd3c799a10df1d4aac03171a1b98.png'),
(8, 'c2ca0a9f4fc306261d769bc43c8b3d26', 'uploads/attachment/c2ca0a9f4fc306261d769bc43c8b3d26.png'),
(9, '8482b3ae2c8ab2bc6ad49549cebd877e', 'uploads/attachment/8482b3ae2c8ab2bc6ad49549cebd877e.png'),
(10, 'ea4a6d5c2e6b7a0492b783791990b6eb', 'uploads/attachment/ea4a6d5c2e6b7a0492b783791990b6eb.png'),
(11, 'fd2fe3b23aeaa108d54c1802278472c8', 'uploads/attachment/fd2fe3b23aeaa108d54c1802278472c8.png'),
(12, '956996e8c01e7219b24b8d5e07f73521', 'uploads/attachment/956996e8c01e7219b24b8d5e07f73521.png'),
(13, '3e8b83859ffebecd11c4d0025820cae7', 'uploads/attachment/3e8b83859ffebecd11c4d0025820cae7.png'),
(14, '5d90d5d568d722716e6455686ed267a6', 'uploads/attachment/5d90d5d568d722716e6455686ed267a6.png'),
(15, 'dc0831209f0f6fd41f70df36dbac69cb', 'uploads/attachment/dc0831209f0f6fd41f70df36dbac69cb.png'),
(16, '5c1ad818f09de3764116b45d7258eca6', 'uploads/attachment/5c1ad818f09de3764116b45d7258eca6.png');

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE `blocks` (
  `id` bigint(255) NOT NULL,
  `userId` bigint(255) NOT NULL,
  `profileId` bigint(255) NOT NULL,
  `dateAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(255) NOT NULL,
  `userId` bigint(255) NOT NULL,
  `postId` bigint(255) NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 NOT NULL,
  `attachmentId` varchar(255) DEFAULT NULL,
  `dateAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `userId`, `postId`, `message`, `attachmentId`, `dateAt`) VALUES
(2, 1, 3, '8J+YiPCfmII=', '', '2016-10-01 18:49:32'),
(3, 3, 3, '8J+SmQ==', '', '2016-10-01 18:50:12'),
(4, 3, 3, 'Li4u', '', '2016-10-01 18:50:25'),
(5, 3, 3, '8J+kkA==', '', '2016-10-01 20:05:53');

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` bigint(255) NOT NULL,
  `user1` bigint(255) NOT NULL,
  `user2` bigint(255) NOT NULL,
  `startedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conversations`
--

INSERT INTO `conversations` (`id`, `user1`, `user2`, `startedAt`) VALUES
(1, 3, 1, '2016-09-13 14:58:02'),
(2, 3, 4, '2016-09-23 21:11:10');

-- --------------------------------------------------------

--
-- Table structure for table `follows`
--

CREATE TABLE `follows` (
  `id` bigint(255) NOT NULL,
  `profileId` bigint(255) NOT NULL,
  `userId` bigint(255) NOT NULL,
  `dateAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `follows`
--

INSERT INTO `follows` (`id`, `profileId`, `userId`, `dateAt`) VALUES
(6, 2, 1, '2016-08-20 18:42:14'),
(7, 4, 3, '2016-09-23 21:23:08'),
(8, 3, 4, '2016-09-23 21:57:27'),
(9, 1, 3, '2016-10-01 18:48:24'),
(10, 3, 1, '2016-10-01 18:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(255) NOT NULL,
  `conversationId` bigint(255) NOT NULL,
  `fromId` bigint(255) NOT NULL,
  `toId` bigint(255) NOT NULL,
  `sentAt` datetime NOT NULL,
  `seenAt` datetime NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 NOT NULL,
  `attachmentId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `conversationId`, `fromId`, `toId`, `sentAt`, `seenAt`, `message`, `attachmentId`) VALUES
(1, 1, 3, 1, '2016-09-13 15:14:37', '2016-09-13 15:41:03', 'Test', NULL),
(2, 1, 3, 1, '2016-09-13 15:15:11', '2016-09-13 15:41:03', 'Wtf', NULL),
(3, 1, 3, 1, '2016-09-13 15:15:25', '2016-09-13 15:41:03', 'Woot', NULL),
(4, 1, 3, 1, '2016-09-13 15:17:34', '2016-09-13 15:41:03', 'Test', '8'),
(5, 1, 3, 1, '2016-09-13 15:19:54', '2016-09-13 15:41:03', 'It work!', '7'),
(6, 1, 3, 1, '2016-09-13 15:20:48', '2016-09-13 15:41:03', 'Mmm', ''),
(7, 1, 3, 1, '2016-09-13 15:31:44', '2016-09-13 15:41:03', 'K', ''),
(8, 1, 3, 1, '2016-09-13 15:40:26', '2016-09-13 15:41:03', 'Cool stuff', '9'),
(9, 1, 1, 3, '2016-09-13 15:41:12', '2016-09-13 15:41:33', 'Nice', ''),
(10, 1, 3, 1, '2016-09-14 13:34:01', '2016-09-15 22:00:30', '<hr>', ''),
(11, 1, 3, 1, '2016-09-14 13:34:22', '2016-09-15 22:00:30', '<script>test</script>', ''),
(12, 1, 3, 1, '2016-09-14 13:37:20', '2016-09-15 22:00:30', 'Djdh', ''),
(13, 1, 3, 1, '2016-09-14 13:38:31', '2016-09-15 22:00:30', 'Fjdjd\nDjdjdjd', ''),
(14, 1, 3, 1, '2016-09-21 02:41:40', '2016-09-21 20:56:04', 'Hd', ''),
(15, 2, 3, 4, '2016-09-23 21:23:56', '2016-09-30 14:19:08', 'Yo', ''),
(16, 2, 3, 4, '2016-09-23 21:24:38', '2016-09-23 21:24:48', 'Test', ''),
(17, 2, 3, 4, '2016-09-23 21:24:59', '2016-09-23 21:24:59', 'Rip fonctionnalités briser je peux pas t''envoyer de photo...', ''),
(18, 2, 3, 4, '2016-09-23 21:59:05', '2016-09-23 21:59:06', 'Wtf', '12'),
(19, 2, 3, 4, '2016-09-23 23:17:38', '2016-09-30 14:19:08', '??', ''),
(20, 1, 3, 1, '2016-09-24 23:28:01', '2016-09-24 23:28:12', 'Hey', ''),
(21, 1, 3, 1, '2016-09-24 23:28:54', '2016-09-24 23:28:54', 'Cool', '14'),
(22, 1, 3, 1, '2016-10-02 01:06:30', '2016-10-02 01:06:46', 'Test', ''),
(23, 1, 3, 1, '2016-10-02 01:18:37', '2016-10-02 01:18:46', 'Yo', ''),
(24, 1, 1, 3, '2016-10-02 01:26:07', '2016-10-02 01:26:30', 'test', ''),
(25, 1, 3, 1, '2016-10-02 01:26:46', '2016-10-02 01:26:46', 'Cool you got it working...', ''),
(26, 1, 1, 3, '2016-10-02 01:28:42', '2016-10-02 02:21:40', 'dsadasdsad', '');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(255) NOT NULL,
  `userId` bigint(255) NOT NULL,
  `fromId` bigint(255) NOT NULL,
  `type` int(255) NOT NULL,
  `dateAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `userId`, `fromId`, `type`, `dateAt`) VALUES
(107, 2, 3, 0, '2016-09-22 18:57:24'),
(390, 4, 3, 0, '2016-09-23 21:12:37'),
(392, 4, 3, 0, '2016-09-23 21:18:18'),
(393, 4, 3, 0, '2016-09-23 21:18:24'),
(395, 4, 3, 0, '2016-09-23 21:19:44'),
(397, 4, 3, 0, '2016-09-23 21:19:57'),
(398, 4, 3, 0, '2016-09-23 21:22:53'),
(399, 4, 3, 2, '2016-09-23 21:23:08'),
(407, 4, 3, 0, '2016-09-23 21:54:53'),
(413, 4, 3, 0, '2016-09-23 22:24:52'),
(417, 4, 3, 0, '2016-09-23 22:51:33'),
(418, 4, 3, 0, '2016-09-23 22:51:37'),
(423, 4, 3, 5, '2016-09-23 22:53:11'),
(431, 4, 3, 5, '2016-09-23 23:02:28'),
(433, 4, 3, 0, '2016-09-23 23:17:50'),
(434, 4, 3, 0, '2016-09-23 23:19:30'),
(450, 4, 1, 0, '2016-09-24 16:51:14'),
(481, 4, 1, 0, '2016-09-24 17:48:34'),
(497, 4, 1, 0, '2016-09-24 19:06:27'),
(508, 4, 1, 0, '2016-09-24 19:43:57'),
(522, 4, 1, 0, '2016-09-24 20:23:13'),
(554, 2, 3, 0, '2016-09-24 23:33:11'),
(555, 4, 3, 0, '2016-09-24 23:33:15'),
(557, 4, 3, 0, '2016-09-24 23:47:09'),
(558, 4, 3, 0, '2016-09-24 23:56:18'),
(559, 4, 3, 0, '2016-09-25 00:00:53'),
(560, 4, 3, 0, '2016-09-25 00:04:25'),
(561, 4, 1, 0, '2016-09-25 00:11:47'),
(562, 4, 1, 3, '2016-09-25 00:11:50'),
(564, 4, 3, 0, '2016-09-25 00:14:59'),
(568, 4, 3, 0, '2016-09-25 00:19:45'),
(569, 4, 3, 5, '2016-09-25 00:20:14'),
(571, 4, 3, 5, '2016-09-25 00:21:19'),
(573, 4, 3, 5, '2016-09-25 00:24:34'),
(575, 4, 3, 0, '2016-09-25 00:25:34'),
(577, 4, 3, 5, '2016-09-25 00:26:26'),
(579, 4, 3, 5, '2016-09-25 00:27:08'),
(582, 4, 3, 0, '2016-09-25 00:27:48'),
(584, 4, 3, 5, '2016-09-25 00:31:40'),
(585, 4, 3, 0, '2016-09-25 00:31:46'),
(586, 4, 3, 7, '2016-09-25 00:32:17'),
(590, 4, 3, 5, '2016-09-25 01:00:58'),
(591, 4, 3, 0, '2016-09-25 01:02:18'),
(643, 4, 3, 0, '2016-09-27 22:24:04'),
(649, 4, 3, 0, '2016-09-27 22:57:36'),
(693, 4, 3, 0, '2016-09-28 23:42:36'),
(697, 4, 3, 0, '2016-09-28 23:44:41'),
(740, 4, 3, 0, '2016-09-29 00:24:56'),
(960, 4, 3, 0, '2016-09-30 14:14:45'),
(963, 4, 3, 5, '2016-09-30 16:05:37'),
(964, 4, 3, 0, '2016-09-30 16:06:20'),
(965, 4, 3, 0, '2016-09-30 16:08:13'),
(966, 4, 3, 0, '2016-09-30 16:08:17'),
(968, 4, 3, 0, '2016-09-30 16:10:53'),
(969, 4, 3, 0, '2016-09-30 16:11:19'),
(980, 4, 3, 5, '2016-10-01 04:04:00'),
(1072, 1, 3, 0, '2016-10-02 01:03:25'),
(1074, 1, 3, 0, '2016-10-02 01:04:38'),
(1075, 1, 3, 0, '2016-10-02 01:05:09'),
(1076, 1, 3, 0, '2016-10-02 01:06:10'),
(1078, 1, 3, 11, '2016-10-02 01:07:55'),
(1081, 1, 3, 11, '2016-10-02 01:11:09'),
(1083, 1, 3, 0, '2016-10-02 01:18:28'),
(1084, 1, 3, 11, '2016-10-02 01:21:59'),
(1087, 1, 3, 0, '2016-10-02 01:25:41'),
(1088, 1, 3, 11, '2016-10-02 01:25:47'),
(1092, 1, 3, 11, '2016-10-02 01:31:50'),
(1095, 1, 3, 11, '2016-10-02 01:34:12'),
(1096, 1, 3, 0, '2016-10-02 02:17:56'),
(1098, 1, 3, 0, '2016-10-02 02:18:12'),
(1099, 1, 3, 0, '2016-10-02 02:21:22'),
(1101, 1, 3, 0, '2016-10-02 02:21:49');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(255) NOT NULL,
  `userId` bigint(255) NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 NOT NULL,
  `attachmentId` varchar(255) DEFAULT NULL,
  `dateAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `userId`, `message`, `attachmentId`, `dateAt`) VALUES
(3, 3, '8J+klw==', '', '2016-10-01 18:49:08');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` bigint(255) NOT NULL,
  `userId` bigint(255) NOT NULL,
  `profileId` bigint(255) NOT NULL,
  `dateAt` datetime NOT NULL,
  `accepted` int(1) NOT NULL DEFAULT '0',
  `requestType` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(255) NOT NULL,
  `userId` bigint(255) NOT NULL,
  `tokenId` varchar(255) NOT NULL,
  `amount` int(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `dateAt` datetime NOT NULL,
  `expireAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `userId`, `tokenId`, `amount`, `currency`, `dateAt`, `expireAt`) VALUES
(1, 3, 'ch_9CY7kFpmsnqum9', 999, 'usd', '2016-09-15 19:12:17', '2016-10-16 19:12:17'),
(2, 4, 'dsadasd', 99, 'cad', '2016-09-01 00:00:00', '3016-09-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `uploadtokens`
--

CREATE TABLE `uploadtokens` (
  `id` bigint(255) NOT NULL,
  `accountId` bigint(255) NOT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `uploadtokens`
--

INSERT INTO `uploadtokens` (`id`, `accountId`, `ip`) VALUES
(7, 3, '192.168.1.103');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follows`
--
ALTER TABLE `follows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploadtokens`
--
ALTER TABLE `uploadtokens`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `follows`
--
ALTER TABLE `follows`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1102;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `uploadtokens`
--
ALTER TABLE `uploadtokens`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
