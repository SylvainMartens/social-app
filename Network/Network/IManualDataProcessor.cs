using System;
using System.Threading;

namespace Network
{
    public interface IManualDataProcessor
    {
        void Enqueue(Action processingMethod);

        ManualResetEvent EnqueueWaitHandle(Action processingMethod);
    }
}