using System;

namespace Network
{
    public class PerformanceMonitor
    {
        private static long _totalExecutionCounts;

        private static double _totalExecutionTime;

        private static long _totalConnectionReadCounts;

        private static long _totalConnectionHeaderReadCounts;

        private static double _totalConnectionHeaderReadTime;

        private static long _totalConnectionBodyReadCounts;

        private static double _totalConnectionBodyReadTime;

        private static object recLock = new object();

        public static long totalExecutionCounts
        {
            get
            {
                return PerformanceMonitor._totalExecutionCounts;
            }
        }

        public static double totalExecutionTime
        {
            get
            {
                return PerformanceMonitor._totalExecutionTime;
            }
        }

        [Obsolete("Please use totalConnectionHeaderCounts and totalConnectionBodyCounts")]
        public static long totalConnectionReadCounts
        {
            get
            {
                return PerformanceMonitor._totalConnectionHeaderReadCounts;
            }
        }

        public static double totalConnectionReadTime
        {
            get
            {
                return PerformanceMonitor.totalConnectionHeaderReadTime + PerformanceMonitor.totalConnectionBodyReadTime;
            }
        }

        public static long totalConnectionHeaderReadCounts
        {
            get
            {
                return PerformanceMonitor._totalConnectionHeaderReadCounts;
            }
        }

        public static double totalConnectionHeaderReadTime
        {
            get
            {
                return PerformanceMonitor._totalConnectionHeaderReadTime;
            }
        }

        public static long totalConnectionBodyReadCounts
        {
            get
            {
                return PerformanceMonitor._totalConnectionBodyReadCounts;
            }
        }

        public static double totalConnectionBodyReadTime
        {
            get
            {
                return PerformanceMonitor._totalConnectionBodyReadTime;
            }
        }

        public static double averageExecutionTime
        {
            get
            {
                if (PerformanceMonitor.totalExecutionCounts < 1L)
                {
                    return 0.0;
                }
                return PerformanceMonitor._totalExecutionTime / (double)PerformanceMonitor._totalExecutionCounts;
            }
        }

        public static double averageConnectionReadTime
        {
            get
            {
                if (PerformanceMonitor.totalConnectionHeaderReadCounts < 1L)
                {
                    return 0.0;
                }
                return PerformanceMonitor.totalConnectionReadTime / (double)PerformanceMonitor._totalConnectionHeaderReadCounts;
            }
        }

        public static double averageConnectionHeaderReadTime
        {
            get
            {
                if (PerformanceMonitor.totalConnectionHeaderReadCounts < 1L)
                {
                    return 0.0;
                }
                return PerformanceMonitor.totalConnectionHeaderReadTime / (double)PerformanceMonitor._totalConnectionHeaderReadCounts;
            }
        }

        public static double averageConnectionBodyReadTime
        {
            get
            {
                if (PerformanceMonitor.totalConnectionBodyReadCounts < 1L)
                {
                    return 0.0;
                }
                return PerformanceMonitor.totalConnectionBodyReadTime / (double)PerformanceMonitor._totalConnectionBodyReadCounts;
            }
        }

        public static void ResetCounters()
        {
            PerformanceMonitor._totalExecutionCounts = 0L;
            PerformanceMonitor._totalExecutionTime = 0.0;
            PerformanceMonitor._totalConnectionReadCounts = 0L;
            PerformanceMonitor._totalConnectionHeaderReadTime = 0.0;
            PerformanceMonitor._totalConnectionBodyReadTime = 0.0;
        }

        internal static void RecordExecutionTime(long time)
        {
            lock (PerformanceMonitor.recLock)
            {
                PerformanceMonitor._totalExecutionTime += (double)time;
                PerformanceMonitor._totalExecutionCounts += 1L;
            }
        }

        internal static void RecordConnectionHeaderReadTime(long time)
        {
            lock (PerformanceMonitor.recLock)
            {
                PerformanceMonitor._totalConnectionHeaderReadTime += (double)time;
                PerformanceMonitor._totalConnectionHeaderReadCounts += 1L;
            }
        }

        internal static void RecordConnectionBodyReadTime(long time)
        {
            lock (PerformanceMonitor.recLock)
            {
                PerformanceMonitor._totalConnectionBodyReadTime += (double)time;
                PerformanceMonitor._totalConnectionBodyReadCounts += 1L;
            }
        }

        internal static void LogPerformanceData()
        {
            Interface.Log(string.Concat(new string[]
            {
                "*************************************************\nPerformance stats:\n\tTotal Connection Read Time:\t",
                PerformanceMonitor.totalConnectionReadTime.ToString(),
                "ms\n\t\t\tHeader:\t\t",
                PerformanceMonitor.totalConnectionHeaderReadTime.ToString(),
                "ms\n\t\t\tBody:\t\t",
                PerformanceMonitor.totalConnectionBodyReadTime.ToString(),
                "ms\n\t\tAverage:\t\t",
                Math.Round(PerformanceMonitor.averageConnectionReadTime, 2).ToString(),
                "ms\n\t\t\tHeader:\t\t",
                Math.Round(PerformanceMonitor.averageConnectionHeaderReadTime, 2).ToString(),
                "ms\n\t\t\tBody:\t\t",
                Math.Round(PerformanceMonitor.averageConnectionBodyReadTime, 2).ToString(),
                "ms\n\tTotal Execution Time:\t\t",
                PerformanceMonitor._totalExecutionTime.ToString(),
                "ms\n\t\tAverage:\t\t",
                Math.Round(PerformanceMonitor.averageExecutionTime, 2).ToString(),
                "ms\n*************************************************"
            }));
        }
    }
}