using System;

namespace Network
{
    public class NotAllowedException : Exception
    {
        public NotAllowedException(string message) : base(message)
        {
        }
    }
}