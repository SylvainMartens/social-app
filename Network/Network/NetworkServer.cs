using Network.ConfigTools;
using Network.IDAllocation;
using Network.Storage;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Network
{
    public class NetworkServer
    {
        public delegate void ServerCloseEvent();

        public const int CONNECTION_LIMIT = 65534;

        internal static TcpListener listener;

        public static ConfigReader settings;

        private static bool running;

        internal static List<ConnectionService> connections = new List<ConnectionService>();

        public static bool logData;

        internal static IManualDataProcessor manualDataProcessor;

        private static object closeLock = new object();

        public static event NetworkServer.ServerCloseEvent onServerClose;

        public static Database database
        {
            get;
            internal set;
        }

        public static Mode mode
        {
            get;
            private set;
        }

        public static int port
        {
            get;
            private set;
        }

        public static ushort maxConnections
        {
            get;
            private set;
        }

        public static void Bootstrap(Mode mode, Action<string> onLog, Action<string> onWarning, Action<string> onError, Action<string> onFatal, IManualDataProcessor manualDataProcessor, Type[] forceLoadPlugins)
        {
            if (NetworkServer.running)
            {
                throw new NotAllowedException("You can't bootstrap Network when it's already been bootstrapped!");
            }
            NetworkServer.running = true;
            NetworkServer.mode = mode;
            Interface.Setup(onLog, onWarning, onError, onFatal);
            Interface.LogTrace("#### Server boot ####");
            try
            {
                NetworkServer.settings = new ConfigReader("settings.cnf");
            }
            catch (Exception ex)
            {
                Interface.LogFatal("settings.cnf did not load corectly, could not continue.");
                Interface.LogTrace(ex.ToString());
                return;
            }
            if (NetworkServer.settings["Port"] == null)
            {
                Interface.LogFatal("settings.cnf is missing Port key, could not continue.");
            }
            else
            {
                if (NetworkServer.settings["MaxConnections"] == null)
                {
                    Interface.LogFatal("settings.cnf is missing MaxConnections key, could not continue.");
                    return;
                }
                if (NetworkServer.settings["LogData"] == null)
                {
                    Interface.LogFatal("settings.cnf is missing LogData key, could not continue.");
                    return;
                }
                try
                {
                    NetworkServer.port = int.Parse(NetworkServer.settings["Port"]);
                }
                catch (FormatException)
                {
                    Interface.LogError("The port specified cannot be converted to a number!");
                    return;
                }
                try
                {
                    NetworkServer.maxConnections = ushort.Parse(NetworkServer.settings["MaxConnections"]);
                }
                catch (FormatException)
                {
                    Interface.LogError("The max connections specified cannot be converted to a number!");
                    return;
                }
                NetworkServer.logData = NetworkServer.settings.IsTrue("LogData");
                NetworkServer.FinishBootstrapping(manualDataProcessor, forceLoadPlugins);
                return;
            }
        }

        public static void Bootstrap(Mode mode, int port, ushort maxConnections, bool logData, Action<string> onLog, Action<string> onWarning, Action<string> onError, Action<string> onFatal, IManualDataProcessor manualDataProcessor, Type[] forceLoadPlugins)
        {
            if (NetworkServer.running)
            {
                throw new NotAllowedException("You can't bootstrap Network when it's already been bootstrapped!");
            }
            NetworkServer.running = true;
            NetworkServer.mode = mode;
            NetworkServer.port = port;
            NetworkServer.maxConnections = maxConnections;
            NetworkServer.logData = logData;
            Interface.Setup(onLog, onWarning, onError, onFatal);
            NetworkServer.FinishBootstrapping(manualDataProcessor, forceLoadPlugins);
        }

        private static void FinishBootstrapping(IManualDataProcessor manualDataProcessor, Type[] forceLoadPlugins)
        {
            NetworkServer.manualDataProcessor = manualDataProcessor;
            Interface.LogTrace("#### Server boot ####");
            NetworkServer.listener = new TcpListener(IPAddress.Any, NetworkServer.port);
            try
            {
                NetworkServer.listener.Start();
            }
            catch (Exception ex)
            {
                Interface.LogFatal(ex.ToString());
                return;
            }
            Interface.Log("Server mounted, listening on port " + NetworkServer.port.ToString());
            Interface.SetupServerCommands();
            if (forceLoadPlugins != null)
            {
                PluginManager.LoadPlugins(forceLoadPlugins);
            }
            if (NetworkServer.mode == Mode.Standalone)
            {
                PluginManager.LoadPluginsFromFolder();
            }
            if (NetworkServer.mode == Mode.Standalone || (forceLoadPlugins != null && forceLoadPlugins.Length > 0))
            {
                PluginManager.SetupUpdate();
            }
            PluginManager.InvokeOnPluginsLoaded();
            NetworkServer.listener.BeginAcceptSocket(new AsyncCallback(NetworkServer.AcceptClient), NetworkServer.listener);
        }

        private static void AcceptClient(IAsyncResult result)
        {
            Socket socket = NetworkServer.listener.EndAcceptSocket(result);
            try
            {
                ushort availableID = IDAllocator.GetAvailableID();
                ConnectionService connectionService = new ConnectionService(socket, availableID);
                lock (NetworkServer.connections)
                {
                    NetworkServer.connections.Add(connectionService);
                }
                connectionService.CallOnPostPlayerConnect();
            }
            catch (NoAvailableIDException)
            {
                if (NetworkServer.connections.Count >= 65534)
                {
                    Interface.LogError("Connection refused as your license limits you to " + 65534.ToString() + " concurrent users!");
                }
                else
                {
                    Interface.LogError("Connection refused due to your limit set!");
                }
                ConnectionService.RejectConnection(socket);
            }
            catch (Exception ex)
            {
                Interface.LogTrace("Exception trace connection accepting thread in Main");
                Interface.LogError(ex.ToString());
            }
            NetworkServer.listener.BeginAcceptSocket(new AsyncCallback(NetworkServer.AcceptClient), NetworkServer.listener);
        }

        public static ConnectionService GetConnectionServiceByID(ushort id)
        {
            lock (NetworkServer.connections)
            {
                foreach (ConnectionService current in NetworkServer.connections)
                {
                    if (current.id == id)
                    {
                        return current;
                    }
                }
            }
            return null;
        }

        public static ConnectionService[] GetAllConnections()
        {
            ConnectionService[] result;
            lock (NetworkServer.connections)
            {
                result = NetworkServer.connections.ToArray();
            }
            return result;
        }

        public static ushort GetConnectionLimit()
        {
            if (65534 < NetworkServer.maxConnections)
            {
                return 65534;
            }
            return NetworkServer.maxConnections;
        }

        public static int GetNumberOfConnections()
        {
            int count;
            lock (NetworkServer.connections)
            {
                count = NetworkServer.connections.Count;
            }
            return count;
        }

        internal static void RemoveConnection(ConnectionService cs)
        {
            lock (NetworkServer.connections)
            {
                NetworkServer.connections.Remove(cs);
            }
        }

        public static void Close(bool quit)
        {
            lock (NetworkServer.closeLock)
            {
                Interface.Log("Shutting down server...");
                if (NetworkServer.onServerClose != null)
                {
                    NetworkServer.onServerClose();
                }
                lock (NetworkServer.connections)
                {
                    while (NetworkServer.connections.Count > 0)
                    {
                        if (NetworkServer.connections[0] != null)
                        {
                            NetworkServer.connections[0].Close(true, null);
                        }
                        else
                        {
                            NetworkServer.connections.RemoveAt(0);
                        }
                    }
                }
                Interface.LogTrace("Closing database");
                if (NetworkServer.database != null)
                {
                    NetworkServer.database.Dispose();
                }
                Interface.LogTrace("#### Server stop ####");
                if (quit)
                {
                    Environment.Exit(0);
                }
            }
        }
    }
}