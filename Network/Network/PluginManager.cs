using Network.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Timers;

namespace Network
{
    public class PluginManager
    {
        public delegate void PluginsLoadedEvent();

        public static event PluginManager.PluginsLoadedEvent onPluginsLoaded;

        public static Dictionary<string, Plugin> plugins
        {
            get;
            private set;
        }

        internal static void CallUpdate(object source, ElapsedEventArgs e)
        {
            foreach (KeyValuePair<string, Plugin> current in PluginManager.plugins)
            {
                current.Value.Update();
            }
        }

        internal static void LoadPluginsFromFolder()
        {
            PluginManager.plugins = new Dictionary<string, Plugin>();
            if (!Directory.Exists(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "Plugins"))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "Plugins");
                Directory.CreateDirectory(string.Concat(new object[]
                {
                    Directory.GetCurrentDirectory(),
                    Path.DirectorySeparatorChar,
                    "Plugins",
                    Path.DirectorySeparatorChar,
                    "Lib"
                }));
            }
            string[] files = Directory.GetFiles(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "Plugins", "*.dll");
            List<Type> list = new List<Type>();
            string[] array = files;
            for (int i = 0; i < array.Length; i++)
            {
                string text = array[i];
                try
                {
                    Assembly assembly = Assembly.LoadFrom(text);
                    if (assembly != null)
                    {
                        Type[] types = assembly.GetTypes();
                        for (int j = 0; j < types.Length; j++)
                        {
                            Type type = types[j];
                            if (!type.IsInterface && !type.IsAbstract && type.GetInterface(typeof(IPlugin).FullName) != null)
                            {
                                Interface.LogTrace("Found plugin: " + text + ", type of " + type.ToString());
                                list.Add(type);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Interface.LogError("Plugin " + text + " failed to load: \n" + ex.ToString());
                    if (ex is ReflectionTypeLoadException)
                    {
                        Exception[] loaderExceptions = ((ReflectionTypeLoadException)ex).LoaderExceptions;
                        Interface.LogTrace("Exception trace PluginManager.LoadPlugins");
                        Exception[] array2 = loaderExceptions;
                        for (int k = 0; k < array2.Length; k++)
                        {
                            Exception ex2 = array2[k];
                            Interface.LogError(ex2.ToString());
                        }
                    }
                }
            }
            Interface.LogTrace("Found a total of " + list.Count.ToString() + " plugins.");
            PluginManager.LoadPlugins(list.ToArray());
        }

        internal static void InvokeOnPluginsLoaded()
        {
            try
            {
                if (PluginManager.onPluginsLoaded != null)
                {
                    PluginManager.onPluginsLoaded();
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.ToString());
            }
        }

        internal static void SetupUpdate()
        {
            Timer timer = new Timer(10.0);
            timer.Elapsed += new ElapsedEventHandler(PluginManager.CallUpdate);
            timer.Start();
        }

        internal static void LoadPlugins(Type[] forceLoadPlugins)
        {
            bool flag = false;
            for (int i = 0; i < forceLoadPlugins.Length; i++)
            {
                Type type = forceLoadPlugins[i];
                try
                {
                    if (type.IsSubclassOf(typeof(Database)))
                    {
                        if (flag)
                        {
                            Interface.LogError("You have multiple database plugins installed, don't have!");
                        }
                        else
                        {
                            Database database = Activator.CreateInstance(type) as Database;
                            Interface.Log("Loaded database connector: " + database.name + " Version: " + database.version);
                            NetworkServer.database = database;
                            Interface.RegisterCommands(database.commands);
                            flag = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Interface.LogError("Plugin " + type.FullName + " failed to instantiate: \n" + ex.ToString());
                    if (ex is ReflectionTypeLoadException)
                    {
                        Exception[] loaderExceptions = ((ReflectionTypeLoadException)ex).LoaderExceptions;
                        Interface.LogTrace("Exception trace PluginManager.LoadPlugins");
                        Exception[] array = loaderExceptions;
                        for (int j = 0; j < array.Length; j++)
                        {
                            Exception ex2 = array[j];
                            Interface.LogError(ex2.ToString());
                        }
                    }
                }
            }
            for (int k = 0; k < forceLoadPlugins.Length; k++)
            {
                Type type2 = forceLoadPlugins[k];
                try
                {
                    if (type2.IsSubclassOf(typeof(Plugin)))
                    {
                        Plugin plugin = Activator.CreateInstance(type2) as Plugin;
                        Interface.Log("Loaded plugin: " + plugin.name + " Version: " + plugin.version);
                        PluginManager.plugins.Add(plugin.name, plugin);
                        Interface.RegisterCommands(plugin.commands);
                    }
                }
                catch (Exception ex3)
                {
                    Interface.LogError("Plugin " + type2.FullName + " failed to instantiate: \n" + ex3.ToString());
                    if (ex3 is ReflectionTypeLoadException)
                    {
                        Exception[] loaderExceptions2 = ((ReflectionTypeLoadException)ex3).LoaderExceptions;
                        Interface.LogTrace("Exception trace PluginManager.LoadPlugins");
                        Exception[] array2 = loaderExceptions2;
                        for (int l = 0; l < array2.Length; l++)
                        {
                            Exception ex4 = array2[l];
                            Interface.LogError(ex4.ToString());
                        }
                    }
                }
            }
        }
    }
}