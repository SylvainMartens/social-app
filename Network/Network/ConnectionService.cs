using Network.Transmission;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Network
{
    public class ConnectionService
    {
        public delegate void PlayerConnectEvent(ConnectionService con);

        public delegate void DataEvent(ConnectionService con, ref NetworkMessage data);

        public delegate void ReadOnlyDataEvent(ConnectionService con, NetworkMessage data);

        public delegate void PlayerDisconnectEvent(ConnectionService con);

        public delegate bool DistributorEvent(ConnectionService con, NetworkMessage data);

        private const byte INTERNAL_MESSAGE = 255;

        private const ushort DISCONNECT_MESSAGE = 0;

        private const ushort CONNECTION_CLOSE_MESSAGE = 1;

        private ushort _id;

        private Socket soc;

        private NetworkStream stream;

        private volatile bool closed;

        private Dictionary<string, object> playerData;

        private object closeLock = new object();

        public static event ConnectionService.PlayerConnectEvent onPlayerConnect;

        public static event ConnectionService.PlayerConnectEvent onPostPlayerConnect;

        public static event ConnectionService.DataEvent onData;

        [Obsolete("Please use the onData and call NetworkMessage.DecodeData to get data. This will still work for backward compatability.")]
        public static event ConnectionService.DataEvent onDataDecoded;

        public static event ConnectionService.PlayerDisconnectEvent onPlayerDisconnect;

        public static event ConnectionService.ReadOnlyDataEvent onServerMessage;

        public static event ConnectionService.DistributorEvent onDistribute;

        public ushort id
        {
            get
            {
                return this._id;
            }
        }

        public EndPoint remoteEndPoint
        {
            get;
            private set;
        }

        internal ConnectionService(Socket socket, ushort id)
        {
            this._id = id;
            this.playerData = new Dictionary<string, object>();
            this.soc = socket;
            socket.NoDelay = true;
            socket.LingerState = new LingerOption(true, 1);
            Interface.Log("Connected: " + this.soc.RemoteEndPoint.ToString());
            this.stream = new NetworkStream(this.soc);
            this.Handshake();
            this.remoteEndPoint = this.soc.RemoteEndPoint;
            if (ConnectionService.onPlayerConnect != null)
            {
                if (NetworkServer.manualDataProcessor != null)
                {
                    NetworkServer.manualDataProcessor.Enqueue(delegate
                    {
                        ConnectionService.onPlayerConnect(this);
                    });
                }
                else
                {
                    ConnectionService.onPlayerConnect(this);
                }
            }
            DataBufferItem dataBufferItem = new DataBufferItem();
            dataBufferItem.header = new byte[4];
            lock (this.soc)
            {
                this.soc.BeginReceive(dataBufferItem.header, 0, 4, SocketFlags.None, new AsyncCallback(this.ReadHeaderCallback), dataBufferItem);
            }
        }

        private void Handshake()
        {
            byte[] array = HandshakeProtocol.EncodeHandshake(new Handshake(this._id, ConnectionCode.Accept));
            lock (this.soc)
            {
                this.stream.Write(array, 0, array.Length);
            }
        }

        internal static void RejectConnection(Socket soc)
        {
            using (NetworkStream networkStream = new NetworkStream(soc))
            {
                byte[] array = HandshakeProtocol.EncodeHandshake(new Handshake(0, ConnectionCode.Reject));
                lock (soc)
                {
                    networkStream.Write(array, 0, array.Length);
                }
            }
            soc.Close();
        }

        internal void CallOnPostPlayerConnect()
        {
            if (ConnectionService.onPostPlayerConnect != null)
            {
                ConnectionService.onPostPlayerConnect(this);
            }
        }

        private void ReadHeaderCallback(IAsyncResult result)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            int num;
            try
            {
                lock (this.soc)
                {
                    num = this.soc.EndReceive(result);
                }
            }
            catch (Exception e)
            {
                this.Close(false, e);
                return;
            }
            if (num != 0)
            {
                DataBufferItem dataBufferItem = (DataBufferItem)result.AsyncState;
                int num2 = BitConverter.ToInt32(dataBufferItem.header, 0);
                dataBufferItem.body = new byte[num2];
                try
                {
                    lock (this.soc)
                    {
                        this.soc.BeginReceive(dataBufferItem.body, 0, num2, SocketFlags.None, new AsyncCallback(this.ReadBodyCallback), dataBufferItem);
                    }
                }
                catch (Exception e2)
                {
                    this.Close(false, e2);
                    return;
                }
                finally
                {
                    stopwatch.Stop();
                }
                PerformanceMonitor.RecordConnectionHeaderReadTime(stopwatch.ElapsedMilliseconds);
                return;
            }
            this.Close(false, null);
        }

        private void ReadBodyCallback(IAsyncResult result)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            int num;
            try
            {
                lock (this.soc)
                {
                    num = this.soc.EndReceive(result);
                }
            }
            catch (Exception e)
            {
                this.Close(false, e);
                return;
            }
            if (num != 0)
            {
                DataBufferItem item = (DataBufferItem)result.AsyncState;
                if (NetworkServer.manualDataProcessor != null)
                {
                    NetworkServer.manualDataProcessor.Enqueue(delegate
                    {
                        this.ProcessDataItem(item);
                    });
                }
                else
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(this.ProcessDataItem), item);
                }
                DataBufferItem dataBufferItem = new DataBufferItem();
                dataBufferItem.header = new byte[4];
                try
                {
                    lock (this.soc)
                    {
                        this.soc.BeginReceive(dataBufferItem.header, 0, 4, SocketFlags.None, new AsyncCallback(this.ReadHeaderCallback), dataBufferItem);
                    }
                }
                catch (Exception e2)
                {
                    this.Close(false, e2);
                    return;
                }
                finally
                {
                    stopwatch.Stop();
                }
                PerformanceMonitor.RecordConnectionBodyReadTime(stopwatch.ElapsedMilliseconds);
                return;
            }
            this.Close(false, null);
        }

        private void ProcessDataItem(object context)
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                DataBufferItem dataBufferItem = (DataBufferItem)context;
                NetworkMessage networkMessage;
                try
                {
                    networkMessage = TransmissionProtocol.DecodeMessageHeader(dataBufferItem);
                }
                catch (VersionException)
                {
                    Interface.LogError("Version exception raised - No definition on how to decode message.");
                    stopwatch.Stop();
                    return;
                }
                catch (Exception ex)
                {
                    Interface.LogTrace("Exception trace ConnectionService.ProcessDataItem decoding message header");
                    Interface.LogError(ex.ToString());
                    Interface.LogWarning("Error occured, data lost.");
                    return;
                }
                if (NetworkServer.logData)
                {
                    if (networkMessage.tag == 255)
                    {
                        Interface.Log(string.Concat(new string[]
                        {
                            "Data: Sender: ",
                            networkMessage.senderID.ToString(),
                            " DistributionType: ",
                            networkMessage.distributionType.ToString(),
                            (networkMessage.distributionType == DistributionType.ID) ? (" Destination: " + networkMessage.destinationID.ToString()) : "",
                            " Tag-Subject: internal (255)-",
                            (networkMessage.subject == 0) ? ("Disconnect (" + 0 + ")") : networkMessage.subject.ToString()
                        }));
                    }
                    else
                    {
                        Interface.Log(string.Concat(new string[]
                        {
                            "Data: Sender: ",
                            networkMessage.senderID.ToString(),
                            " DistributionType: ",
                            networkMessage.distributionType.ToString(),
                            (networkMessage.distributionType == DistributionType.ID) ? (" Destination: " + networkMessage.destinationID.ToString()) : "",
                            " Tag-Subject: ",
                            networkMessage.tag.ToString(),
                            "-",
                            networkMessage.subject.ToString()
                        }));
                    }
                }
                int num = 0;
                if (networkMessage.tag == 255)
                {
                    if (networkMessage.subject == 0)
                    {
                        this.Close(false, null);
                    }
                }
                else
                {
                    if (ConnectionService.onData != null)
                    {
                        ConnectionService.onData(this, ref networkMessage);
                        num = 1;
                    }
                    if (ConnectionService.onDataDecoded != null)
                    {
                        if (networkMessage.data == null)
                        {
                            networkMessage.data = TransmissionProtocol.DecodeMessageData(dataBufferItem);
                        }
                        ConnectionService.onDataDecoded(this, ref networkMessage);
                        num = 2;
                    }
                }
                byte[] array;
                if (num == 2 || (num == 1 && networkMessage.data != null))
                {
                    array = TransmissionProtocol.EncodeMessage(networkMessage);
                }
                else
                {
                    array = new byte[dataBufferItem.body.Length + 4];
                    Buffer.BlockCopy(dataBufferItem.header, 0, array, 0, 4);
                    Buffer.BlockCopy(dataBufferItem.body, 0, array, 4, dataBufferItem.body.Length);
                    if (num == 1)
                    {
                        array = TransmissionProtocol.EncodeHeader(networkMessage, array);
                    }
                }
                bool flag = true;
                if (ConnectionService.onDistribute != null)
                {
                    flag = !ConnectionService.onDistribute(this, networkMessage);
                }
                if (flag)
                {
                    if (networkMessage.data is NetworkReader)
                    {
                        networkMessage.data = NetworkUtilities.ConvertReaderToWriter((NetworkReader)networkMessage.data);
                    }
                    switch (networkMessage.distributionType)
                    {
                        case DistributionType.All:
                        lock (NetworkServer.connections)
                        {
                            for (int i = 0; i < NetworkServer.connections.Count; i++)
                            {
                                if (NetworkServer.connections[i] != null)
                                {
                                    NetworkServer.connections[i].SendBytes(array);
                                }
                            }
                        }
                        if (ConnectionService.onServerMessage != null)
                        {
                            ConnectionService.onServerMessage(this, TransmissionProtocol.DecodeMessage(dataBufferItem));
                            goto IL_452;
                        }
                        goto IL_452;
                        case DistributionType.Server:
                        break;

                        case DistributionType.Others:
                        lock (NetworkServer.connections)
                        {
                            for (int j = 0; j < NetworkServer.connections.Count; j++)
                            {
                                if (NetworkServer.connections[j] != null && NetworkServer.connections[j]._id != networkMessage.senderID)
                                {
                                    NetworkServer.connections[j].SendBytes(array);
                                }
                            }
                            goto IL_452;
                        }
                        break;

                        case DistributionType.ID:
                        {
                            ConnectionService connectionServiceByID = NetworkServer.GetConnectionServiceByID(networkMessage.destinationID);
                            if (connectionServiceByID != null)
                            {
                                connectionServiceByID.SendBytes(array);
                                goto IL_452;
                            }
                            Interface.LogError("Attempt to send data to ID: " + networkMessage.destinationID + " failed because the ID does not exist.");
                            goto IL_452;
                        }
                        default:
                        Interface.LogError("You've sent a message with a custom distribution type. You need to specify what to do with this data in a plugin!");
                        goto IL_452;
                    }
                    if (ConnectionService.onServerMessage != null)
                    {
                        networkMessage.DecodeData();
                        ConnectionService.onServerMessage(this, networkMessage);
                    }
                IL_452:
                    stopwatch.Stop();
                    PerformanceMonitor.RecordExecutionTime(stopwatch.ElapsedMilliseconds);
                }
            }
            catch (Exception ex2)
            {
                Interface.LogTrace("Exception trace ConnectionService.ProcessDataItem");
                Interface.LogError(ex2.ToString());
            }
        }

        internal bool SendBytes(byte[] bytes)
        {
            try
            {
                lock (this.soc)
                {
                    if (this.soc.Connected && !this.closed)
                    {
                        this.stream.Write(bytes, 0, bytes.Length);
                        return true;
                    }
                }
            }
            catch (IOException ex)
            {
                if (ex.InnerException is SocketException)
                {
                    SocketException e = (SocketException)ex.InnerException;
                    this.Close(false, e);
                }
                else
                {
                    this.Close(false, ex);
                }
            }
            catch (Exception e2)
            {
                this.Close(false, e2);
            }
            return false;
        }

        public bool SendNetworkMessage(NetworkMessage message)
        {
            return this.SendBytes(TransmissionProtocol.EncodeMessage(message));
        }

        public bool SendReply(byte tag, ushort subject, object data)
        {
            return this.SendNetworkMessage(new NetworkMessage(0, DistributionType.Reply, this.id, tag, subject, data));
        }

        public void Close()
        {
            this.Close(true, null);
        }

        internal void Close(bool inform, Exception e = null)
        {
            lock (this.closeLock)
            {
                if (!this.closed)
                {
                    this.closed = true;
                    if (ConnectionService.onPlayerDisconnect != null)
                    {
                        if (NetworkServer.manualDataProcessor != null)
                        {
                            ManualResetEvent manualResetEvent = NetworkServer.manualDataProcessor.EnqueueWaitHandle(delegate
                            {
                                ConnectionService.onPlayerDisconnect(this);
                            });
                            manualResetEvent.WaitOne();
                        }
                        else
                        {
                            ConnectionService.onPlayerDisconnect(this);
                        }
                    }
                    if (inform)
                    {
                        this.SendNetworkMessage(new NetworkMessage(0, DistributionType.Reply, 0, 255, 1, 0));
                    }
                    Interface.Log("Disconnected: " + this.remoteEndPoint.ToString());
                    if (e != null)
                    {
                        Interface.LogTrace("Disconnect was due to handled exception: " + e.ToString());
                    }
                    lock (this.soc)
                    {
                        this.stream.Close();
                    }
                    NetworkServer.RemoveConnection(this);
                }
            }
        }

        public void SetData(string pluginName, string fieldName, object data)
        {
            this.playerData[pluginName + ":" + fieldName] = data;
        }

        public object GetData(string pluginName, string fieldName)
        {
            return this.playerData[pluginName + ":" + fieldName];
        }

        public bool HasData(string pluginName, string fieldName)
        {
            return this.playerData.ContainsKey(pluginName + ":" + fieldName);
        }
    }
}