namespace Network.IDAllocation
{
    internal class IDAllocator
    {
        internal static ushort GetAvailableID()
        {
            for (ushort num = 1; num <= NetworkServer.GetConnectionLimit(); num += 1)
            {
                bool flag = true;
                foreach (ConnectionService current in NetworkServer.connections)
                {
                    if (num == current.id)
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    return num;
                }
            }
            throw new NoAvailableIDException();
        }
    }
}