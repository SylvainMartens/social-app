namespace Network.Storage
{
    public struct QueryParameter
    {
        public string name;

        public object obj;

        public QueryParameter(string name, object obj)
        {
            this.name = name;
            this.obj = obj;
        }
    }
}