using Android.Media;
using SocialAPP.Shared;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SocialAPP.Droid
{
    public class AudioCapture
    {
        byte[] audioBuffer = null;
        AudioRecord audioRecord = null;
        bool endRecording = false;
        bool isRecording = false;

        public Action<byte[]> OnAudioReady;
        public Action<bool> RecordingStateChanged;

        int bufferSize = 8000;

        int mBufferSize = 256;
        int mSampleRate = 44100;
        Encoding mEncoding = Encoding.Pcm16bit;
        bool micMuted = false;

        public bool IsRecording
        {
            get { return (isRecording); }
        }

        public void ToggleMuteMic()
        {
            micMuted = !micMuted;
        }

        public AudioRecord FindAudioRecord()
        {
            int[] sampleRates = new int[] { 16000, 44100 };//16000
            Encoding[] encodings = new Encoding[] { Encoding.Pcm16bit, Encoding.Pcm8bit };
            //ChannelIn[] channelConfigs = new ChannelIn[] { ChannelIn.Mono, ChannelIn.Stereo };

            //Not all of the formats are supported on each device
            foreach (int sampleRate in sampleRates)
            {
                foreach (Encoding encoding in encodings)
                {
                    //foreach (ChannelIn channelConfig in channelConfigs)
                    {
                        try
                        {
                            Console.WriteLine("Attempting rate " + sampleRate + "Hz, bits: " + encoding + ", channel: " + ChannelIn.Mono);
                            //mBufferSize = AudioRecord.GetMinBufferSize(sampleRate, channelConfig, encoding);
                            mBufferSize = AudioTrack.GetMinBufferSize(sampleRate, ChannelOut.Mono, encoding);

                            if (mBufferSize > 0)
                            {
                                // check if we can instantiate and have a success
                                AudioRecord recorder = new AudioRecord(AudioSource.VoiceCommunication, sampleRate, ChannelIn.Mono, encoding, bufferSize * 2);

                                if (recorder.State == State.Initialized)
                                {
                                    mSampleRate = sampleRate;
                                    mEncoding = encoding;
                                    return recorder;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(sampleRate + "Exception, keep trying." + ex.Message);
                        }
                    }
                }
            }
            return null;
        }

        public void Start()
        {
            endRecording = false;
            isRecording = true;

            RaiseRecordingStateChangedEvent();

            audioRecord = FindAudioRecord();
            if (audioRecord == null) return;

            audioBuffer = new byte[bufferSize];
            //audioBuffer = new byte[mBufferSize];

            audioRecord.StartRecording();

            // Off line this so that we do not block the UI thread.
            Thread thread = new Thread(CaptureAudio);
            thread.Start();
        }

        private void RaiseRecordingStateChangedEvent()
        {
            RecordingStateChanged?.Invoke(isRecording);
        }

        public void Stop()
        {
            endRecording = true;
            Thread.Sleep(500); // Give it time to drop out.
            micMuted = false;
        }

        private void CaptureAudio()
        {
            while (true)
            {
                if (endRecording)
                {
                    endRecording = false;
                    break;
                }
                try
                {
                    // Keep reading the buffer while there is audio input.
                    var size = audioRecord.Read(audioBuffer, 0, audioBuffer.Length);
                    if (size != audioBuffer.Length) return;
                    if (!micMuted)
                    {
                        OnAudioReady?.Invoke(ProtoSerializer.Serialize(new AudioPacket()
                        {
                            Bitrate = mSampleRate,
                            Encoding = (int)mEncoding,
                            Data = audioBuffer
                        }));
                    }
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine(ex.Message);
                    //break;
                }
            }
            audioRecord.Stop();
            audioRecord.Release();

            isRecording = false;

            RaiseRecordingStateChangedEvent();
        }
        private byte[] ResizeArray(byte[] byteArray, int len)
        {
            byte[] tmp = new byte[len];
            Array.Copy(byteArray, tmp, len);
            return tmp;
        }
    }
}