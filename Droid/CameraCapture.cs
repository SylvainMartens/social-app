using System;
using Android.Graphics;
using System.IO;
using System.Threading;
using Android.Views;
using Android.Widget;

namespace SocialAPP.Droid
{
    public class CameraCapture
    {
        public Action<byte[]> OnFrameReady;
        Android.Hardware.Camera PhoneCamera;
        TextureView videoView;

        SurfaceTexture _surface;

        public bool IsRecording { get; private set; }

        bool useFrontCamera;

        public CameraCapture(TextureView view)
        {
            videoView = view;
        }

        public void ToggleCamera()
        {
            if (IsRecording)
            {
                Stop();
            }
            else
            {
                Start();
            }
        }

        public bool Start()
        {
            IsRecording = true;
            var ready = PrepareVideoRecorder();
            if (!ready)
            {
                IsRecording = false;
                return false;
            }
            //PhoneCamera?.Lock();
            var thread = new Thread(CaptureFrames);
            thread.Start();
            return IsRecording;
        }

        public void Stop()
        {
            IsRecording = false;
            Thread.Sleep(100);
        }

        public void SwitchCamera()
        {
            if (Android.Hardware.Camera.NumberOfCameras < 2) return;
            useFrontCamera = !useFrontCamera;
            if (!IsRecording) return;

            PhoneCamera?.StopPreview();
            PhoneCamera?.Release();
            PhoneCamera = useFrontCamera ? Android.Hardware.Camera.Open(1) : Android.Hardware.Camera.Open();
            PhoneCamera.SetDisplayOrientation(90);
            PhoneCamera.SetPreviewTexture(_surface);
            PhoneCamera.StartPreview();
        }

        public void UpdateSurface(SurfaceTexture surface)
        {
            _surface = surface;
        }

        public void SurfaceDestroyed(SurfaceTexture surface)
        {
            //TOTO: Implement.
        }

        bool PrepareVideoRecorder()
        {
            try
            {
                PhoneCamera = useFrontCamera ? Android.Hardware.Camera.Open(1) : Android.Hardware.Camera.Open();

                PhoneCamera.SetDisplayOrientation(90);

                //VideoRecorder = new MediaRecorder();

                //Unlock & set camera to media recorder
                //PhoneCamera.Unlock();
                //VideoRecorder.SetCamera(PhoneCamera);

                //Set Source - Video and audio
                //VideoRecorder.SetAudioSource(AudioSource.Camcorder);
                //VideoRecorder.SetVideoSource(VideoSource.Camera);
                //VideoRecorder.SetOutputFormat(OutputFormat.Default);
                //VideoRecorder.SetVideoEncoder(VideoEncoder.H264);
                //VideoRecorder.SetAudioEncoder(AudioEncoder.Default);
                //set Camcorder profile
                //VideoRecorder.SetProfile(CamcorderProfile.Get(CamcorderQuality.Q720p));

                // var path = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/temp.mp4";
                //VideoRecorder.SetOutputFile(path);
                //set preview display
                // VideoRecorder.SetPreviewDisplay(VideoRecorderView.Holder.Surface);

                //VideoRecorder.SetMaxDuration(60000); // Set max duration 60 sec.
                //VideoRecorder.SetMaxFileSize(50000000); // Set max file size 50M

                //Prepare media recorder
                //VideoRecorder.SetMaxDuration(100);
                //VideoRecorder.SetVideoFrameRate(25);
                //VideoRecorder.Prepare();
                //VideoRecorder.Start();

                PhoneCamera.SetPreviewTexture(_surface);
                PhoneCamera.StartPreview();
            }
            catch (Exception ex)
            {
                //Utils.WriteDebugInfo("Excepion -PrepareVideoRecorder: " + ex.Message);
                return false;

            }
            return true;
        }

        public void CaptureFrames()
        {
            while (true)
            {
                if (!IsRecording)
                {
                    break;
                }
                if (videoView == null)
                {
                    IsRecording = false;
                    break;
                }
                if (PhoneCamera != null)
                {
                    try
                    {

                        var bitmap = videoView.Bitmap;
                        byte[] bitmapData;
                        var bitmapScalled = Bitmap.CreateScaledBitmap(bitmap, bitmap.Width, bitmap.Height, false);
                        bitmap.Recycle();
                        using (var stream = new MemoryStream())
                        {
                            var ping = MainActivity.callClient.Ping;
                            var quality = 25;
                            if (quality <= 500)
                                quality = 25;
                            if (ping <= 300)
                                quality = 50;
                            if (ping <= 200)
                                quality = 75;
                            if (ping <= 100)
                                quality = 95;
                            if (ping <= 50)
                                quality = 100;
                            bitmapScalled.Compress(Bitmap.CompressFormat.Jpeg, quality, stream);
                            bitmapData = stream.ToArray();
                        }
                        //var encoded = Base64.EncodeToString(bitmapData, Base64Flags.Default);
                        OnFrameReady?.Invoke(bitmapData);
                    }
                    catch (Exception e)
                    {

                    }
                }

                Thread.Sleep(40);
            }
            //VideoRecorder?.Stop();
            //VideoRecorder?.Release();
            //PhoneCamera?.Lock();
            PhoneCamera.StopPreview();
            PhoneCamera.Release();
            PhoneCamera = null;
        }
    }
}