using Network.Transmission;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;

namespace Network
{
    public class NetworkConnection
    {
        public delegate void DataEvent(byte tag, ushort subject, object data);

        public delegate void DetailedDataEvent(ushort sender, byte tag, ushort subject, object data);

        public delegate void ConnectionEvent(ushort id);

        private const ushort DISCONNECT_MESSAGE = 0;

        private const ushort CONNECTION_CLOSE_MESSAGE = 1;

        private const int DEFAULT_PORT = 4296;

        private bool _workInBackground = true;

        private TcpClient client;

        private NetworkStream stream;

        private Queue<NetworkMessage> receiveBuffer = new Queue<NetworkMessage>();

        public event NetworkConnection.DataEvent onData;

        public event NetworkConnection.DetailedDataEvent onDataDetailed;

        public event NetworkConnection.ConnectionEvent onPlayerDisconnected;

        public ushort id
        {
            get;
            private set;
        }

        public bool isConnected
        {
            get;
            private set;
        }

        public bool workInBackground
        {
            get
            {
                return this._workInBackground;
            }
            set
            {
                if (this.isConnected)
                {
                    throw new NotSupportedException("You cannot change the background worker settings while connected!");
                }
                this._workInBackground = value;
            }
        }

        public NetworkConnection()
        {
        }

        public NetworkConnection(string ip)
        {
            this.Connect(ip);
        }

        public NetworkConnection(string ip, int port)
        {
            this.Connect(ip, port);
        }

        public void ConnectInBackground(string ip)
        {
            this.ConnectInBackground(ip, 4296);
        }

        public void ConnectInBackground(string ip, int port)
        {
            ThreadPool.QueueUserWorkItem(delegate (object param0)
            {
                this.Connect(ip, port);
            });
        }

        public bool Connect(string ip)
        {
            return this.Connect(ip, 4296);
        }

        public bool Connect(string ip, int port)
        {
            try
            {
                this.client = new TcpClient(ip, port);
            }
            catch (Exception innerException)
            {
                throw new ConnectionFailedException("Couldn't connect to the specified server.", innerException);
            }
            this.stream = this.client.GetStream();
            this.client.NoDelay = true;
            this.client.LingerState = new LingerOption(true, 1);
            byte[] array = new byte[1];
            this.stream.Read(array, 0, 1);
            byte version = array[0];
            array = new byte[HandshakeProtocol.GetNoBytesOfHandshake(version) - 1];
            this.stream.Read(array, 0, array.Length);
            Handshake handshake = HandshakeProtocol.DecodeHandshake(version, array);
            if (handshake.connectionCode == ConnectionCode.Reject)
            {
                throw new ConnectionFailedException("Couldn't connect to server as the server rejected the connection!");
            }
            if (handshake.connectionCode == ConnectionCode.Accept)
            {
                this.id = handshake.ID;
                this.isConnected = true;
                if (this.workInBackground)
                {
                    byte[] array2 = new byte[4];
                    this.stream.BeginRead(array2, 0, 4, new AsyncCallback(this.DecodeDataToBuffer), array2);
                }
                return true;
            }
            throw new ConnectionFailedException("Probably couldn't connect to the server as the server said it's not sure if it wants to let us connect...? Received: " + ((byte)handshake.connectionCode).ToString());
        }

        private void DecodeDataToBuffer(IAsyncResult result)
        {
            int num;
            try
            {
                num = this.stream.EndRead(result);
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (Exception)
            {
                return;
            }
            if (num == 0)
            {
                this.Close(false);
            }
            byte[] value = (byte[])result.AsyncState;
            lock (this.receiveBuffer)
            {
                try
                {
                    this.receiveBuffer.Enqueue(this.ReadAndDecode(BitConverter.ToUInt32(value, 0)));
                }
                catch (InvalidOperationException)
                {
                    this.Close(false);
                }
            }
            byte[] array = new byte[4];
            try
            {
                this.stream.BeginRead(array, 0, 4, new AsyncCallback(this.DecodeDataToBuffer), array);
            }
            catch (ObjectDisposedException)
            {
            }
        }

        private NetworkMessage ReadAndDecode(uint byteCount)
        {
            NetworkMessage result = default(NetworkMessage);
            try
            {
                byte[] array = new byte[byteCount];
                if (this.stream.Read(array, 0, array.Length) == 0)
                {
                    throw new InvalidOperationException();
                }
                result = TransmissionProtocol.DecodeMessage(new DataBufferItem(null, array));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                //throw ex;
            }
            return result;
        }

        public void Receive()
        {
            if (this.isConnected)
            {
                if (this.workInBackground)
                {
                    lock (this.receiveBuffer)
                    {
                        while (this.receiveBuffer.Count > 0)
                        {
                            this.ProcessMessage(this.receiveBuffer.Dequeue());
                        }
                        return;
                    }
                }
                try
                {
                    while (this.stream.DataAvailable)
                    {
                        byte[] array = new byte[4];
                        this.stream.Read(array, 0, 4);
                        this.ProcessMessage(this.ReadAndDecode(BitConverter.ToUInt32(array, 0)));
                    }
                }
                catch (ObjectDisposedException)
                {
                    this.Close(false);
                }
                catch (Exception)
                {
                    //Unexpected error occured.
                }
            }
        }

        private void ProcessMessage(NetworkMessage message)
        {
            if (message.tag == 255)
            {
                if (message.subject == 0 && this.onPlayerDisconnected != null)
                {
                    this.onPlayerDisconnected(message.senderID);
                }
                if (message.subject == 1)
                {
                    this.Close(false);
                    return;
                }
            }
            else
            {
                if (this.onData != null)
                {
                    this.onData(message.tag, message.subject, message.data);
                }
                if (this.onDataDetailed != null)
                {
                    this.onDataDetailed(message.senderID, message.tag, message.subject, message.data);
                }
                if (message.data is NetworkReader)
                {
                    ((NetworkReader)message.data).Close();
                }
            }
        }

        public void SendMessageToServer(byte tag, ushort subject, object data)
        {
            this.SendMessage(DistributionType.Server, tag, subject, data, 0);
        }

        public void SendMessageToID(ushort targetID, byte tag, ushort subject, object data)
        {
            this.SendMessage(DistributionType.ID, tag, subject, data, targetID);
        }

        public void SendMessageToAll(byte tag, ushort subject, object data)
        {
            this.SendMessage(DistributionType.All, tag, subject, data, 0);
        }

        public void SendMessageToOthers(byte tag, ushort subject, object data)
        {
            this.SendMessage(DistributionType.Others, tag, subject, data, 0);
        }

        public void SendMessage(DistributionType distributionType, byte tag, ushort subject, object data, ushort id = 0)
        {
            this.SendMessage((byte)distributionType, tag, subject, data, id);
        }

        public void SendMessage(byte distributionMode, byte tag, ushort subject, object data, ushort destinationID = 0)
        {
            if (!this.isConnected)
            {
                throw new NotConnectedException("You're not connected so can't send data!");
            }
            NetworkMessage msg = new NetworkMessage(this.id, distributionMode, destinationID, tag, subject, data);
            if (this.ValidateNetworkMessage(msg))
            {
                this.SendNetworkMessage(msg);
                return;
            }
        }

        private bool ValidateNetworkMessage(NetworkMessage msg)
        {
            if (msg.tag == 255)
            {
                throw new InvalidDataException("Data could not be sent as it had a tag of 255 (this is reserved for internal use!)");
            }
            return true;
        }

        private void SendNetworkMessage(NetworkMessage msg)
        {
            if (this.workInBackground)
            {
                ThreadPool.QueueUserWorkItem(delegate (object param0)
                {
                    this.SendNetworkMessageNow(msg);
                });
                return;
            }
            this.SendNetworkMessageNow(msg);
        }

        private void SendNetworkMessageNow(NetworkMessage msg)
        {
            byte[] array = TransmissionProtocol.EncodeMessage(msg);
            lock (this.stream)
            {
                try
                {
                    this.stream.Write(array, 0, array.Length);
                }
                catch (Exception)
                {
                    this.Close(false);
                }
            }
        }

        public void Disconnect()
        {
            if (this.isConnected)
            {
                this.Close(true);
                return;
            }
            throw new NotConnectedException("You're not connected so you don't need to disconnect!");
        }

        private void Close(bool inform)
        {
            if (inform)
            {
                this.SendNetworkMessageNow(new NetworkMessage(this.id, DistributionType.All, 0, 255, 0, 0));
            }
            this.isConnected = false;
            this.stream.Close();
            this.receiveBuffer.Clear();
        }
    }
}