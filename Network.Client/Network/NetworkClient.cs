using System;

namespace Network
{
    public static class NetworkClient
    {
        public delegate void DataEvent(byte tag, ushort subject, object data);

        public delegate void DetailedDataEvent(ushort sender, byte tag, ushort subject, object data);

        public delegate void ConnectionEvent(ushort id);

        public static NetworkConnection connection = new NetworkConnection();

        private static bool areEventsRedirected = false;

        public static event NetworkClient.DataEvent onData;

        public static event NetworkClient.DetailedDataEvent onDataDetailed;

        public static event NetworkClient.ConnectionEvent onPlayerDisconnected;

        public static ushort id
        {
            get
            {
                return NetworkClient.connection.id;
            }
        }

        public static bool isConnected
        {
            get
            {
                return NetworkClient.connection.isConnected;
            }
        }

        public static bool workInBackground
        {
            get
            {
                return NetworkClient.connection.workInBackground;
            }
            set
            {
                NetworkClient.connection.workInBackground = value;
            }
        }

        public static void ConnectInBackground(string ip)
        {
            NetworkClient.RedirectConnectionEvents();
            NetworkClient.connection.ConnectInBackground(ip);
        }

        public static void ConnectInBackground(string ip, int port)
        {
            NetworkClient.RedirectConnectionEvents();
            NetworkClient.connection.ConnectInBackground(ip, port);
        }

        public static bool Connect(string ip)
        {
            NetworkClient.RedirectConnectionEvents();
            return NetworkClient.connection.Connect(ip);
        }

        public static bool Connect(string ip, int port)
        {
            NetworkClient.RedirectConnectionEvents();
            return NetworkClient.connection.Connect(ip, port);
        }

        private static void FireOnData(byte tag, ushort subject, object data)
        {
            if (NetworkClient.onData != null)
            {
                NetworkClient.onData(tag, subject, data);
            }
        }

        private static void FireOnDataDetailed(ushort sender, byte tag, ushort subject, object data)
        {
            if (NetworkClient.onDataDetailed != null)
            {
                NetworkClient.onDataDetailed(sender, tag, subject, data);
            }
        }

        private static void FireOnPlayerDisconnected(ushort id)
        {
            if (NetworkClient.onPlayerDisconnected != null)
            {
                NetworkClient.onPlayerDisconnected(id);
            }
        }

        [Obsolete("This method is spelt wrong. It's here for legacy purposes only, use Receive instead.")]
        public static void Recieve()
        {
            NetworkClient.connection.Receive();
        }

        public static void Receive()
        {
            NetworkClient.connection.Receive();
        }

        public static void SendMessageToServer(byte tag, ushort subject, object data)
        {
            NetworkClient.connection.SendMessageToServer(tag, subject, data);
        }

        public static void SendMessageToID(ushort targetID, byte tag, ushort subject, object data)
        {
            NetworkClient.connection.SendMessageToID(targetID, tag, subject, data);
        }

        public static void SendMessageToAll(byte tag, ushort subject, object data)
        {
            NetworkClient.connection.SendMessageToAll(tag, subject, data);
        }

        public static void SendMessageToOthers(byte tag, ushort subject, object data)
        {
            NetworkClient.connection.SendMessageToOthers(tag, subject, data);
        }

        public static void SendMessage(DistributionType distributionType, byte tag, ushort subject, object data, ushort id = 0)
        {
            NetworkClient.connection.SendMessage(distributionType, tag, subject, data, id);
        }

        public static void SendMessage(byte distributionMode, byte tag, ushort subject, object data, ushort destinationID = 0)
        {
            NetworkClient.connection.SendMessage(distributionMode, tag, subject, data, destinationID);
        }

        public static void Disconnect()
        {
            NetworkClient.connection.Disconnect();
        }

        private static void RedirectConnectionEvents()
        {
            if (!NetworkClient.areEventsRedirected)
            {
                NetworkClient.connection.onData += new NetworkConnection.DataEvent(NetworkClient.FireOnData);
                NetworkClient.connection.onDataDetailed += new NetworkConnection.DetailedDataEvent(NetworkClient.FireOnDataDetailed);
                NetworkClient.connection.onPlayerDisconnected += new NetworkConnection.ConnectionEvent(NetworkClient.FireOnPlayerDisconnected);
                NetworkClient.areEventsRedirected = true;
            }
        }
    }
}