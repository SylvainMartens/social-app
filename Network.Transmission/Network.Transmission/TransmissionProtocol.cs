using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Network.Transmission
{
    public static class TransmissionProtocol
    {
        public const byte latestMessageVersion = 0;

        public static NetworkMessage DecodeMessage(DataBufferItem item)
        {
            NetworkMessage result = TransmissionProtocol.DecodeMessageHeader(item);
            result.data = TransmissionProtocol.DecodeMessageData(item);
            return result;
        }

        public static object DecodeMessageData(DataBufferItem item)
        {
            object result;
            if (item.body.Length < 9)
            {
                result = null;
            }
            else
            {
                byte specifiedEncodingVersion = item.body[0];
                switch (specifiedEncodingVersion)
                {
                    case 0:
                    using (MemoryStream memoryStream = new MemoryStream(item.body, 9, item.body.Length - 9))
                    {
                        BinaryFormatter binaryFormatter = new BinaryFormatter();
                        result = binaryFormatter.Deserialize(memoryStream);
                        return result;
                    }
                    break;

                    case 1:
                    break;

                    default:
                    throw new VersionException(specifiedEncodingVersion);
                }
                MemoryStream stream = new MemoryStream(item.body, 9, item.body.Length - 9, false);
                result = new NetworkReader(stream);
            }
            return result;
        }

        public static NetworkMessage DecodeMessageHeader(DataBufferItem item)
        {
            NetworkMessage result = new NetworkMessage(BitConverter.ToUInt16(item.body, 1), item.body[3], BitConverter.ToUInt16(item.body, 4), item.body[6], BitConverter.ToUInt16(item.body, 7), item);
            return result;
        }

        private static byte[] EncodeMessage(NetworkMessage msg, byte version)
        {
            byte[] array;
            if (msg.data != null)
            {
                switch (version)
                {
                    case 0:
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        BinaryFormatter binaryFormatter = new BinaryFormatter();
                        try
                        {
                            binaryFormatter.Serialize(memoryStream, msg.data);
                        }
                        catch (ArgumentNullException)
                        {
                        }
                        array = memoryStream.ToArray();
                    }
                    break;

                    case 1:
                    {
                        NetworkWriter writer = (NetworkWriter)msg.data;
                        array = ((MemoryStream)writer.BaseStream).ToArray();
                        break;
                    }
                    default:
                    throw new VersionException(version);
                }
            }
            else
            {
                array = new byte[0];
            }
            byte[] array2 = new byte[13 + array.Length];
            Buffer.BlockCopy(BitConverter.GetBytes((uint)(array2.Length - 4)), 0, array2, 0, 4);
            array2[4] = version;
            Buffer.BlockCopy(BitConverter.GetBytes(msg.senderID), 0, array2, 5, 2);
            array2[7] = (byte)msg.distributionType;
            Buffer.BlockCopy(BitConverter.GetBytes(msg.destinationID), 0, array2, 8, 2);
            array2[10] = msg.tag;
            Buffer.BlockCopy(BitConverter.GetBytes(msg.subject), 0, array2, 11, 2);
            Buffer.BlockCopy(array, 0, array2, 13, array.Length);
            return array2;
        }

        public static byte[] EncodeMessage(NetworkMessage msg)
        {
            byte[] result;
            if (msg.data is NetworkWriter)
            {
                result = TransmissionProtocol.EncodeMessage(msg, 1);
            }
            else
            {
                result = TransmissionProtocol.EncodeMessage(msg, 0);
            }
            return result;
        }

        public static byte[] EncodeHeader(NetworkMessage msg, byte[] bytes)
        {
            Buffer.BlockCopy(BitConverter.GetBytes(msg.senderID), 0, bytes, 5, 2);
            bytes[7] = (byte)msg.distributionType;
            Buffer.BlockCopy(BitConverter.GetBytes(msg.destinationID), 0, bytes, 8, 2);
            bytes[10] = msg.tag;
            Buffer.BlockCopy(BitConverter.GetBytes(msg.subject), 0, bytes, 11, 2);
            return bytes;
        }
    }
}