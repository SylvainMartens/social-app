using System.IO;
using System.Text;

namespace Network
{
    public class NetworkReader : BinaryReader
    {
        private int readLength;

        private int i;

        private long Length
        {
            get
            {
                return this.BaseStream.Length;
            }
        }

        internal NetworkReader(Stream stream) : base(stream)
        {
        }

        internal NetworkReader(Stream stream, Encoding encoding) : base(stream, encoding)
        {
        }

        public virtual byte[] ReadBytes()
        {
            this.readLength = base.ReadInt32();
            byte[] array = new byte[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadByte();
                this.i++;
            }
            return array;
        }

        public virtual char[] ReadChars()
        {
            this.readLength = base.ReadInt32();
            char[] array = new char[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadChar();
                this.i++;
            }
            return array;
        }

        public virtual bool[] ReadBooleans()
        {
            this.readLength = base.ReadInt32();
            bool[] array = new bool[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadBoolean();
                this.i++;
            }
            return array;
        }

        public virtual decimal[] ReadDecimals()
        {
            this.readLength = base.ReadInt32();
            decimal[] array = new decimal[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadDecimal();
                this.i++;
            }
            return array;
        }

        public virtual double[] ReadDoubles()
        {
            this.readLength = base.ReadInt32();
            double[] array = new double[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadDouble();
                this.i++;
            }
            return array;
        }

        public virtual short[] ReadInt16s()
        {
            this.readLength = base.ReadInt32();
            short[] array = new short[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadInt16();
                this.i++;
            }
            return array;
        }

        public virtual int[] ReadInt32s()
        {
            this.readLength = base.ReadInt32();
            int[] array = new int[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadInt32();
                this.i++;
            }
            return array;
        }

        public virtual long[] ReadInt64s()
        {
            this.readLength = base.ReadInt32();
            long[] array = new long[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadInt64();
                this.i++;
            }
            return array;
        }

        public virtual sbyte[] ReadSBytes()
        {
            this.readLength = base.ReadInt32();
            sbyte[] array = new sbyte[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadSByte();
                this.i++;
            }
            return array;
        }

        public virtual float[] ReadSingles()
        {
            this.readLength = base.ReadInt32();
            float[] array = new float[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadSingle();
                this.i++;
            }
            return array;
        }

        public virtual string[] ReadStrings()
        {
            this.readLength = base.ReadInt32();
            string[] array = new string[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadString();
                this.i++;
            }
            return array;
        }

        public virtual ushort[] ReadUInt16s()
        {
            this.readLength = base.ReadInt32();
            ushort[] array = new ushort[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadUInt16();
                this.i++;
            }
            return array;
        }

        public virtual uint[] ReadUInt32s()
        {
            this.readLength = base.ReadInt32();
            uint[] array = new uint[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadUInt32();
                this.i++;
            }
            return array;
        }

        public virtual ulong[] ReadUInt64s()
        {
            this.readLength = base.ReadInt32();
            ulong[] array = new ulong[this.readLength];
            this.i = 0;
            while (this.i < this.readLength)
            {
                array[this.i] = base.ReadUInt64();
                this.i++;
            }
            return array;
        }
    }
}